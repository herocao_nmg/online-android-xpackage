/*
 *Copyright (c) 2013. ccx (ccxandroid@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package x.bitmap;

import java.io.FileDescriptor;
import java.util.logging.Logger;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Bitmap解码器类
 * @author Hero
 * @since 2013-10-21
 */
public class BitmapDecoder {
	
	private static final String TAG = "BitmapDecoder";
	private static Bitmap mBitmap;
	private static Bitmap.Config mConfig=Bitmap.Config.RGB_565;

    private BitmapDecoder() {
    }

    /**
     * 從應用的資源中根據設置的圖片資源ID、大小和顯示配置進行解碼Bitmap
     * @param res 應用的資源實例
     * @param resId 圖片資源ID
     * @param reqWidth 圖片顯示的寬度
     * @param reqHeight 圖片顯示的高度
     * @param config 圖片顯示配置
     * @return
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight, Bitmap.Config config) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        options.inInputShareable = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        if (config != null) {
            options.inPreferredConfig = config;
        }
        try {
            return BitmapFactory.decodeResource(res, resId, options);
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * 根據設置的圖片資源文件名稱、大小和顯示配置進行解碼Bitmap
     * @param filename 图片资源文件名称
     * @param reqWidth 圖片顯示的寬度
     * @param reqHeight 圖片顯示的高度
     * @param config 圖片顯示配置
     * @return
     */
    public static Bitmap decodeSampledBitmapFromFile(String filename, int reqWidth, int reqHeight, Bitmap.Config config) {
       /* final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        options.inInputShareable = true;
        BitmapFactory.decodeFile(filename, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        
        
        if (config != null) {
            options.inPreferredConfig = config;
        }
        try {
            return BitmapFactory.decodeFile(filename, options);
        } catch (Throwable e) {
            e.printStackTrace();
            Logger.i(TAG, " decodeSampledBitmapFromFile=图片溢出 ， filename="+filename);
          
            // 需要 解决加载出错之后的，多加载3次！！！！！！！！！！！！
         //  if(count<=3){
            return decodeSampledBitmapFromFile(filename, reqWidth, reqHeight, config);
          //  }
        }*/
    	
    	
    	return decodeSampledBitmapFromFile(filename, reqWidth, reqHeight, config,5);
    }
    
    

    /**
     * 根據設置的圖片資源文件名稱、大小和顯示配置進行解碼Bitmap
     * @param filename 图片资源文件名称
     * @param reqWidth 圖片顯示的寬度
     * @param reqHeight 圖片顯示的高度
     * @param config 圖片顯示配置
     * @return
     */
    public static Bitmap decodeSampledBitmapFromFile(String filename, int reqWidth, int reqHeight, Bitmap.Config config,int count) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        options.inInputShareable = true;
        BitmapFactory.decodeFile(filename, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        
        
        if (config != null) {
            options.inPreferredConfig = config;
        }
        try {
            return BitmapFactory.decodeFile(filename, options);
        } catch (Throwable e) {
            e.printStackTrace();
           -- count;
           if(count!=0){
        	   return decodeSampledBitmapFromFile(filename, reqWidth, reqHeight, config,count);
           }else{
               return null;
           }
        }
    }

    /**
     * 根據設置的圖片資源文件描述符實例、大小和顯示配置進行解碼Bitmap
     * @param fileDescriptor 圖片資源文件描述符實例
     * @param reqWidth 圖片顯示的寬度
     * @param reqHeight 圖片顯示的高度
     * @param config 圖片顯示配置
     * @return
     */
    public static Bitmap decodeSampledBitmapFromDescriptor(FileDescriptor fileDescriptor, int reqWidth, int reqHeight, Bitmap.Config config) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        options.inInputShareable = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        if (config != null) {
            options.inPreferredConfig = config;
        }
        try {
            return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 根據設置的圖片資源文件byte[]數據、大小和顯示配置進行解碼Bitmap
     * @param data 圖片資源文件byte[]數據
     * @param reqWidth 圖片顯示寬度
     * @param reqHeight 圖片顯示高度
     * @param config 圖片顯示配置
     * @return
     */
    public static Bitmap decodeSampledBitmapFromByteArray(byte[] data, int reqWidth, int reqHeight, Bitmap.Config config) {
    	if(null==data || data.length<0 || null==config) return null;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        options.inInputShareable = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        
        options.inJustDecodeBounds = false;
        if (config != null) {
            options.inPreferredConfig = config;
        }
        try {
            return BitmapFactory.decodeByteArray(data, 0, data.length, options);
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 從應用的資源中根據設置的圖片資源ID進行解碼Bitmap
     * @param res 應用資源實例
     * @param resId 圖片資源ID
     * @return
     */
    public static Bitmap decodeResource(Resources res, int resId) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inInputShareable = true;
        try {
            return BitmapFactory.decodeResource(res, resId, options);
        } catch (Throwable e) {
        	  e.printStackTrace();
            return null;
        }
    }

    /**
     * 根據圖片資源文件名稱解碼Bitmap
     * @param filename 圖片資源文件的名稱
     * @return
     */
    public static Bitmap decodeFile(String filename) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inInputShareable = true;
        try {
            return BitmapFactory.decodeFile(filename, options);
        } catch (Throwable e) {
        	  e.printStackTrace();
            return null;
        }
    }

    /**
     * 根據圖片資源文件描述符實例解碼Bitmap
     * @param fileDescriptor 圖片資源文件描述符實例
     * @return
     */
    public static Bitmap decodeFileDescriptor(FileDescriptor fileDescriptor) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inInputShareable = true;
        try {
            return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
        } catch (Throwable e) {
        	  e.printStackTrace();
            return null;
        }
    }

    /**
     * 根據圖片資源文件的byte[]數據進行解碼Bitmap
     * @param data 圖片資源文件的byte[]數據
     * @return
     */
    public static Bitmap decodeByteArray(byte[] data) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        options.inInputShareable = true;
        try {
            return BitmapFactory.decodeByteArray(data, 0, data.length, options);
        } catch (Throwable e) {
        	  e.printStackTrace();
            return null;
        }
    }

    /**
     * 根據圖片顯示的寬度和高度來計算圖片解碼的採樣大小
     * @param options
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }

            final float totalPixels = width * height;

            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        
        
        return inSampleSize;
    }
}
