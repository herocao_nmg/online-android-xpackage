package x.silverlight;

import java.util.LinkedList;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.view.MotionEvent;

/**
 * SilverlightImage工具類
 * @author Hero
 * @since 2014-01-13
 */
public class SilverlightImageUtils {

	/**
	 * 平移
	 * @param meta
	 * @param eX
	 * @param eY
	 */
	 public static void translate(SilverlightImageMeta meta,int eX , int eY){  
		meta.prePivot.x += eX - meta.lastPoint.x;
		meta.prePivot.y += eY - meta.lastPoint.y;
		meta.moveX = meta.prePivot.x - meta.lastPivot.x;
		meta.moveY = meta.prePivot.y - meta.lastPivot.y;
		meta.lastPivot.x = meta.prePivot.x;
		meta.lastPivot.y = meta.prePivot.y;
		setMatrix(meta, SilverlightImageConfig.OPER_TRANSLATE);  
	    }  
	 
	  	/**
	  	 * 設置Matrix
	  	 * @param meta
	  	 * @param operationType
	  	 */
	    public static void setMatrix(SilverlightImageMeta meta,int operationType){  
	        switch (operationType) {  
	        case SilverlightImageConfig.OPER_TRANSLATE:  
	        	meta.matrix.postTranslate(meta.moveX , meta.moveY);  
	            break;  
	        case SilverlightImageConfig.OPER_SCALE:  
	        	meta.matrix.postScale(meta.scaleValue, meta.scaleValue, meta.symmetricPoint.x, meta.symmetricPoint.y);  
	            break;  
	        case SilverlightImageConfig.OPER_ROTATE:  
	        	meta.matrix.postRotate(meta.preDegree - meta.lastDegree, meta.dstPoints[SilverlightImageConfig.CTR_MID_MID * 2], meta.dstPoints[SilverlightImageConfig.CTR_MID_MID * 2 + 1]);  
	            break;  
	        }  
	        meta.matrix.mapPoints(meta.dstPoints, meta.srcPoints);  
	        meta.matrix.mapRect(meta.dstRect, meta.srcRect);  
	    }  
	 
	/**
	 * 獲取MotionEvent的Action
	 * @param event
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static int getAction(MotionEvent event) {
		MotionEvent me = event;
		if (me.getAction() == MotionEvent.ACTION_POINTER_1_UP
				|| me.getAction() == MotionEvent.ACTION_POINTER_2_UP) {
			return MotionEvent.ACTION_UP;
		}
		if (me.getAction() == MotionEvent.ACTION_POINTER_1_DOWN
				|| me.getAction() == MotionEvent.ACTION_POINTER_2_DOWN) {
			return MotionEvent.ACTION_DOWN;
		}
		return me.getAction();
	}
	 
	 /**
	  * 繪製操作控件
	  * @param canvas
	  * @param meta
	  */
	 public static void drawControlPoints(Canvas canvas,SilverlightImageMeta meta){  
	        for (int i = 0; i < meta.dstPoints.length; i+=2) {  
	        	if(i==16){
	        		canvas.drawBitmap(meta.rotateBmp, meta.dstPoints[i]-meta.rotateBmpWidth/2.0f, meta.dstPoints[i+1]-meta.rotateBmpHeight/2.0f, meta.paint);
	        	}else if(i==12){
	        		canvas.drawBitmap(meta.deleteBmp, meta.dstPoints[i]-meta.deleteBmpWidth/2.0f, meta.dstPoints[i+1]-meta.deleteBmpHeight/2.0f, meta.paint);  
	        	}else{
	        		canvas.drawBitmap(meta.scaleBmp, meta.dstPoints[i]-meta.scaleBmpWidth/2.0f, meta.dstPoints[i+1]-meta.scaleBmpHeight/2.0f, meta.paint);  
	        	}
	        }  
	    }  
	 
	 /**
	  * 繪製邊框
	  * @param canvas
	  * @param meta
	  */
	 public static  void drawFrame(Canvas canvas,SilverlightImageMeta meta){  
	        canvas.drawLine(meta.dstPoints[0], meta.dstPoints[1], meta.dstPoints[4], meta.dstPoints[5], meta.paintFrame);  
	        canvas.drawLine(meta.dstPoints[4], meta.dstPoints[5], meta.dstPoints[8], meta.dstPoints[9], meta.paintFrame);  
	        canvas.drawLine(meta.dstPoints[8], meta.dstPoints[9], meta.dstPoints[12], meta.dstPoints[13], meta.paintFrame);  
	        canvas.drawLine(meta.dstPoints[0], meta.dstPoints[1], meta.dstPoints[12], meta.dstPoints[13], meta.paintFrame);  
//	        canvas.drawPoint(meta.dstPs[16], meta.dstPs[17], meta.paintFrame); // 中心點
	    }  
	
	 /**
	  * 繪製背景
	  * @param canvas
	  * @param meta
	  */
	  public static void drawBackground(Canvas canvas,SilverlightImageMeta meta){  
	        canvas.drawRect(meta.dstRect, meta.paintRect);  
	    }  
	  
	  /**
	   * 計算兩個座標點直接的距離
	   * @param p1
	   * @param p2
	   * @return
	   */
	  public static float computeDistance(Point p1, Point p2){  
	        return (float)(Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y)));  
	    }  
	      
	  /**
	   * 計算距離
	   * @param x1
	   * @param y1
	   * @param x2
	   * @param y2
	   * @return
	   */
	    public static float computeDistance(float x1,float y1,float x2,float y2){  
	        return (float)(Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)));  
	    }  
	    
	    
	    /** 
	     * 计算两点与垂直方向夹角 
	     * @param p1 
	     * @param p2 
	     * @return 
	     */  
	    public static float computeDegree(PointF p1, PointF p2){  
	        float tranX = p1.x - p2.x;  
	        float tranY = p1.y - p2.y;  
	        float degree = 0.0f;  
	        float angle = (float)(Math.asin(tranX/Math.sqrt(tranX*tranX + tranY* tranY))*180/Math.PI);  
	        if(!Float.isNaN(angle)){  
	            if(tranX >= 0 && tranY <= 0){
	                degree = angle;  
	            }else if(tranX <= 0 && tranY <= 0){   
	                degree = angle;  
	            }else if(tranX <= 0 && tranY >= 0){
	                degree = -180 - angle;  
	            }else if(tranX >= 0 && tranY >= 0){
	                degree = 180 - angle;  
	            }  
	        }  
	        return degree;  
	    }  
	    
	    /**
	     * 旋轉
	     * @param event
	     * @param meta
	     */
	    public static void rotate(MotionEvent event,SilverlightImageMeta meta) {  
	        if(event.getPointerCount() == 2){  
	        	meta.preDegree = computeDegree(new PointF(event.getX(1), event.getY(1)), new PointF(event.getX(0), event.getY(0)));  
	        }else{  
	        	meta.preDegree = computeDegree(new PointF(event.getX(), event.getY()), new PointF(meta.dstPoints[16], meta.dstPoints[17]));  
	        }  
	        setMatrix(meta,SilverlightImageConfig.OPER_ROTATE);  
	        meta.lastDegree = meta.preDegree;  
	    }  
	    
	    /**
	     * 旋轉
	     * @param event
	     * @param meta
	     * @param currentDegree
	     * @return
	     */
	    public static float rotate(MotionEvent event,SilverlightImageMeta meta,float currentDegree) {  
	        if(event.getPointerCount() == 2){  
	        	meta.preDegree = computeDegree(new PointF(event.getX(0), event.getY(0)), new PointF(event.getX(1), event.getY(1)));  
	        }
	        else{  
	        	meta.preDegree = computeDegree(new PointF(event.getX(), event.getY()), new PointF(meta.dstPoints[16], meta.dstPoints[17]));  
	        }  
	        meta.lastDegree=currentDegree;
	        setMatrix(meta,SilverlightImageConfig.OPER_ROTATE);  
	        meta.lastDegree = meta.preDegree;  
	        return meta.preDegree;
	    }  
	    
	    /**
	     * 縮放
	     * @param event
	     * @param meta
	     * @param currentControl
	     */
	    public static void scale(MotionEvent event,SilverlightImageMeta meta,int currentControl) {  
	        int pointIndex = currentControl*2 ;  
	        float px = meta.dstPoints[pointIndex];  
	        float py = meta.dstPoints[pointIndex+1];  
	        float eX = event.getX();  
	        float eY = event.getY();  
	        float oppositeX = 0 ;  
	        float oppositeY = 0 ;  
	        if(currentControl<4 &&currentControl >= 0){  
	             oppositeX = meta.dstPoints[pointIndex+8];  
	             oppositeY = meta.dstPoints[pointIndex+9];  
	        }else if(currentControl >= 4){  
	             oppositeX = meta.dstPoints[pointIndex-8];  
	             oppositeY = meta.dstPoints[pointIndex-7];  
	        }  
	        float temp1 = computeDistance(px,py,oppositeX,oppositeY);  
	        float temp2 = computeDistance(eX,eY,oppositeX,oppositeY);  
	        meta.scaleValue = temp2 / temp1 ;  
	        meta.totalScale*=meta.scaleValue;
	        if(meta.mainBmpHeight*meta.totalScale < SilverlightImageConfig.minHeight || meta.mainBmpWidth*meta.totalScale <SilverlightImageConfig.minWidth){
	        	meta.totalScale/=meta.scaleValue*1.0f;
	        	setMatrix(meta,SilverlightImageConfig.OPER_DEFAULT);
	        }else{
	        	meta.symmetricPoint.x = (int) oppositeX;  
	        	meta.symmetricPoint.y = (int)oppositeY;  
	            setMatrix(meta,SilverlightImageConfig.OPER_SCALE);  
	        }
	    }  
	    
	/**
	 * 縮放
	 * @param event
	 * @param meta
	 * @param currentDistance
	 * @return
	 */
	public static float scale(MotionEvent event, SilverlightImageMeta meta,
			float currentDistance) {
		float eX = event.getX();
		float eY = event.getY();
		float eX2 = event.getX(1);
		float eY2 = event.getY(1);
		float oppositeX = meta.dstPoints[16];
		float oppositeY = meta.dstPoints[17];
		float distance = computeDistance(eX, eY, eX2, eY2);
		meta.scaleValue = distance / currentDistance;
		meta.totalScale *= meta.scaleValue;
		if (meta.mainBmpHeight * meta.totalScale < SilverlightImageConfig.minHeight
				|| meta.mainBmpWidth * meta.totalScale < SilverlightImageConfig.minWidth) {
			meta.totalScale /= meta.scaleValue * 1.0f;
			setMatrix(meta, SilverlightImageConfig.OPER_DEFAULT);
		} else {
			meta.symmetricPoint.x = (int) oppositeX;
			meta.symmetricPoint.y = (int) oppositeY;
			setMatrix(meta, SilverlightImageConfig.OPER_SCALE);
		}
		return distance;
	}
	    
	/**
	 * 是否觸摸中圖片
	 * @param meta
	 * @param x
	 * @param y
	 * @return
	 */
	    public static boolean isTouchInImage(SilverlightImageMeta meta,float x,float y){
	    	float[] p = getPointBeforTransform(x,y,meta.matrix);
			if (p[0] > 0 && p[0] <= meta.mainBmpWidth && p[1] > 0 && p[1] <= meta.mainBmpHeight) {
				return true;
			}
			return false;
	    }
	    
	    /**
	     * 獲取變換之前的座標點
	     * @param x
	     * @param y
	     * @param matrix
	     * @return
	     */
	    public static float[] getPointBeforTransform(float x, float y, Matrix matrix) {
			Matrix inverseMatrix = new Matrix();
			matrix.invert(inverseMatrix);
			float[] values = new float[9];
			inverseMatrix.getValues(values);
			float x1 = values[0] * x + values[1] * y + values[2];
			float y1 = values[3] * x + values[4] * y + values[5];
			return new float[]{x1,y1};
		}
	    
	    /**
	     * 獲取目標
	     * @param metas
	     * @param x
	     * @param y
	     * @return
	     */
	public static SilverlightImageMeta getTarget(
			LinkedList<SilverlightImageMeta> metas, float x, float y) {
		for (int i = 0; i < metas.size(); i++) {
			if (metas.get(i).isSelected
					&& isTouchInControl(metas.get(i), x, y) != SilverlightImageConfig.CTR_NONE) {
				changePriority(metas, metas.get(i));
				return metas.get(i);
			}
			float[] p = getPointBeforTransform(x, y, metas.get(i).matrix);
			if (p[0] > 0 && p[0] <= metas.get(i).mainBmpWidth && p[1] > 0
					&& p[1] <= metas.get(i).mainBmpHeight) {
				changePriority(metas, metas.get(i));
				return metas.get(i);
			}
		}
		return null;
	}
	    
	    /**
	     * 改變優先級
	     * @param metas
	     * @param target
	     */
	    public  static void changePriority(LinkedList<SilverlightImageMeta> metas,SilverlightImageMeta target){
	    	for(SilverlightImageMeta meta : metas){
	    		if(meta.priority>target.priority){
	    			meta.priority-=1;
	    		}
	    		if(meta!=target){
	    			meta.isSelected=false;
	    		}
	    	}
	    	target.priority=metas.size()-1;
	    	target.isSelected=true;
		}
	    
	    /**
	     * 通過優先級找到目標
	     * @param metas
	     * @param priority
	     * @return
	     */
	    public static SilverlightImageMeta getTargetByPriority(LinkedList<SilverlightImageMeta> metas,int priority){
	    	for(SilverlightImageMeta meta : metas){
	    		if(meta.priority==priority) return meta;
	    	}
	    	return null;
	    }
	    
	    /**
	     * 通過座標點找到目標
	     * @param metas
	     * @param point
	     * @return
	     */
	    public static SilverlightImageMeta getTargetByPoint(SilverlightImageMeta[] metas,Point point){
	    	for(SilverlightImageMeta meta : metas ){
	    		RectF rect = new RectF(point.x-meta.scaleBmpWidth/2.0f,point.y-meta.scaleBmpHeight/2.0f,point.x+meta.scaleBmpWidth/2.0f,point.y+meta.scaleBmpHeight/2.0f);
	    		for(int i=0;i<meta.dstPoints.length;i+=2){
	    			if(rect.contains(meta.dstPoints[i], meta.dstPoints[i+1])) return meta;
	    		}
	    	}
	    	return null;
	    }
	    
	    /**
	     * 通過操作控件的索引找到對應的座標點
	     * @param meta
	     * @param ctrIndex
	     * @return
	     */
	    public static PointF getPointByIndex(SilverlightImageMeta meta,int ctrIndex){
	    	switch (ctrIndex) {
			case SilverlightImageConfig.CTR_LEFT_TOP:
				return new PointF(meta.dstPoints[0], meta.dstPoints[1]);
			case SilverlightImageConfig.CTR_MID_TOP:
				return new PointF(meta.dstPoints[2], meta.dstPoints[3]);
			case SilverlightImageConfig.CTR_RIGHT_TOP:
				return new PointF(meta.dstPoints[4], meta.dstPoints[5]);
			case SilverlightImageConfig.CTR_RIGHT_MID:
				return new PointF(meta.dstPoints[6], meta.dstPoints[7]);
			case SilverlightImageConfig.CTR_RIGHT_BOTTOM:
				return new PointF(meta.dstPoints[8], meta.dstPoints[9]);
			case SilverlightImageConfig.CTR_MID_BOTTOM:
				return new PointF(meta.dstPoints[10], meta.dstPoints[11]);
			case SilverlightImageConfig.CTR_LEFT_BOTTOM:
				return new PointF(meta.dstPoints[13], meta.dstPoints[14]);
			case SilverlightImageConfig.CTR_LEFT_MID:
				return new PointF(meta.dstPoints[14], meta.dstPoints[15]);
			case SilverlightImageConfig.CTR_MID_MID:
				return new PointF(meta.dstPoints[16], meta.dstPoints[17]);
			default:
				return null;
			}
	    }
	    
	    
	    /** 
	     * 判斷點是觸摸到操作控件
	     * @param evX 
	     * @param evY 
	     * @return 
	     */   
	    public static int isTouchInControl(SilverlightImageMeta meta,float eX, float eY) {  
	        RectF rect = new RectF(eX-meta.scaleBmpWidth/2.0f,eY-meta.scaleBmpHeight/2.0f,eX+meta.scaleBmpWidth/2.0f,eY+meta.scaleBmpHeight/2.0f);  
	        int dot = 0 ;  
	        for (int i = 0; i < meta.dstPoints.length; i+=2) {   
	            if(rect.contains(meta.dstPoints[i], meta.dstPoints[i+1])){  
	                return dot ;  
	            }  
	            ++dot ;   
	        }  
	        return SilverlightImageConfig.CTR_NONE;  
	    }  
	    
	    /**
	     * 獲取操作類型
	     * @param event
	     * @param meta
	     * @param currentControl
	     * @param lastOper
	     * @return
	     */
	    public static  int[] getOperationType(MotionEvent event,SilverlightImageMeta meta,int currentControl,int lastOper){  
	        float eX = event.getX();  
	        float eY = event.getY();  
	        int curOper = lastOper;  
	        switch(event.getAction()) {  
	            case MotionEvent.ACTION_DOWN:  
	                currentControl = isTouchInControl(meta,eX, eY);  
	                boolean result=false;
	                if(currentControl != SilverlightImageConfig.CTR_NONE || (result=isTouchInImage(meta,eX, eY))){  
	                    curOper = SilverlightImageConfig.OPER_SELECTED;  
	                }  
	                break;  
	            case MotionEvent.ACTION_POINTER_2_DOWN:
	            case MotionEvent.ACTION_POINTER_DOWN:
	            	if(curOper==SilverlightImageConfig.OPER_SELECTED){
	            		curOper=SilverlightImageConfig.OPER_SCALE_ROTATE;
	            	}
	            	break;
	            case MotionEvent.ACTION_MOVE:  
	            	  if(currentControl==SilverlightImageConfig.CTR_LEFT_BOTTOM){
	            		  curOper=SilverlightImageConfig.OPER_DELETE;
	            	  }
	            	  else if(currentControl > SilverlightImageConfig.CTR_NONE  &&  currentControl < SilverlightImageConfig.CTR_MID_MID  ){  
	                        curOper = SilverlightImageConfig.OPER_SCALE;  
	                    }else if(currentControl == SilverlightImageConfig.CTR_MID_MID ){  
	                        curOper = SilverlightImageConfig.OPER_ROTATE;  
	                    }else if(lastOper == SilverlightImageConfig.OPER_SELECTED && event.getPointerCount()==1){  
	                        curOper = SilverlightImageConfig.OPER_TRANSLATE;  
	                    }  
	                break;  
	            case MotionEvent.ACTION_UP:  
	            	curOper=SilverlightImageConfig.OPER_DEFAULT;
	                break;  
	        }  
	        return new int[]{ currentControl,curOper };
	    }  

	    /**
	     * 釋放資源
	     * @param meta
	     */
	    public static void recycle(SilverlightImageMeta meta){
	    	meta.deleteBmp.recycle();
	    	meta.deleteBmp=null;
	    	meta.mainBmp.recycle();
	    	meta.mainBmp=null;
	    	meta.scaleBmp.recycle();
	    	meta.scaleBmp=null;
	    	meta.rotateBmp.recycle();
	    	meta.rotateBmp=null;
	    	meta=null;
	    }
	    
	    /**
	     * 進行繪製
	     * @param canvas
	     * @param metas
	     */
	    public static void doDraw(Canvas canvas,LinkedList<SilverlightImageMeta> metas){
	    	for(int i=0;i<metas.size();i++){
	    		SilverlightImageMeta meta = SilverlightImageUtils.getTargetByPriority(metas, i);
//	            	drawBackground(canvas,meta); 
	    			canvas.drawBitmap(meta.mainBmp, meta.matrix, meta.paint); 
	    			if(meta.isSelected){
		    			drawFrame(canvas,meta); 
		    			drawControlPoints(canvas,meta); 
	    			}
	    	}
	    }
	    
	    /**
	     * 配置資源優先級
	     * @param metas
	     */
	    public static void configPriority(LinkedList<SilverlightImageMeta> metas){
	    	for(int i=0;i<metas.size();i++){
	    		metas.get(i).priority=i;
	    	}
	    }
	    
}
