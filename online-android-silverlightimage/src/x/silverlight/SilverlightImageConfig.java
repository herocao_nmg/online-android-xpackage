package x.silverlight;

/**
 * SilverlightImage配置
 * @author Hero
 * @since 2014-01-08
 */
public class SilverlightImageConfig {

	public static final int OPER_DEFAULT = -1;     
    public static final int OPER_TRANSLATE = 0;    
    public static final int OPER_SCALE = 1;         
    public static final int OPER_ROTATE = 2;       
    public static final int OPER_SELECTED = 3;     
    public static final int OPER_DELETE=4; 
    public static final int OPER_SCALE_ROTATE=5; 
      
    public static final int CTR_NONE = -1;  
    public static final int CTR_LEFT_TOP = 0;  
    public static final int CTR_MID_TOP = 1;  
    public static final int CTR_RIGHT_TOP = 2;  
    public static final int CTR_RIGHT_MID = 3;  
    public static final int CTR_RIGHT_BOTTOM = 4;  
    public static final int CTR_MID_BOTTOM = 5;  
    public static final int CTR_LEFT_BOTTOM = 6;  
    public static final int CTR_LEFT_MID = 7;  
    public static final int CTR_MID_MID = 8;  
    
	public static final int minWidth = 200;
	public static final int minHeight = 200;
	
}