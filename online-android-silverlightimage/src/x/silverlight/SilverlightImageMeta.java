package x.silverlight;

import hk.nmg.face.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;


/**
 * SilverlightImage數據元
 * @author Hero
 * @since 2014-01-08
 */
public final class SilverlightImageMeta {

	public Bitmap mainBmp, rotateBmp, scaleBmp, deleteBmp;
	public int mainBmpWidth, mainBmpHeight, scaleBmpWidth, scaleBmpHeight,
			rotateBmpWidth, rotateBmpHeight, deleteBmpWidth, deleteBmpHeight;
	public Matrix matrix;
	public float[] srcPoints, dstPoints;
	public RectF srcRect, dstRect;
	public Paint paint, paintRect, paintFrame;
	public float moveX = 0, moveY = 0;  
	public float scaleValue = 1.0f;  
	public Point lastPoint;
	public Point prePivot, lastPivot;
	public float preDegree, lastDegree;
	public short currentSelectedPointindex;  
	public Point symmetricPoint = new Point();  

	public float totalScale = 1.0f;
	public boolean isDelete = false;  
	public boolean isSelected=false;
	
	public int priority;
	public String uri;
	
	public SilverlightImageMeta(Context context,Bitmap bitmap,int defaultX,int defaultY){
		   mainBmp = bitmap;
	        rotateBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.accessoryoperater);  
	        scaleBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.beautify_boobs_scale_button);  
	        deleteBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.album_btn_delete_a);  
	        mainBmpWidth = mainBmp.getWidth();  
	        mainBmpHeight = mainBmp.getHeight();  
	        
	        // 注意, 必須保證所有操作控件的大小一樣 ,只需要一個 controlWidth 和 controlHeight 就可以了
	        scaleBmpWidth = scaleBmp.getWidth();  
	        scaleBmpHeight = scaleBmp.getHeight();  
	        rotateBmpWidth = rotateBmp.getWidth();  
	        rotateBmpHeight = rotateBmp.getHeight();  
	        deleteBmpWidth = deleteBmp.getWidth();  
	        deleteBmpHeight = deleteBmp.getHeight();  
	          
	        srcPoints = new float[]{  
	                                0,0,   
	                                mainBmpWidth/2 , 0 ,   
	                                mainBmpWidth , 0 ,   
	                                mainBmpWidth , mainBmpHeight/2 ,  
	                                mainBmpWidth , mainBmpHeight ,   
	                                mainBmpWidth/2 , mainBmpHeight ,   
	                                0 , mainBmpHeight ,   
	                                0 , mainBmpHeight/2 ,   
	                                mainBmpWidth/2 , mainBmpHeight/2  
	                            };  
	        dstPoints = srcPoints.clone();  
	        srcRect = new RectF(0, 0, mainBmpWidth, mainBmpHeight);  
	        dstRect = new RectF();  
	        matrix = new Matrix();  
	        prePivot = new Point(mainBmpWidth/2, mainBmpHeight/2);  
	        lastPivot = new Point(mainBmpWidth/2, mainBmpHeight/2);  
	        lastPoint = new Point(0,0);  
	        paint = new Paint();  
	        paintRect = new Paint();  
	        paintRect.setColor(Color.RED);  
	        paintRect.setAlpha(100);  
	        paintRect.setAntiAlias(true);  
	        paint.setFilterBitmap(true);
	        paintFrame = new Paint();  
	        paintFrame.setColor(Color.GREEN);  
	        paintFrame.setAntiAlias(true);  
	        paintFrame.setFilterBitmap(true);
	        paintFrame.setStrokeWidth(3f);
	        SilverlightImageUtils.translate(this,defaultX, defaultY);
	}
}
