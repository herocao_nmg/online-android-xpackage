
package x.silverlight;
import java.util.LinkedList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * 支持資源旋轉,縮放,平移的View
 * @author Hero
 * @since 2014-01-08
 */
public class SilverlightImageView extends View {  
  
    private Context mContext ;  
    public int lastOper = SilverlightImageConfig.OPER_DEFAULT;  
    public int currentControl = SilverlightImageConfig.CTR_NONE;
	private LinkedList<SilverlightImageMeta> mSilverlightImageMetas;
	private Bitmap faceBitmap;
	private SilverlightImageMeta target;  
	private SilverlightImageMeta lastSeletedTarget=null;
	private float currentDistance;
	private float currentDegree;
      
    @SuppressWarnings("deprecation")
	public SilverlightImageView(Context context,LinkedList<SilverlightImageMeta> silverlightImageMetas,Bitmap faceBitmap){  
        super(context);  
        this.mContext = context ;  
        this.mSilverlightImageMetas=silverlightImageMetas;
        this.faceBitmap=faceBitmap;
        setBackgroundDrawable(new BitmapDrawable(getResources(),faceBitmap));
    }  
      
    public SilverlightImageView(Context context, AttributeSet attrs) {  
        super(context, attrs);  
        this.mContext = context ;  
    }  
      
    @Override  
    public boolean dispatchTouchEvent(MotionEvent event) {  
        int eX = (int)event.getX();  
        int eY = (int)event.getY();  
        if(event.getAction()==MotionEvent.ACTION_DOWN){
        	target = SilverlightImageUtils.getTarget(mSilverlightImageMetas, eX, eY);
        }else if(event.getAction()==MotionEvent.ACTION_POINTER_DOWN || event.getAction()==MotionEvent.ACTION_POINTER_2_DOWN){
        	 currentDistance = SilverlightImageUtils.computeDistance(event.getX(), event.getY(), event.getX(1), event.getY(1));
        	currentDegree= SilverlightImageUtils.computeDegree(new PointF(event.getX(),event.getY()), new PointF(event.getX(1), event.getY(1)));
        }
        
    	if(null!=lastSeletedTarget && lastSeletedTarget!=target){
    		lastSeletedTarget.isSelected=false;
    	}
    	
    	lastSeletedTarget=target;
        if(null==target){
    		invalidate();
    		return true;
    	}
    	invalidate();
        
        int operType = SilverlightImageConfig.OPER_DEFAULT;  
        int[] values = SilverlightImageUtils.getOperationType(event, target,currentControl,lastOper);
        currentControl=values[0];
        operType=values[1];
        
          System.out.println("dispatchTouchEvent,operType="+operType);
        switch (operType) {  
        case SilverlightImageConfig.OPER_TRANSLATE:  
        	if(event.getPointerCount()==1){
        		SilverlightImageUtils.translate(target,eX, eY);
        	} 
            break;  
        case SilverlightImageConfig.OPER_SCALE:  
        	SilverlightImageUtils.scale(event,target,currentControl);  
            break;  
        case SilverlightImageConfig.OPER_ROTATE:  
        	SilverlightImageUtils.rotate(event, target);
            break;  
        case  SilverlightImageConfig.OPER_DELETE:
        	if(null!=target){
        	mSilverlightImageMetas.remove(target);
        	SilverlightImageUtils.recycle(target);
        	target=null;
        	 invalidate();
        	}
        	 lastOper = operType;  
        	 return true;
        	 
        case SilverlightImageConfig.OPER_SCALE_ROTATE:
        	if(event.getPointerCount()>=2){
        	currentDistance=SilverlightImageUtils.scale(event,target,currentDistance);  
        	currentDegree=SilverlightImageUtils.rotate(event, target,currentDegree);
        	}
        	break;
        }  
      
        target.lastPoint.x = eX;  
        target.lastPoint.y = eY;    
        lastOper = operType;  
        
        if(event.getAction()!=MotionEvent.ACTION_UP){
        	invalidate();    
        }
        return true;  
    }  
  
      
    @Override  
    public void onDraw(Canvas canvas){  
    	SilverlightImageUtils.doDraw(canvas, mSilverlightImageMetas);
    }  
   
}  