package hk.nmg.face;

import java.util.LinkedList;

import x.silverlight.SilverlightImageMeta;
import x.silverlight.SilverlightImageView;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

public class SilverlightViewActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		initialization();
	}

	private void initialization() {

		int len=5;
		int x=50,y=60;
		int value=100;
		
		LinkedList<SilverlightImageMeta> silverlightImageMetas=new LinkedList<SilverlightImageMeta>();
		
		for(int i=0;i<len;i++){
			
			if(i==0){
				Bitmap bitmap=BitmapFactory.decodeResource(getResources(), R.drawable.m082x);;
				SilverlightImageMeta meta=new SilverlightImageMeta(this, bitmap, i*x+value, i*y+value);
				meta.priority=i;
				silverlightImageMetas.add(meta);
				meta.uri="С�޺Ͳ�ݮ";
			}else if(i==1){
				Bitmap bitmap=BitmapFactory.decodeResource(getResources(), R.drawable.m092x);;
				SilverlightImageMeta meta=new SilverlightImageMeta(this, bitmap, i*x+value, i*y+value);
				meta.priority=i;
				silverlightImageMetas.add(meta);
				meta.uri="С��";
			}else if(i==2){
				Bitmap bitmap=BitmapFactory.decodeResource(getResources(), R.drawable.m012x);;
				SilverlightImageMeta meta=new SilverlightImageMeta(this, bitmap, i*x+value, i*y+value);
				meta.priority=i;
				meta.uri="J��";
				silverlightImageMetas.add(meta);
			}else if(i==3){
				Bitmap bitmap=BitmapFactory.decodeResource(getResources(), R.drawable.m042x);;
				SilverlightImageMeta meta=new SilverlightImageMeta(this, bitmap, i*x+value, i*y+value);
				meta.priority=i;
				silverlightImageMetas.add(meta);
			}else if(i==4){
				Bitmap bitmap=BitmapFactory.decodeResource(getResources(), R.drawable.m052x);;
				SilverlightImageMeta meta=new SilverlightImageMeta(this, bitmap, i*x+value, i*y+value);
				meta.priority=i;
				silverlightImageMetas.add(meta);
			}else{
				Bitmap bitmap=BitmapFactory.decodeResource(getResources(), R.drawable.m062x);;
				SilverlightImageMeta meta=new SilverlightImageMeta(this, bitmap, i*x+value, i*y+value);
				meta.priority=i;
				silverlightImageMetas.add(meta);
			}
		}
		
		Bitmap faceBitmap=BitmapFactory.decodeResource(getResources(), R.drawable.guide_recommend_bg_dayima);
		SilverlightImageView view=new SilverlightImageView(this, silverlightImageMetas, faceBitmap);
		
		setContentView(view);
	}
	
}
