package nmg.util;

/**
 * 文件,异或
 * 
 * @author ethan
 * 
 */
public class XOR {
	
	 public final static int[] xor_key = {0x13, 0x47, 0xe8, 0x99, 0xE3, 0xf4, 0x98, 0x4F, 0x32, 0x63, 0xa7, 0x33, 0x91, 0xE3, 0x7F, 0x65,0x94, 0x5E, 0x39, 0x64, 0x89, 0xf2, 0x3a, 0xa4, 0x86, 0xAE, 0x45, 0x91, 0xE3, 0x49, 0x63, 0xFE};
	
	/**
	 * 异或一次
	 * @param data
	 * @param key
	 * @return
	 */
	public static byte[] encrypt(byte[] data) {
		if(data==null){
			return null;
		}
		int keyLength = xor_key.length;
		int dataLength = data.length;
		for (int x = 0; x < dataLength; x++) {
			data[x] ^=  xor_key[x%keyLength];
		}
		return data;
	}
}
