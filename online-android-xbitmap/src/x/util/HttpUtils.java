/*
 *Copyright (c) 2013. ccx (ccxandroid@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package x.util;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;

import android.text.TextUtils;

/**
 * Http工具類
 * @author Hero
 * @since 2013-10-21
 */
public class HttpUtils {

	private static final String TAG = "HttpUtils";

	private HttpUtils(){
	}
	
	/**
	 * 使用HttpClient的方式下載資源
	 * 
	 * @param uri
	 * @return
	 */
	public static byte[] downloadByHttpClient(String uri) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(uri);
			HttpResponse httpResponse = httpClient.execute(httpPost);

			int code = httpResponse.getStatusLine().getStatusCode();
			if (code == 200) {
				InputStream is = httpResponse.getEntity().getContent();
				byte[] data = IOUtils.getBytes(is);
				IOUtils.close(is);
				return data;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 使用HttpURLConnection方式下載資源
	 * 
	 * @param path
	 * @return
	 */
	public static byte[] downloadByHttpURLConn(String path) {
		byte[] data = null;
		InputStream mInputStream = null;
		HttpURLConnection mConnection = null;
		try {
			URL url = new URL(path);
			mConnection = (HttpURLConnection) url.openConnection();
			mConnection.setDoInput(true);
			mConnection.setRequestMethod("POST");
			mConnection.setConnectTimeout(30 * 1000);
			mInputStream = mConnection.getInputStream();
			if (mConnection.getResponseCode() == 200) {
				data = IOUtils.getBytes(mInputStream);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			IOUtils.close(mInputStream);
		}
		return data;
	}

	/**
	 * 從Http協議響應報文頭中得到當前下載資源的文件名稱
	 * @param response
	 * @return
	 */
	public static String getFileNameFromHttpResponse(HttpResponse response) {
		if (response == null)
			return null;
		String result = null;
		Header header = response.getFirstHeader("Content-Disposition");
		if (header != null) {
			for (HeaderElement element : header.getElements()) {
				NameValuePair fileNamePair = element
						.getParameterByName("filename");
				if (fileNamePair != null) {
					result = fileNamePair.getValue();
					result = CharsetUtils.toCharset(result, HTTP.UTF_8,
							result.length());// 嘗試轉換亂碼
					break;
				}
			}
		}
		return result;
	}

	/**
	 * 從Http協議響應報文頭中獲取當前下載資源的編碼（字符集）
	 * @param response
	 * @return
	 */
	public static String getCharsetFromHttpResponse(HttpResponse response) {
		if (response == null)
			return null;
		String result = null;
		Header header = response.getEntity().getContentType();
		if (header != null) {
			for (HeaderElement element : header.getElements()) {
				NameValuePair charsetPair = element
						.getParameterByName("charset");
				if (charsetPair != null) {
					result = charsetPair.getValue();
					break;
				}
			}
		}

		boolean isSupportedCharset = false;
		if (!TextUtils.isEmpty(result)) {
			try {
				isSupportedCharset = Charset.isSupported(result);
			} catch (Exception e) {
			}
		}

		return isSupportedCharset ? result : null;
	}

	private static final int STRING_BUFFER_LENGTH = 100;

	public static long sizeOfString(String str, String charset)
			throws UnsupportedEncodingException {
		if (TextUtils.isEmpty(str)) {
			return 0;
		}
		int len = str.length();
		if (len < STRING_BUFFER_LENGTH) {
			return str.getBytes(charset).length;
		}
		long size = 0;
		for (int i = 0; i < len; i += STRING_BUFFER_LENGTH) {
			int end = i + STRING_BUFFER_LENGTH;
			end = end < len ? end : len;
			String temp = getSubString(str, i, end);
			size += temp.getBytes(charset).length;
		}
		return size;
	}

	public static String getSubString(String str, int start, int end) {
		return new String(str.substring(start, end));
	}

	public static StackTraceElement getCurrentStackTraceElement() {
		return Thread.currentThread().getStackTrace()[3];
	}

	public static StackTraceElement getCallerStackTraceElement() {
		return Thread.currentThread().getStackTrace()[4];
	}

	private static TrustManager[] trustAllCerts;

	public static void trustAllSSLForHttpsURLConnection() {
		if (trustAllCerts == null) {
			trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs,
						String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs,
						String authType) {
				}
			} };
		}
		final SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance("TLS");
			sslContext.init(null, trustAllCerts, null);
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext
					.getSocketFactory());
		} catch (Exception e) {
			e.printStackTrace();
		}
		HttpsURLConnection
				.setDefaultHostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	}

}
