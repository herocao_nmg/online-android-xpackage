package x.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;

/**
 * 文件操作工具類
 * @author Hero
 * @since 2014-01-20
 */
public final class FileUtils {
	private FileUtils(){ }
	
	public static final long minExternalStorageAvailableSize=1024*1024*10L;  // 10MB
	public static final int bufferSize=1024*8;

	/**
	 * 獲取指定路徑的可用空間大小,單位為byte,可以使用{@link  format}進行格式化
	 * @param path
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static long getUsableSpace(File path){
		if(null==path)return -1;
		StatFs statFs=new StatFs(path.getAbsolutePath());
		return Long.valueOf(statFs.getBlockCount()) * Long.valueOf(statFs.getBlockSize());
	}
	
	/**
	 * 刪除文件,如果當前是目錄就遍歷刪除所有的子目錄和文件,如果當前是文件就直接刪除
	 * @param file 文件
	 * @param isDeleteSelf 如果當前是目錄,若為true就也把當前目錄刪除,若為false不刪除當前目錄
	 */
/*	public static void delete(File file, boolean isDeleteSelf) { 
		if (null==file || !file.exists()) return;
			
		if (file.isFile()) {
			 file.delete();
			 return;
		}
		File[] files = file.listFiles();
		for(File childFile : files){
			 delete(childFile, true);
		}
		if (isDeleteSelf){
			file.delete();
			return;
		}
	}*/
	
	/**
	 * 刪除文件,如果當前是目錄就遍歷刪除所有的子目錄和文件,如果當前是文件就直接刪除,如果不需要刪除的文件名稱不為空,就不會刪除跟目錄
	 * @param file
	 * @param nonDeleteFileName 不需要刪除的文件名稱,如果當前是目錄才有效
	 * @param isDeleteSelf 如果當前是目錄,若為true就也把當前目錄刪除,前提是 nonDeleteFileName 為空,若為false不刪除當前目錄
	 */
	public static void delete(File file , String nonDeleteFileName, boolean isDeleteSelf) { 
		if (null==file || !file.exists()) return;
			
		if (file.isFile()) {
			 file.delete();
			 return;
		}
		File[] files = file.listFiles();
		for(File childFile : files){
			if(!(!TextUtils.isEmpty(nonDeleteFileName) && childFile.getName().equals(nonDeleteFileName))){
				 delete(childFile,nonDeleteFileName, true);
			}
		}
		if ( TextUtils.isEmpty(nonDeleteFileName) && isDeleteSelf){
			file.delete();
			return;
		}
	}
	
	/**
	 * 刪除文件
	 * @param file
	 */
	public static void delete(File file){
		delete(file,null, true);
	}
	
	/**
	 * 刪除文件
	 * @param filename
	 */
	public static void delete(String filename){
		 delete(filename, true);
	}
	
	/**
	 * 刪除文件,如果當前是目錄就遍歷刪除所有的子目錄和文件,如果當前是文件就直接刪除
	 * @param filename 文件全路徑名稱
	 * @param isDeleteSelf 如果當前是目錄,若為true就也把當前目錄刪除,若為false不刪除當前目錄
	 */
	public static void delete(String filename, boolean isDeleteSelf) { 
		if(TextUtils.isEmpty(filename)) return;
		 delete(new File(filename),null, isDeleteSelf);
	}
	
	/**
	 * 刪除目錄中的文件,不刪除指定的文件
	 * @param dir 絕對路徑文件目錄
	 * @param nonDeleteFileName 不需要刪除的文件名稱,不需要包含路徑,例如: test.jpg
	 */
	public static void delete(File dir,String nonDeleteFileName){
		delete(dir,nonDeleteFileName,false);
	}
	
	/**
	 * 保存文件
	 * @param context 上下文
	 * @param data 數據
	 * @param file 保存數據的絕對路徑文件
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean save(Context context,byte[] data,File file,boolean whenExceptionSave2Rom) throws IOException{
		if(null==data || null==file) return false;
		OutputStream os=null;
		if(isExternalStoragePath(file.getPath()) &&( !isMountedExternalStorage() || isFulledExternalStorage()) && null!=context && whenExceptionSave2Rom){
			 os=new FileOutputStream(new File(context.getCacheDir(),getFileName(file)));
				os.write(data);
				close(os);
				return true;
		}else{
			 os=new FileOutputStream(file);
			os.write(data);
			close(os);
			return true;
		}
	}
	
	/**
	 * 保存文件
	 * @param data 數據
	 * @param file 保存數據的絕對路徑文件
	 * @return
	 * @throws IOException
	 */
	public static boolean save(byte[] data,File file) throws IOException{
		return save(null, data, file, false);
	}
	
	/**
	 * 保存文件
	 * @param data 數據
	 * @param filename 保存數據的絕對路徑文件名稱
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean save(byte[] data,String filename,boolean whenExceptionSave2Rom) throws IOException{
		return save(null, data, filename, whenExceptionSave2Rom);
	}
	
	/**
	 * 保存文件
	 * @param context 上下文
	 * @param data 數據
	 * @param filename 保存數據的絕對路徑文件名稱
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean save(Context context,byte[] data,String filename,boolean whenExceptionSave2Rom) throws IOException{
		if(TextUtils.isEmpty(filename)) return false;
		return save(context, data, new File(filename), whenExceptionSave2Rom);
	}
	
	/**
	 * 保存文件
	 * @param data 數據
	 * @param filename 保存數據的絕對路徑文件名稱
	 * @return
	 * @throws IOException
	 */
	public static boolean save(byte[] data,String filename) throws IOException{
		if(TextUtils.isEmpty(filename)) return false;
		return save(null, data, new File(filename), true);
	}
	
	/**
	 * 保存文件
	 * @param context 上下文
	 * @param is 數據InputStream
	 * @param file 保存InputStream的絕對路徑文件
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @param isCloseInputStream 是否關閉InputStream,如果為true則關閉,為false則不關閉
	 * @return
	 * @throws IOException
	 */
	public static boolean save(Context context,InputStream is,File file,boolean whenExceptionSave2Rom,boolean isCloseInputStream) throws IOException{
		if(null==is || null==file) return false;
		return save(context, inputStream2Bytes(is, isCloseInputStream), file, whenExceptionSave2Rom);
	}
	
	/**
	 * 保存文件
	 * @param is 數據InputStream
	 * @param file 保存InputStream的絕對路徑文件
	 * @return
	 * @throws IOException
	 */
	public static boolean save(InputStream is,File file) throws IOException{
		return save(null, is, file, false,false);
	}
	
	/**
	 * 保存文件
	 * @param is 數據InputStream
	 * @param file 保存InputStream數據的絕對路徑文件
	 * @param isCloseInputStream 是否關閉InputStream,如果為true則關閉,為false則不關閉
	 * @return
	 * @throws IOException
	 */
	public static boolean save(InputStream is,File file,boolean isCloseInputStream) throws IOException{
		return save(null, is, file, false,isCloseInputStream);
	}
	
	/**
	 * 保存文件
	 * @param is 數據InputStream
	 * @param filename 保存InputStream數據的絕對路徑文件名稱
	 * @param isCloseInputStream 是否關閉InputStream,如果為true則關閉,為false則不關閉
	 * @return
	 */
	public static boolean save(InputStream is,String filename,boolean isCloseInputStream){
		return save(is, filename, isCloseInputStream);
	}
	
	/**
	 * 保存文件
	 * @param context 上下文
	 * @param is 數據InputStream
	 * @param filename 保存InputStream數據的絕對路徑文件名稱
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @param isCloseInputStream 是否關閉InputStream,如果為true則關閉,為false則不關閉
	 * @return
	 * @throws IOException
	 */
	public static boolean save(Context context,InputStream is,String filename,boolean whenExceptionSave2Rom,boolean isCloseInputStream) throws IOException{
		if(TextUtils.isEmpty(filename)) return false;
		 return save(context, is, new File(filename), whenExceptionSave2Rom, isCloseInputStream);
	}
	
	/**
	 * 保存文件
	 * @param is 數據InputStream
	 * @param filename 保存InputStream數據的絕對路徑文件名稱
	 * @return
	 * @throws IOException
	 */
	public static boolean save(InputStream is,String filename) throws IOException{
		if(TextUtils.isEmpty(filename)) return false;
		return save(is, new File(filename), false);
	}
	
	/**
	 * 獲取文件的目錄
	 * @param filename
	 * @return
	 */
	public static String getDirectory(String filename){
		if(TextUtils.isEmpty(filename) || !filename.contains(File.separator)) return null;
		return filename.substring(0,filename.lastIndexOf(File.separator));
	}
	
	/**
	 * 獲取文件目錄
	 * @param file
	 * @return
	 */
	public static String getDirectory(File file){
		if(null==file || !file.getPath().contains(File.separator)) return null;
		return file.getPath().substring(0,file.getPath().lastIndexOf(File.separator));
	}
	
	/**
	 * 創建目錄
	 * @param directory
	 * @param createParents 如果當前目錄的父級目錄不存在,是否需要創建,如果為true則創建,如果為false則不會創建
	 * @return
	 * @throws IOException
	 */
	public static boolean makeDirectory(File directory, boolean createParents)
			throws IOException {
		boolean created;
		if (createParents) {
			created = directory.mkdirs();
		} else {
			created = directory.mkdir();
		}
		return created;
	}
	
	/**
	 * 創建目錄,如果當前目錄的父級目錄不存在,則會自動創建
	 * @param directory
	 * @return
	 * @throws IOException
	 */
	public static boolean makeDirectory(File directory)
			throws IOException {
		return makeDirectory(directory, true);
	}
	
	/**
	 * 創建目錄
	 * @param directory
	 * @param createParents 如果當前目錄的父級目錄不存在,是否需要創建,如果為true則創建,如果為false則不會創建
	 * @return
	 * @throws IOException
	 */
	public static boolean makeDirectory(String directory, boolean createParents)
			throws IOException {
		if(TextUtils.isEmpty(directory)) return false;
		return makeDirectory(new File(directory), createParents);
	}
	
	/**
	 * 創建目錄
	 * @param directory 絕對路徑
	 * @return
	 * @throws IOException
	 */
	public static boolean makeDirectory(String directory) throws IOException{
		return makeDirectory(directory, true);
	}
	
	/**
	 * 創建文件
	 * @param filePath 絕對路徑
	 * @param createParents 如果當前文件的父級目錄不存在,是否需要創建,如果為true則創建,如果為false則不會創建
	 * @return
	 * @throws IOException
	 */
	 public static boolean createNewFile(File filePath, boolean createParents) throws IOException {
		File parent = filePath.getParentFile();
		if (createParents && !parent.exists()) {
			parent.mkdirs();
		}
		return filePath.createNewFile();
	    }
	 
	 /**
	  * 在SD卡中創建一個文件
	  * @param filename 文件名稱,不需要包含SD卡的路徑,例如 book.txt , 保存在 /SD卡路徑/book.txt
	  * @return
	  * @throws IOException
	  */
	public static boolean createFileAtExternalStorage(String filename)
			throws IOException {
		if (TextUtils.isEmpty(filename))
			return false;
		File file = new File(Environment.getExternalStorageDirectory(),
				filename);
		return file.createNewFile();
	}
	
	/**
	 * 在當前App的緩存目錄下創建一個文件
	 * @param context 上下文
	 * @param filename 文件名稱,不需要包含App的緩存路徑(context.getCacheDir()),例如 book.txt ,保存在 /context.getCacheDir()/book.txt
	 * @return
	 * @throws IOException
	 */
	public static boolean createFileAtCacheDir(Context context,String filename)
			throws IOException {
		if (null==context || TextUtils.isEmpty(filename))
			return false;
		File file = new File(context.getCacheDir(),
				filename);
		return file.createNewFile();
	}
	
	/**
	 * 拷貝文件
	 * @param src 絕對路徑的源文件
	 * @param dst 絕對路徑的目標文件
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(File src, File dst) throws IOException {
		if(null==src || null==dst) return false;
		FileInputStream fis = new FileInputStream(src);
		FileOutputStream fos = new FileOutputStream(dst);
		FileChannel fc = fis.getChannel();
		MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0,src.length());
		close(fc);
		close(fis);
		if (!dst.exists()) {
			File dir = new File(getDirectory(dst.getPath()));
			makeDirectory(dir);
			dir = null;
		}
		FileChannel foc = fos.getChannel();
		foc.write(mbb);
		close(foc);
		close(fos);
		mbb.clear();
		mbb = null;
		return true;
	}
	
	/**
	 * 拷貝文件
	 * @param src 絕對路徑源文件名稱
	 * @param dst 絕對路徑目標文件名稱
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(String src, String dst) throws IOException {
		if(TextUtils.isEmpty(src) || TextUtils.isEmpty(dst)) return false;
		return copy(new File(src), new File(dst));
	}
	
	/**
	 * 拷貝文件
	 * @param src 絕對路徑源文件
	 * @param dst 絕對路徑目標文件名稱
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(File src, String dst) throws IOException {
		if(null==src || TextUtils.isEmpty(dst)) return false;
		return copy( src, new File(dst));
	}
	
	/**
	 * 拷貝文件
	 * @param src 絕對路徑源文件名稱
	 * @param dst 絕對路徑目標文件
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(String src, File dst) throws IOException {
		if(null==dst || TextUtils.isEmpty(src)) return false;
		return copy( new File(src), dst);
	}
	
	/**
	 * 拷貝文件
	 * @param context 上下文
	 * @param src 絕對路徑源文件
	 * @param dst 絕對路徑目標文件
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(Context context,File src,File dst,boolean whenExceptionSave2Rom) throws IOException{
		if(null==src || null==dst) return false;
		if(isExternalStoragePath(dst.getPath()) &&( !isMountedExternalStorage() || isFulledExternalStorage()) && null!=context && whenExceptionSave2Rom){
			return copy(src, new File(context.getCacheDir(),getFileName(dst)));
		}else{
			return copy(src, dst);
		}
	}
	
	/**
	 * 拷貝文件
	 * @param context 上下文
	 * @param src 絕對路徑源文件名稱
	 * @param dst 絕對路徑目標文件名稱
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(Context context,String src,String dst,boolean whenExceptionSave2Rom) throws IOException{
		if(TextUtils.isEmpty(src) || TextUtils.isEmpty(dst)) return false;
		return copy(context, new File(src), new File(dst), whenExceptionSave2Rom);
	}
	
	/**
	 * 拷貝文件
	 * @param context 上下文
	 * @param src 絕對路徑源文件名稱
	 * @param dst 絕對路徑目標文件名稱
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(Context context,String src,File dst,boolean whenExceptionSave2Rom) throws IOException{
		if(TextUtils.isEmpty(src) || null==dst) return false;
		return copy(context, new File(src),dst, whenExceptionSave2Rom);
	}
	
	/**
	 * 拷貝文件
	 * @param context 上下文
	 * @param src 絕對路徑源文件
	 * @param dst 絕對路徑目標文件名稱
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean copy(Context context,File src,String dst,boolean whenExceptionSave2Rom) throws IOException{
		if(TextUtils.isEmpty(dst) || null==src) return false;
		return copy(context, src,new File(dst), whenExceptionSave2Rom);
	}
	
	/**
	 * 將InputStream轉換為byte[]
	 * @param is 數據InputStream
	 * @param isCloseInputStream 是否關閉InputStream,如果為true則關閉,為false則不關閉
	 * @return
	 * @throws IOException
	 */
	public static byte[] inputStream2Bytes(InputStream is,boolean isCloseInputStream) throws IOException{
		if(null==is)return null;
		byte[] buffer=new byte[bufferSize];
		int len=-1;
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		while((len=is.read(buffer))!=-1){
			baos.write(buffer, 0, len);
		}
		baos.flush();
		byte[] data = baos.toByteArray();
		close(baos);
		if(isCloseInputStream) close(is);
		return data;
	}
	
	/**
	 * 將InputStream轉換為byte[],默認沒有關閉InputStream
	 * @param is 數據InputStream
	 * @return
	 * @throws IOException
	 */
	public static byte[] inputStream2Bytes(InputStream is) throws IOException{
		 return inputStream2Bytes(is, false);
	}
	
	/**
	 * 將byte[]轉換成InputStream
	 * @param data byte[]數據
	 * @return
	 */
	public static InputStream bytes2InputStream(byte[] data){
		if(null==data || data.length<=0) return null;
		return new ByteArrayInputStream(data);
	}
	
	/**
	 *獲取文件的後綴,例如: 11/test.txt 得到 txt
	 * @param filename
	 * @return
	 */
	public static String getSuffix(String filename){
		if(TextUtils.isEmpty(filename)) return null;
		return filename.substring(filename.lastIndexOf(".")+1, filename.length());
	}
	
	/**
	 * 獲取SD卡可用空間大小,單位為byte,可以使用 {@link format} 格式化
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static long getExternalStorageAvailableSize(){  
	     StatFs sf = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());   
	     return Long.valueOf(sf.getBlockSize()) * Long.valueOf(sf.getAvailableBlocks());
	   }
	
	/**
	 * 關閉資源
	 * @param closeable
	 */
	public static void close(Closeable closeable) {
		if (null != closeable) {
			try {
				closeable.close();
				closeable = null;
			} catch (Exception e) {
				closeable = null;
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 判斷文件路徑是否為SD卡路徑
	 * @param filename
	 * @return
	 */
	public static boolean isExternalStoragePath(String filename){
		if(TextUtils.isEmpty(filename)) return false;
		return filename.startsWith(Environment.getExternalStorageDirectory().getPath());
	}
	
	/**
	 * SD卡空間是否滿了
	 * @return
	 */
	public static boolean isFulledExternalStorage(){
		if(isMountedExternalStorage()){
			if(getExternalStorageAvailableSize()<minExternalStorageAvailableSize){
				return true;
			} 
			return false;
		}
		return true;
	}
	
	/**
	 * 移動文件
	 * @param context 上下文
	 * @param src 絕對路徑源文件
	 * @param dst 絕對路徑目標文件
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean move(Context context,File src,File dst,boolean whenExceptionSave2Rom) throws IOException{
		boolean result=copy(context, src, dst, whenExceptionSave2Rom);
		if(result) src.delete();
		return result;
	}
	
	/**
	 * 移動文件
	 * @param context 上下文
	 * @param src 絕對路徑源文件名稱
	 * @param dst 絕對路徑目標文件名稱
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean move(Context context,String src,String dst,boolean whenExceptionSave2Rom) throws IOException{
		boolean result=copy(context, src, dst, whenExceptionSave2Rom);
		if(result)new File(src).delete();
		return result;
	}
	
	/**
	 * 移動文件
	 * @param context 上下文
	 * @param src 絕對路徑源文件
	 * @param dst 絕對路徑目標文件名稱
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean move(Context context,File src,String dst,boolean whenExceptionSave2Rom) throws IOException{
		boolean result=copy(context, src, dst, whenExceptionSave2Rom);
		if(result)src.delete();
		return result;
	}
	
	/**
	 * 移動文件
	 * @param context
	 * @param src 絕對路徑源文件名稱
	 * @param dst 絕對路徑目標文件
	 * @param whenExceptionSave2Rom 如果是把數據保存到SD卡中,當發生異常(SD卡沒有掛載或者SD卡的空間不夠)時,如果為true就自動保存到System的Rom中,如果為false則不會
	 * @return
	 * @throws IOException
	 */
	public static boolean move(Context context,String src,File dst,boolean whenExceptionSave2Rom) throws IOException{
		boolean result=copy(context, src, dst, whenExceptionSave2Rom);
		if(result)new File(src).delete();
		return result;
	}
	
	/**
	 * 移動文件
	 * @param src 絕對路徑源文件
	 * @param dst 絕對路徑目標文件
	 * @return
	 * @throws IOException
	 */
	public static boolean move(File src,File dst) throws IOException{
		boolean result=copy(src, dst);
		if(result) src.delete();
		return result;
	}
	
	/**
	 * 移動文件
	 * @param src 絕對路徑源文件
	 * @param dst 絕對路徑目標文件名稱
	 * @return
	 * @throws IOException
	 */
	public static boolean move(File src,String dst) throws IOException{
		boolean result=copy(src, dst);
		if(result) src.delete();
		return result;
	}
	
	/**
	 * 移動文件
	 * @param src 絕對路徑目標文件名稱
	 * @param dst 絕對路徑目標文件
	 * @return
	 * @throws IOException
	 */
	public static boolean move(String src,File dst) throws IOException{
		boolean result=copy(src, dst);
		if(result)new File(src).delete();
		return result;
	}
	
	/**
	 * 移動文件
	 * @param src 絕對路徑源文件名稱
	 * @param dst 絕對路徑目標文件名稱
	 * @return
	 * @throws IOException
	 */
	public static boolean move(String src,String dst) throws IOException{
		boolean result=copy(src, dst);
		if(result)new File(src).delete();
		return result;
	}
	
	/**
	 * 讀取文件,注意這是以字符流的方式讀取文件,對於圖片/音頻/視頻等文件不可以使用,這個為讀取普通字符文件提供的方法
	 * @param filePath
	 * @param charsetName 編碼名稱,例如 utf-8 等
	 * @return
	 * @throws IOException
	 */
	public static String read(String filePath, String charsetName) throws IOException {
        File file = new File(filePath);
        if (file == null || !file.isFile()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        BufferedReader reader = null;
            InputStreamReader is = new InputStreamReader(new FileInputStream(file), charsetName);
            reader = new BufferedReader(is);
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (!sb.toString().equals("")) {
                    sb.append("\r\n");
                }
                sb.append(line);
            }
            close(reader);
            close(is);
            return sb.toString();
    }
	
	/**
	 * 讀取文件
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public static byte[] read(String filePath) throws IOException{
		if(TextUtils.isEmpty(filePath)) return null;
		FileInputStream fis=new FileInputStream(filePath);
		byte[] buff=new byte[bufferSize];
		int len=-1;
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		while((len=fis.read(buff))!=-1){
				baos.write(buff, 0, len);
		}
		baos.flush();
		byte[] data=baos.toByteArray();
		close(baos);
		close(fis);
		return data;
	}
	
	/**
	 * 讀取文件
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static byte[] read(File file) throws IOException{
		if(null==file)return null;
		return read(file.getPath());
	}
	
	/**
	 * 判斷是否掛載SD卡
	 * @return
	 */
	public static boolean isMountedExternalStorage(){
		return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
	}
	
	/**
	 * 獲取文件名稱,例如 /sdcard/carche/data/test.txt 得到 test.txt
	 * @param file
	 * @return
	 */
	public  static String getFileName(File file) {
		if (null==file)	return null;
		return file.getPath().substring(file.getPath().lastIndexOf(File.separator) + 1);
	}
	
	/**
	 *  獲取文件名稱,例如 /sdcard/carche/data/test.txt 得到 test.txt
	 * @param filename
	 * @return
	 */
	public  static String getFileName(String filename) {
		if (TextUtils.isEmpty(filename))	return null;
		return filename.substring(filename.lastIndexOf(File.separator) + 1);
	}
	
	/**
	 * 獲取文件名稱
	 * @param uri
	 * @param isContainSuffix 是否包含後綴,如果為true則包含,如果為false則不包含,例如 test 和 test.jpg
	 * @return
	 */
	public  static String getFileName(String uri,boolean isContainSuffix) {
		if (null == uri)
			return null;
		if(isContainSuffix)
			return uri.substring(uri.lastIndexOf("/") + 1);
		return uri.substring(uri.lastIndexOf("/") + 1,uri.lastIndexOf("."));
	}
	
	
	/**
	 * 獲取文件的後綴
	 * @param filename
	 * @param isCantainDot 是否包含點(.),如果為true則包含,false則不包含 ,例如 .jpg 和 jpg
	 * @return
	 */
	public static String getSuffix(String filename,boolean isCantainDot){
		if(TextUtils.isEmpty(filename)) return null;
		if(isCantainDot) 
			return filename.substring(filename.lastIndexOf("."), filename.length());
		return filename.substring(filename.lastIndexOf(".")+1, filename.length());
	}
	
	/**
	 * 格式化
	 * @param size
	 * @return
	 */
	public static String format(long size){
		DecimalFormat df=new DecimalFormat("####.00");
		if(size<0)return String.valueOf(size);
		if(size<1024f){
			return df.format(size)+"B";
		}else if(size<Math.pow(1024f, 2)){    
			double kbSize=size/(1024f);   
			return df.format(kbSize)+"KB";
		}else if(size<Math.pow(1024f, 3)){
			double mbSize=size/Math.pow(1024f, 2);
			return df.format(mbSize)+"MB";
		}else if(size<Math.pow(1024f, 4)){
			double gbSize=size/Math.pow(1024f, 3);
			return df.format(gbSize)+"GB";
		}else if(size<Math.pow(1024f, 5)){
			double tbSize=size/Math.pow(1024f, 4);
			return df.format(tbSize)+"TB";
		}else{
			return "size error";
		}
	}
	
}
