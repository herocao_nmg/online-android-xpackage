/*
 *Copyright (c) 2013. ccx (ccxandroid@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package x.util;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.InputStream;

import android.database.Cursor;

/**
 * IO工具類
 * 
 * @author Hero
 * @since 2013-10-21
 */
public class IOUtils {

	private IOUtils(){
		
	}
	
	/**
	 * 根據InputStream得到byte[]
	 * 
	 * @param is
	 *            InputStream
	 * @return byte[]
	 */
	public static byte[] getBytes(InputStream is) {
		if (is == null) {
			return null;
		}
		try {
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

			int len = -1;
			byte[] buff = new byte[1024];

			while ((len = is.read(buff)) != -1) {
				byteArrayOutputStream.write(buff, 0, len);
			}
			byteArrayOutputStream.flush();
			byteArrayOutputStream.close();
			byte[] data = byteArrayOutputStream.toByteArray();
			byteArrayOutputStream = null;

			return data;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * 關閉Closeable操作
	 * 
	 * @param closeable
	 */
	public static void close(Closeable closeable) {
		if (null != closeable) {
			try {
				closeable.close();
				closeable = null;
			} catch (Exception e) {
				closeable = null;
				e.printStackTrace();
			}
		}
	}

	/**
	 * 關閉Cursor操作
	 * 
	 * @param cursor
	 */
	public static void close(Cursor cursor) {
		if (null != cursor && !cursor.isClosed()) {
			try {
				cursor.close();
				cursor = null;
			} catch (Exception e) {
				cursor = null;
				e.printStackTrace();
			}
		}
	}
}
