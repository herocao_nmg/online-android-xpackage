package x.util;

import java.lang.reflect.Field;
import java.util.LinkedList;

import android.content.Context;
import android.view.WindowManager;
import android.webkit.WebView;

/**
 * WebView工具類
 * @author Hero
 * @since 2013-11-05
 */
public class WebViewUtils {

	private static final String TAG = "WebViewUtils";
	
	public static LinkedList<WebView> webViews=new LinkedList<WebView>();
    
    private WebViewUtils() {
    }

	/**
	 * 通過反射調用WebView底層代碼,把WebView的引用回收掉,但是有的版本會報錯
	 * @param windowManager
	 */
	public static void setConfigCallback(WindowManager windowManager) {
	    try {
	        Field field = WebView.class.getDeclaredField("mWebViewCore");
	        field = field.getType().getDeclaredField("mBrowserFrame");
	        field = field.getType().getDeclaredField("sConfigCallback");
	        field.setAccessible(true);
	        Object configCallback = field.get(null);

	        if (null == configCallback) {
	            return;
	        }

	        field = field.getType().getDeclaredField("mWindowManager");
	        field.setAccessible(true);
	        field.set(configCallback, windowManager);
	        
	        field=null;
	    } catch(Exception e) {
	    	e.printStackTrace();
	    }
	}
	
	/**
	 * 創建一個WebView實例,同時把當前的實例添加到集合中
	 * @param context 上下文
	 * @return
	 */
	public synchronized static WebView addWebView(Context context) {
		WebView webView = new WebView(context);
			if(webViews.size()>=3){
				for(int i=webViews.size()-1;i>=3;i--){
					WebView wv=webViews.remove(i);
					if(null!=wv){
						wv.removeAllViews();
						wv.destroy();
						wv=null;
					}
				}
			}
			webViews.addFirst(webView);
		return webView;
	}
	
	/**
	 * 釋放掉集合中緩存的WebView資源,
	 * 可以在Activity的onDestroy方法中調用該方法
	 */
	public synchronized static void release(){
		for(WebView webView: webViews){
			if(null!=webView){
				webView.removeAllViews();
				webView.destroy();
				webView=null;
			}
		}
		webViews.clear();
	}
	
}
