/*
 *Copyright (c) 2013. ccx (ccxandroid@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package x.bitmap;

import x.bitmap.callback.ImageLoadCallBack;
import x.bitmap.callback.SimpleImageLoadCallBack;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.animation.Animation;

/**
 * Bitmap顯示配置
 * @author Hero
 * @since 2013-10-21
 */
public class BitmapDisplayConfig {

	 private int bitmapMaxWidth = 0;
	 private int bitmapMaxHeight = 0;

    private Animation animation;

    private Drawable loadingDrawable;
    private Drawable loadFailedDrawable;

    private ImageLoadCallBack imageLoadCallBack;

    private boolean showOriginal = false;

    private Bitmap.Config bitmapConfig = Bitmap.Config.RGB_565;

    private Context mContext;
    
    private BitmapGlobalConfig bitmapGlobalConfig;
    
    
    private long lastModified;

    private static final Drawable TRANSPARENT_DRAWABLE = new ColorDrawable(Color.TRANSPARENT);
	private static final String TAG = "BitmapDisplayConfig";
	

    public BitmapDisplayConfig(Context context) {
        mContext = context;
    }

    /**
     * 獲取Bitmap顯示的最大寬度
     * @return
     */
    public int getBitmapMaxWidth() {
        if (bitmapMaxWidth == 0) {// default max width = screen_width/3
       
            DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
            bitmapMaxWidth = (int) Math.floor(displayMetrics.widthPixels / 3);
            bitmapMaxHeight = bitmapMaxHeight == 0 ? bitmapMaxWidth : bitmapMaxHeight;
        }
        return bitmapMaxWidth;
    }

    /**
     * 設置Bitmap顯示的最大寬度
     * @param bitmapMaxWidth
     */
    public void setBitmapMaxWidth(int bitmapMaxWidth) {
        this.bitmapMaxWidth = bitmapMaxWidth;
    }

    /**
     * 設置Bitmap顯示的最大高度
     * @return
     */
    public int getBitmapMaxHeight() {
        if (bitmapMaxHeight == 0) {// default max height = screen_width/3
            DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
            bitmapMaxHeight = (int) Math.floor(displayMetrics.widthPixels / 3);
            bitmapMaxWidth = bitmapMaxWidth == 0 ? bitmapMaxHeight : bitmapMaxWidth;
        }
        return bitmapMaxHeight;
    }

    /**
     * 設置Bitmap顯示的最大高度
     * @param bitmapMaxHeight
     */
    public void setBitmapMaxHeight(int bitmapMaxHeight) {
        this.bitmapMaxHeight = bitmapMaxHeight;
    }

    /**
     * 獲取加載圖片的動畫效果
     * @return
     */
    public Animation getAnimation() {
        return animation;
    }

    /**
     * 設置加載圖片的動畫效果
     * @param animation 動畫實例
     */
    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    /**
     * 獲取加載圖片過程中顯示的圖片
     * @return
     */
    public Drawable getLoadingDrawable() {
        return loadingDrawable == null ? TRANSPARENT_DRAWABLE : loadingDrawable;
    }

    /**
     * 設置加載圖片過程中顯示的圖片
     * @param loadingDrawable 圖片資源實例
     */
    public void setLoadingDrawable(Drawable loadingDrawable) {
        this.loadingDrawable = loadingDrawable;
    }

    /**
     * 獲取加載圖片失敗之後顯示的圖片
     * @return
     */
    public Drawable getLoadFailedDrawable() {
        return loadFailedDrawable == null ? TRANSPARENT_DRAWABLE : loadFailedDrawable;
    }

    /**
     * 設置加載圖片失敗之後顯示的圖片
     * @param loadFailedDrawable
     */
    public void setLoadFailedDrawable(Drawable loadFailedDrawable) {
        this.loadFailedDrawable = loadFailedDrawable;
    }

    /**
     * 獲取加載圖片資源完成（成功和失敗）的囘調
     * @return
     */
    public ImageLoadCallBack getImageLoadCallBack() {
        if (imageLoadCallBack == null) {
            imageLoadCallBack = new SimpleImageLoadCallBack();
        }
        return imageLoadCallBack;
    }

    /**
     * 設置加載圖片資源完成（成功和失敗）的囘調
     * @param imageLoadCallBack
     */
    public void setImageLoadCallBack(ImageLoadCallBack imageLoadCallBack) {
        this.imageLoadCallBack = imageLoadCallBack;
    }

    /**
     * 獲取是否使用原先的配置顯示圖片
     * @return
     */
    public boolean isShowOriginal() {
        return showOriginal;
    }

    /**
     * 設置是否使用原先的配置顯示圖片
     * @param showOriginal
     */
    public void setShowOriginal(boolean showOriginal) {
        this.showOriginal = showOriginal;
    }

    /**
     * 獲取Bitmap的配置，例如 Bitmap.Config.ARGB_8888
     * @return
     */
    public Bitmap.Config getBitmapConfig() {
        return bitmapConfig;
    }

    /**
     * 設置Bitmap的配置，例如 Bitmap.Config.ARGB_8888
     * @param bitmapConfig
     */
    public void setBitmapConfig(Bitmap.Config bitmapConfig) {
        this.bitmapConfig = bitmapConfig;
    }

    @Override
    public String toString() {
        return isShowOriginal() ? "" : "-" + getBitmapMaxWidth() + "-" + getBitmapMaxHeight();
    }


	public BitmapGlobalConfig getBitmapGlobalConfig() {
		return bitmapGlobalConfig;
	}

	public void setBitmapGlobalConfig(BitmapGlobalConfig bitmapGlobalConfig) {
		this.bitmapGlobalConfig = bitmapGlobalConfig;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

}
