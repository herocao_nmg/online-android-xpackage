
package x.bitmap.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.lang.ref.SoftReference;

import x.bitmap.BitmapCommonUtils;
import x.bitmap.BitmapDecoder;
import x.bitmap.BitmapDisplayConfig;
import x.bitmap.BitmapGlobalConfig;
import x.util.FileUtils;
import x.util.HttpUtils;
import x.util.IOUtils;
import android.graphics.Bitmap;
import android.text.TextUtils;


/**
 * Bitmap緩存類
 * @author Hero
 * @since 2013-11-07
 */
public class BitmapCache {

	private static final String TAG = "BitmapCache";

	private LruMemoryCache<String, SoftReference<Bitmap>> mMemoryCache;

	private final Object mDiskCacheLock = new Object();

	private BitmapGlobalConfig globalConfig;

	private LruDiskFileCache mLruDiskFileCache;

	/**
	 * 創建BitmapCache實例對象
	 * @param config Bitamp全局配置實例
	 */
	public BitmapCache(BitmapGlobalConfig config) {
		this.globalConfig = config;
	}

	/**
	 * 初始化內存緩存
	 */
	public void initMemoryCache() {
		if (!globalConfig.isMemoryCacheEnabled())
			return;

		if (mMemoryCache != null) {
			try {
				clearMemoryCache();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		mMemoryCache = new LruMemoryCache<String, SoftReference<Bitmap>>(
				globalConfig.getMemoryCacheSize()) {
		
			@Override
			protected int sizeOf(String key, SoftReference<Bitmap> bitmapRef) {
				return BitmapCommonUtils.getBitmapSize(bitmapRef.get());
			}
		};
	}

	
	/**
	 * 初始化設備本地緩存
	 */
	public void initDiskCache() {
		if (!globalConfig.isDiskCacheEnabled())
			return;

		synchronized (mDiskCacheLock) {
			
				if (mLruDiskFileCache == null ) {
				File diskCacheDir = new File(globalConfig.getDiskCachePath());
				if (!diskCacheDir.exists()) {
					diskCacheDir.mkdirs();
				}
				long availableSpace = BitmapCommonUtils
						.getAvailableSpace(diskCacheDir);
				long diskCacheSize = globalConfig.getDiskCacheSize();
				diskCacheSize = availableSpace > diskCacheSize ? diskCacheSize
						: availableSpace;
				try {

					if (null != globalConfig
							&& globalConfig.getGenerateSingleFile()) {
						mLruDiskFileCache = new LruDiskFileCache(
								globalConfig.getDiskCachePath());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			mDiskCacheLock.notifyAll();
		}
	}

	/**
	 * 設置內存緩存大小
	 * @param maxSize 內存緩存大小,單位為byte
	 */
	public void setMemoryCacheSize(int maxSize) {
		if (mMemoryCache != null) {
			mMemoryCache.setMaxSize(maxSize);
		}
	}

/*	public void setDiskCacheSize(int maxSize) {
		if (mDiskLruCache != null) {
			mDiskLruCache.setMaxSize(maxSize);
		}
	}*/


	/**
	 * 下載Bitmap
	 * @param uri 圖片資源的Uri
	 * @param config 圖片顯示配置實例
	 * @param lastModify 
	 * @return
	 */
	public Bitmap downloadBitmap(String uri, BitmapDisplayConfig config, long lastModify) {

		BitmapMeta bitmapMeta = new BitmapMeta();

		OutputStream outputStream = null;
		
		try {
			//下載到設備本地緩存
			if (globalConfig.isDiskCacheEnabled()) {
				synchronized (mDiskCacheLock) {
					
					if(!globalConfig.isDiskCacheEnabled()){
						return null;
					}

					if (null != globalConfig
							&& globalConfig.getGenerateSingleFile()) {
						
							byte[] data=HttpUtils.downloadByHttpClient(uri);
							
							if(null==data || data.length<=0) return null;
							
							boolean result=mLruDiskFileCache.save(uri, data, config,lastModify);
							
							if(result){
								FileUtils.delete(new File(BitmapCommonUtils.getDownloadDir(uri, globalConfig)), String.valueOf(lastModify)+FileUtils.getSuffix(uri,true));
							}
							
							bitmapMeta=null;
							bitmapMeta=new BitmapMeta();
							bitmapMeta.data=data;
							bitmapMeta.globalConfig=globalConfig;
							bitmapMeta.lastModify=lastModify;
							bitmapMeta.expiryTimestamp=System.currentTimeMillis() + globalConfig.getDownloader().getDefaultExpiry();
							
							// 添加到內存緩存中
							return addBitmapToMemoryCache(uri, config, bitmapMeta);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			IOUtils.close(outputStream);
		}

		return null;
	}
	
	/**
	 * 從設備本地加載圖片資源
	 * @param filename 設備本地圖片資源的全路徑名稱
	 * @param config 圖片資源顯示的配置
	 * @return
	 */
	public Bitmap loadBitmap(String filename, BitmapDisplayConfig config) {
		BitmapMeta bitmapMeta = new BitmapMeta();
		Bitmap bitmap=null;
		synchronized (mDiskCacheLock) {
		bitmap=getBitmapFromLocalFile(filename, config, bitmapMeta);
		bitmapMeta.expiryTimestamp=System.currentTimeMillis() + globalConfig.getDownloader().getDefaultExpiry();
		return addBitmapToMemoryCacheByLocalFile(bitmap,filename, config, bitmapMeta);
		}
	}
	
	/**
	 * 根據合成圖片資源路徑,從設備本地加載Bitmap
	 * @param compositeFilename 設備本地圖片資源合成路徑,默認路徑分隔符為@,
	 * 例如: /mnt/sdcard/aa_reader3/book/3000034/book40/1.jpg@/mnt/sdcard/aa_reader3/book/3000034/book40/2.jpg
	 * @param config 圖片顯示的配置
	 * @return
	 */
	public Bitmap loadCombineBitmap(String compositeFilename, BitmapDisplayConfig config) {
		BitmapMeta bitmapMeta = new BitmapMeta();
		Bitmap bitmap=null;
		synchronized (mDiskCacheLock) {
		bitmap=getCombineBitmapFromLocalFile(compositeFilename, config, bitmapMeta);
		bitmapMeta.expiryTimestamp=System.currentTimeMillis() + globalConfig.getDownloader().getDefaultExpiry();
		return addBitmapToMemoryCacheByLocalFile(bitmap,compositeFilename, config, bitmapMeta);
		}
	}
	

	/**
	 * 從設備本地合成圖片資源路徑獲取Bitmap
	 * @param compositeFilename 設備本地圖片資源合成路徑,默認路徑分隔符為@,
	 * 例如: /mnt/sdcard/aa_reader3/book/3000034/book40/1.jpg@/mnt/sdcard/aa_reader3/book/3000034/book40/2.jpg
	 * @param config 顯示配置
	 * @param bitmapMeta 數據元
	 * @return
	 */
	private Bitmap getCombineBitmapFromLocalFile(String compositeFilename,
			BitmapDisplayConfig config, BitmapMeta bitmapMeta) {

		if(null==compositeFilename||null==config || bitmapMeta==null || null== config.getBitmapGlobalConfig() || null==config.getBitmapGlobalConfig().getCompositePathSeparater()){
			return null;
		}
		String[] paths=compositeFilename.split(config.getBitmapGlobalConfig().getCompositePathSeparater());
		if(paths.length<2){
			return null;
		}
		
	Bitmap bitmap = null;
	Bitmap bitmap1=null;
	Bitmap bitmap2=null;
		
		if (config.isShowOriginal()) {
			 bitmap1=BitmapDecoder.decodeByteArray(BitmapCommonUtils.getBitmapByteFromFile(paths[0]));
			 bitmap2=BitmapDecoder.decodeByteArray(BitmapCommonUtils.getBitmapByteFromFile(paths[1]));
		   
		} else {
			 bitmap1 = BitmapDecoder.decodeSampledBitmapFromByteArray(BitmapCommonUtils.getBitmapByteFromFile(paths[0]),
					config.getBitmapMaxWidth(), config.getBitmapMaxHeight(),
					config.getBitmapConfig());
			 bitmap2 = BitmapDecoder.decodeSampledBitmapFromByteArray(BitmapCommonUtils.getBitmapByteFromFile(paths[1]),
					config.getBitmapMaxWidth(), config.getBitmapMaxHeight(),
					config.getBitmapConfig());
		}
		
		if(null==bitmap1 || null==bitmap2) return null;
		
		bitmap=BitmapCommonUtils.compositeBitmap(bitmap1, bitmap2);
		bitmap1.recycle();
		bitmap1=null;
		bitmap2.recycle();
		bitmap1=bitmap2=null;
		paths=null;
		return bitmap;
	}


	/**
	 * 把Bitamp添加到內存緩存中
	 * @param uri 資源Uri
	 * @param config 圖片顯示的配置
	 * @param bitmapMeta 數據元
	 * @return
	 */
	private Bitmap addBitmapToMemoryCache(String uri,
			BitmapDisplayConfig config, BitmapMeta bitmapMeta)
			{
		if (uri == null || bitmapMeta == null) {
			return null;
		}
		
		if(null!=bitmapMeta.globalConfig &&bitmapMeta.globalConfig.getGenerateSingleFile()){
			return  addBitmapToMemoryCacheBySingleFile(uri,config,bitmapMeta);
		}
		return null;
	}

	
	
	/**
	 * 把Bitmap添加到內存緩存中,這是從網絡下載圖片資源到本地的方式
	 * @param uri 網絡圖片資源Uri
	 * @param config 圖片顯示配置
	 * @param bitmapMeta 數據元
	 * @return
	 */
	private Bitmap addBitmapToMemoryCacheBySingleFile(String uri,
			BitmapDisplayConfig config, BitmapMeta bitmapMeta){
		
		if (uri == null || bitmapMeta == null ||bitmapMeta.globalConfig==null ||!bitmapMeta.globalConfig.getGenerateSingleFile()) {
			return null;
		}
		
		Bitmap bitmap = null;
		
		 if (bitmapMeta.data != null &&bitmapMeta.data.length>0) {
				if (null!=config&&config.isShowOriginal()) {
					bitmap = BitmapDecoder.decodeByteArray(bitmapMeta.data);
				} else {
					bitmap = BitmapDecoder.decodeSampledBitmapFromByteArray(
							bitmapMeta.data, config.getBitmapMaxWidth(),
							config.getBitmapMaxHeight(), config.getBitmapConfig());
				}
			} else {
				return null;
			}
		 
			if (bitmap == null) {
				return null;
			}
			//添加到內存緩存中
			if (globalConfig.isMemoryCacheEnabled() && mMemoryCache != null) {
				mMemoryCache.put(BitmapCommonUtils.getNetResourceMemoryCacheKey(config, BitmapCommonUtils.getPath(globalConfig, uri),bitmapMeta.lastModify), new SoftReference<Bitmap>(bitmap),
						bitmapMeta.expiryTimestamp);
			}
			return bitmap;
	}

	/**
	 * 從緩存中獲取Bitmap,這是從網絡下載圖片資源到本地緩存
	 * @param uri 圖片資源的Uri
	 * @param config 圖片顯示配置
	 * @return
	 */
	public Bitmap getBitmapFromMemCache(String uri, BitmapDisplayConfig config) {
		
		if(null==uri|| null==config){
			return null;
		}
		
		uri=BitmapCommonUtils.getPathByCompressFormat(globalConfig,uri);
		String key = uri + config.toString();
		if (mMemoryCache != null) {
			SoftReference<Bitmap> softRef = mMemoryCache.get(key);
						return softRef == null ? null : softRef.get();
		}
		
		return null;
	}
	
	/**
	 * 從緩存中獲取Bitmap,這是從網絡下載圖片資源到本地緩存
	 * @param uri 圖片資源的Uri
	 * @param config 圖片顯示配置
	 * @return
	 */
	public Bitmap getBitmapFromMemCache(String uri, BitmapDisplayConfig config, long lastModify) {
		
		if(TextUtils.isEmpty(uri)|| null==config){
			return null;
		}
		
		if (mMemoryCache != null) {
			SoftReference<Bitmap> softRef = mMemoryCache.get(BitmapCommonUtils.getNetResourceMemoryCacheKey(config,uri, lastModify));
						return softRef == null ? null : softRef.get();
		}
		return null;
	}
	
	/**
	 * 從內存緩存中獲取Bitamp,這原本就是設備本地圖片資源
	 * @param filename 設備緩存圖片資源全路徑名稱
	 * @param config 圖片顯示配置
	 * @return
	 */
	public Bitmap getBitmapFromMemoryCacheByLocalFile(String filename, BitmapDisplayConfig config) {
		
		if(null==filename|| null==config){
			return null;
		}
		
		String key = filename + config.toString();
		
		if (mMemoryCache != null) {
			SoftReference<Bitmap> softRef = mMemoryCache.get(key);
						return softRef == null ? null : softRef.get();
		}
		
		return null;
	}
	
	/**
	 * 把Bitmap添加到內存緩存中,這原本就是設備本地圖片資源
	 * @param bitmap Bitamp實例
	 * @param filename 設備本地圖片資源全路徑名稱
	 * @param config 圖片顯示配置
	 * @param bitmapMeta 數據元
	 * @return
	 */
	private Bitmap addBitmapToMemoryCacheByLocalFile(Bitmap bitmap,
			String filename, BitmapDisplayConfig config, BitmapMeta bitmapMeta) {
		if (null == bitmap || null == filename || null == config
				|| null == bitmapMeta) {
			return null;
		}

		String key = filename + config.toString();
		if (globalConfig.isMemoryCacheEnabled() && mMemoryCache != null) {
			mMemoryCache.put(key, new SoftReference<Bitmap>(bitmap),
					bitmapMeta.expiryTimestamp);
		}
		return bitmap;
	}

	/**
	 * 獲取Bitmap,這原本是本地設備圖片資源的方式
	 * @param filename 設備本地圖片資源文件全路徑名稱
	 * @param config 圖片顯示
	 * @param bitmapMeta 數據元
	 * @return
	 */
	private Bitmap getBitmapFromLocalFile(String filename,
			BitmapDisplayConfig config, BitmapMeta bitmapMeta){
		if(null==filename|| null==config || null== bitmapMeta){
			return null;
		}
		
		Bitmap bitmap = null;
		
		if (config.isShowOriginal()) {
			bitmap=BitmapDecoder.decodeByteArray(BitmapCommonUtils.getBitmapByteFromFile(filename));
		} else {
			byte[] data=BitmapCommonUtils.getBitmapByteFromFile(filename);
			
			bitmap = BitmapDecoder.decodeSampledBitmapFromByteArray(data,
					config.getBitmapMaxWidth(), config.getBitmapMaxHeight(),
					config.getBitmapConfig());
		}
		return bitmap;
	}
	
	

	/**
	 * 獲取Bitmap從本地設備緩存中,這是從網絡下載圖片資源到設備本地緩存的方式
	 * @param uri 圖片資源的Uri
	 * @return
	 */
	public File getBitmapFileFromDiskCache(String uri) {

		if (null != mLruDiskFileCache && null != globalConfig
				&& globalConfig.getGenerateSingleFile()) {
			
			return mLruDiskFileCache.getCacheFile(uri,globalConfig);
		}

		return null;
	}
	
	/**
	 * 設置設備本地緩存圖片資源文件的最後修改時間
	 * @param uri 網絡圖片資源Uri
	 * @param lastModified 最後修改時間,不能為0
	 * @return
	 */
	public boolean setBitmapFileLastModified(String uri,long lastModified){
		if(lastModified==0) return false;
		
		File file=getBitmapFileFromDiskCache(uri);
		if(null==file) return false;
		
		file.setLastModified(lastModified);
		file=null;
		return true;
	}
	

	/**
	 * 獲取Bitamp從本地設備緩存中,這是從網絡下載圖片資源到設備本地緩存的方式
	 * @param uri 圖片資源Uri
	 * @param config 圖片顯示的配置
	 * @return
	 */
	public Bitmap getBitmapFromDiskCache(String uri, BitmapDisplayConfig config,long lastTime) {
		if (null==uri || !globalConfig.isDiskCacheEnabled()){
			return null;
		}
		
		synchronized (mDiskCacheLock) {
			
			if(null!=mLruDiskFileCache && null!=globalConfig && globalConfig.getGenerateSingleFile()){
				
				BitmapMeta bitmapMeta=new BitmapMeta();
				bitmapMeta.data=getByteFromDiskCacheBySingleFile(uri, config, lastTime);
				bitmapMeta.expiryTimestamp=System.currentTimeMillis() + globalConfig.getDownloader().getDefaultExpiry();
				bitmapMeta.globalConfig=globalConfig;
				 return addBitmapToMemoryCache(uri, config, bitmapMeta);
				
			}
			return null;
		}
	}
	

	/**
	 * 從設備本地緩存獲取Bitmap,這是從網絡下載圖片資源到設備本地的方式
	 * @param uri 網絡圖片資源的Uri
	 * @param config 圖片顯示配置
	 * @return
	 */
	private Bitmap getBitmapFromDiskCacheBySingleFile(String uri,
			BitmapDisplayConfig config,long lastTime) {

		if (!globalConfig.isDiskCacheEnabled() ||null==mLruDiskFileCache) {
			return null;
		}

		Bitmap bitmap = null;

		if ( null != globalConfig
				&& globalConfig.getGenerateSingleFile()) {

			byte[] data= mLruDiskFileCache.getImageData(uri,globalConfig,lastTime);;
			
			if (config.isShowOriginal()) {

				if (data != null && data.length > 0) {
					bitmap = BitmapDecoder.decodeByteArray(data);
				}
			}else {

				if (data != null && data.length > 0) {
					bitmap = BitmapDecoder.decodeSampledBitmapFromByteArray(
							data, config.getBitmapMaxWidth(),
							config.getBitmapMaxHeight(),
							config.getBitmapConfig());

				}
			}
		}

		return bitmap;
	}
	
	
	/**
	 * 從設備本地緩存獲取byte[],這是從網絡下載圖片資源到設備本地緩存的方式
	 * @param uri 網絡圖片資源的Uri
	 * @param config 圖片顯示配置
	 * @param lastTime 
	 * @return
	 */
	private byte[] getByteFromDiskCacheBySingleFile(String uri,
			BitmapDisplayConfig config, long lastTime) {

		if (null==uri||null==globalConfig||!globalConfig.isDiskCacheEnabled() ||null==mLruDiskFileCache ||!globalConfig.getGenerateSingleFile()) {
			return null;
		}
		return  mLruDiskFileCache.getImageData(uri,globalConfig,lastTime);
	}
	

	/**
	 * 清除緩存,包含內存緩存和設備本地緩存
	 */
	public void clearCache() {
		clearMemoryCache();
		clearDiskCache();
	}

	/**
	 *釋放內存緩存
	 */
	public void clearMemoryCache() {
		if (mMemoryCache != null) {
			mMemoryCache.evictAll();
		}
	}

	/**
	 * 釋放設備本地緩存
	 */
	public void clearDiskCache() {
		synchronized (mDiskCacheLock) {
			if (null != mLruDiskFileCache && null != globalConfig
					&& globalConfig.getGenerateSingleFile()) {
				try {
					mLruDiskFileCache.deleteAll();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} 
		}
		initDiskCache();
	}

	/**
	 * 根據Uri釋放緩存,包含內存緩存和設備本地緩存
	 * @param uri 圖片資源的Uri
	 * @param config
	 */
	public void clearCache(String uri, BitmapDisplayConfig config) {
		clearMemoryCache(uri, config);
		clearDiskCache(uri);
	}

	/**
	 * 根據Uri釋放內存緩存
	 * @param uri
	 * @param config
	 */
	public void clearMemoryCache(String uri, BitmapDisplayConfig config) {
		String key = uri + config.toString();
		if (mMemoryCache != null) {
			mMemoryCache.remove(key);
		}
	}

	/**
	 * 根據Uri釋放設備本地緩存
	 * @param uri
	 */
	public void clearDiskCache(String uri) {
		synchronized (mDiskCacheLock) {

			if (null != mLruDiskFileCache && null != globalConfig
					&& globalConfig.getGenerateSingleFile()) {
				try {
					mLruDiskFileCache.delete(uri,globalConfig);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} 
		}
	}

	/**
	 * Bitmap數據元
	 * @author Hero
	 * @since 2013-11-06
	 */
	private class BitmapMeta {
		public FileInputStream inputStream;
		public byte[] data;
		public long expiryTimestamp;
		public BitmapGlobalConfig globalConfig;
		public long lastModify;
	}
}
