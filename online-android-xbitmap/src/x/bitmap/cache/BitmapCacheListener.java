
package x.bitmap.cache;

import x.bitmap.BitmapDisplayConfig;


/**
 * Bitmap緩存監聽接口
 * @author Hero
 * @since 2013-11-06
 */
public interface BitmapCacheListener {
    void onInitMemoryCacheFinished();

    void onInitDiskFinished();

    void onClearCacheFinished();

    void onClearMemoryCacheFinished();

    void onClearDiskCacheFinished();

    void onClearCacheFinished(String uri, BitmapDisplayConfig config);

    void onClearMemoryCacheFinished(String uri, BitmapDisplayConfig config);

    void onClearDiskCacheFinished(String uri);

    void onFlushCacheFinished();

    void onCloseCacheFinished();
}
