package x.bitmap.cache;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import x.bitmap.BitmapCommonUtils;
import x.bitmap.BitmapDisplayConfig;
import x.bitmap.BitmapGlobalConfig;
import x.util.IOUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;

/**
 * 設備本地緩存類
 * @author Hero
 * @since 2013-11-06
 */
public class LruDiskFileCache {

	private static final String TAG = "LruDiskFileCache";
	private String diskCachePath;

	/**
	 * 創建LruDiskFileCache實例
	 * @param diskCachePath 設備本地緩存路徑
	 */
	public LruDiskFileCache(String diskCachePath) {
		this.diskCachePath = diskCachePath;
	}

	/**
	 * 保存網絡圖片資源到設備本地緩存
	 * @param uri 網絡圖片資源Uri
	 * @param data 網絡圖片資源的數據
	 * @param globalConfig 圖片參數全局配置
	 * @return
	 */
	public boolean save(String uri, byte[] data, BitmapGlobalConfig globalConfig) {
		if (TextUtils.isEmpty(uri) || data == null || data.length < 1
				|| null == globalConfig) {
			return false;
		}

		String filename=BitmapCommonUtils.getFileName(uri, globalConfig);
		File root = new File(
				BitmapCommonUtils.getDownloadDir(uri, globalConfig));

		if (!root.exists()) {
			root.mkdirs();
		}

		File file = new File(root, filename);
		try {

			if (file.exists()) {
				file.createNewFile();
			}
			FileOutputStream fos = new FileOutputStream(file);
			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			bitmap.compress(BitmapCommonUtils.getCompressFormat(globalConfig, filename), BitmapCommonUtils.getCompressQuality(globalConfig), fos);
			IOUtils.close(fos);
			
			bitmap = null;
			file = root = null;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean save(String uri, byte[] data, BitmapDisplayConfig displayConfig, long lastTime) {
		if (TextUtils.isEmpty(uri) || data == null || data.length < 1
				|| null==displayConfig || null == displayConfig.getBitmapGlobalConfig()) {
			return false;
		}

		BitmapGlobalConfig globalConfig = displayConfig.getBitmapGlobalConfig();
		String filename=BitmapCommonUtils.getFileName(uri, globalConfig);
		
		if(-1!=lastTime){
			filename=String.valueOf(lastTime)+filename.substring(filename.lastIndexOf("."));
		}

		File root = new File(
				BitmapCommonUtils.getDownloadDir(uri, globalConfig));

		if (!root.exists()) {
			root.mkdirs();
		}

		File file = new File(root, filename);
		try {

			if (file.exists()) {
//				file.createNewFile();
				file.delete();
			}
			
			FileOutputStream fos = new FileOutputStream(file);
			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			bitmap.compress(BitmapCommonUtils.getCompressFormat(globalConfig, filename), BitmapCommonUtils.getCompressQuality(globalConfig), fos);
			IOUtils.close(fos);
			
			bitmap = null;
			file = root = null;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}


	/**
	 * 判斷當前Uri對應的圖片資源是否在設備本地緩存中
	 * @param uri 網絡圖片資源Uri
	 * @param globalConfig 圖片全局配置
	 * @return
	 */
	public boolean lookup(String uri, BitmapGlobalConfig globalConfig) {
		if (null == uri)
			return false;

		String filename = BitmapCommonUtils.getFileName(uri);
		filename=BitmapCommonUtils.getPathByCompressFormat(globalConfig,filename);
		File file = new File(BitmapCommonUtils.getDownloadDir(uri, globalConfig), filename);
		boolean exists = file.exists();

		if (exists) {
			file = null;
			filename = null;
			return true;
		} else {
			filename = null;
			return false;
		}
	}


	/**
	 * 根據Uri獲取圖片資源文件的byte[]
	 * @param uri 網絡圖片資源Uri
	 * @param globalConfig 圖片全局配置
	 * @param lastTime 
	 * @return
	 */
	public byte[] getImageData(String uri, BitmapGlobalConfig globalConfig, long lastTime) {
		
		if(null==uri|| null==globalConfig) return null;
		
		String filename = lastTime+uri.substring(uri.lastIndexOf("."), uri.length());
		File file = new File(BitmapCommonUtils.getDownloadDir(uri, globalConfig), filename);
		try {
			if (file.exists() ) {
				FileInputStream fis = new FileInputStream(file);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] buff = new byte[1024];
				int len = -1;

				while ((len = fis.read(buff)) != -1) {
					baos.write(buff, 0, len);
				}
				baos.flush();
				byte[] data = baos.toByteArray();

				IOUtils.close(baos);
				IOUtils.close(fis);
				file = null;
				return data;
				} 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 根據Uri從設備本地緩存中獲取圖片資源文件
	 * @param uri 網絡圖片資源Uri
	 * @param globalConfig 圖片全局配置
	 * @return
	 */
	public File getCacheFile(String uri, BitmapGlobalConfig globalConfig) {
		String filename = BitmapCommonUtils.getFileName(uri);
		filename=BitmapCommonUtils.getPathByCompressFormat(globalConfig,filename);
		File file = new File(BitmapCommonUtils.getDownloadDir(uri, globalConfig), filename);
		if (file.exists()) {
			return file;
		} else {
			return file = null;
		}
	}


	/**
	 * 根據Uri釋放設備本地圖片資源文件緩存
	 * @param uri 網絡圖片資源Uri
	 * @param globalConfig 圖片全局配置
	 * @return
	 * @throws IOException
	 */
	public boolean delete(String uri, BitmapGlobalConfig globalConfig)
			throws IOException {
		String filename = BitmapCommonUtils.getFileName(uri);
		
		filename=BitmapCommonUtils.getPathByCompressFormat(globalConfig,filename);
		File file = new File(BitmapCommonUtils.getDownloadDir(uri, globalConfig), filename);
		
		if (file.exists() && !file.delete()) {
			throw new IOException("failed to delete file: " + file);
		}
		file = null;
		return true;
	}
	

	/**
	 * 釋放設備本地所有圖片資源緩存
	 * @return
	 * @throws IOException
	 */
	public boolean deleteAll() throws IOException {
		File dir = new File(diskCachePath);

		if(dir.isDirectory()){
		deleteAll(dir);
		}else{
			dir.delete();
		}
		dir = null;
		return true;
	}
	
	/**
	 * 遞歸刪除目錄下的文件
	 * @param dir
	 * @return
	 */
	private boolean deleteAll(File dir){
		if(null==dir) return false;
		File[] files = dir.listFiles();
		for (File file : files) {
			if(file.isDirectory()){
			return deleteAll(dir);
			}else{
			return	file.delete();
			}
		}
		return true;
	}
}
