package x.bitmap;

import android.graphics.Bitmap;

/**
 * 加載設備本地圖片資源接口
 * @author Hero
 *
 */
public interface BitmapLoadLocalListener {
	
	void onCompleted(Bitmap bitmap,BitmapDisplayConfig displayConfig);
}
