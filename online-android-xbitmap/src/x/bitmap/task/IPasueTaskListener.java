package x.bitmap.task;

/**
 * 暫停線程任務接口
 * @author Hero
 *
 */
public interface IPasueTaskListener {
	void onPause(boolean pauseTask);
}
