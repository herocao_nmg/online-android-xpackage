package x.bitmap.task;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

import x.bitmap.BitmapCommonUtils;
import x.bitmap.BitmapDisplayConfig;
import x.bitmap.BitmapLoadNetListener;
import x.util.FileUtils;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.widget.ImageView;

/**
 * 加載網絡圖片資源線程任務
 * @author Hero
 *
 */
public class NetBitmapResourceLoadAsyncTask extends
		CompatibleAsyncTask<Object, Void, Bitmap> implements IPasueTaskListener {
	
	private String uri;
	private final WeakReference<ImageView> targetImageViewReference;
	private final BitmapDisplayConfig displayConfig;
	private long lastTime;
	private final Object pauseTaskLock;
	private boolean pauseTask = false;
	private Context mContext;
	private BitmapLoadNetListener mBitmapLoadNetListener;
	
	public NetBitmapResourceLoadAsyncTask(Context context,BitmapLoadNetListener loadNetListener,ImageView imageView,BitmapDisplayConfig displayConfig,long lastTime,Object pauseTaskLock) {
		this.mContext=context;
		this.mBitmapLoadNetListener=loadNetListener;
		targetImageViewReference = new WeakReference<ImageView>(imageView);
		this.displayConfig = displayConfig;
		this.lastTime=lastTime;
		this.pauseTaskLock=pauseTaskLock;
		
		if(null==pauseTaskLock){
			throw new RuntimeException("An error occured while create NetBitmapResourceLoadAsyncTask");
		}
	}

	@Override
	protected Bitmap doInBackground(Object... params) {

		if (params != null && params.length > 0 && !params[0].equals("null")) {
			uri = (String) params[0];
		} else {
			return null;
		}
		Bitmap bitmap = null;

		synchronized (pauseTaskLock) {
			while (pauseTask && !this.isCancelled()) {
				try {
					pauseTaskLock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		if (!this.isCancelled() ){ //&& this.getTargetImageView() != null) {
			bitmap = displayConfig.getBitmapGlobalConfig().getBitmapCache()
					.getBitmapFromDiskCache(uri, displayConfig,lastTime);
			
			System.out.println("NetBitmapResourceLoadAsyncTask,從SD卡中加載,bitmap="+bitmap);
		}
		
		// 下載圖片資源
		if (bitmap == null && !this.isCancelled()){
				//&& this.getTargetImageView() != null) {
				bitmap = displayConfig.getBitmapGlobalConfig().getBitmapCache().downloadBitmap(uri,
						displayConfig,lastTime);
		}
		
		if(null==bitmap){
		File file = BitmapCommonUtils.getOldResource(uri, displayConfig.getBitmapGlobalConfig());
		if(null!=file){
			try {
				bitmap=BitmapCommonUtils.getOldBitmap(uri, displayConfig, FileUtils.read(file));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				file=null;
				e.printStackTrace();
			}
		}
		file=null;
		}
	
		return bitmap;
	}
	
	@Override
	protected void onPostExecute(Bitmap bitmap) {
		
		if(null!=bitmap && null!=mBitmapLoadNetListener){
			mBitmapLoadNetListener.onCompleted(bitmap, displayConfig);
			return;
		}
		
		final ImageView imageView = this.getTargetImageView();
		
		if (imageView != null) {
			if (bitmap != null) {
					displayConfig.getImageLoadCallBack().loadCompleted(
							imageView, new BitmapDrawable(mContext.getResources(),bitmap),
							displayConfig);
			} else {
				displayConfig.getImageLoadCallBack().loadFailed(imageView,
						displayConfig.getLoadFailedDrawable());
			}
		}
	}
	

	@Override
	public void onPause(boolean pauseTask) {
		this.pauseTask=pauseTask;
	}
	
	@Override
	protected void onCancelled(Bitmap bitmap) {
		super.onCancelled(bitmap);
		synchronized (pauseTaskLock) {
			pauseTaskLock.notifyAll();
		}
	}
	
	/**
	 * 獲取目標ImageView實例
	 * 
	 * @return
	 */
	private ImageView getTargetImageView() {
		final ImageView imageView = targetImageViewReference.get();
		final NetBitmapResourceLoadAsyncTask bitmapWorkerTask = getBitmapTaskFromImageView(imageView);

		if (this == bitmapWorkerTask)
			return imageView;

		return null;
	}
	
	/**
	 * 獲取當前ImageView的圖片資源下載任務線程
	 * 
	 * @param imageView
	 * @return
	 */
	public static NetBitmapResourceLoadAsyncTask getBitmapTaskFromImageView(ImageView imageView) {
		if (imageView != null) {
			final Drawable drawable = imageView.getDrawable();

			if (drawable instanceof AsyncBitmapDrawable) {
				final AsyncBitmapDrawable asyncBitmapDrawable = (AsyncBitmapDrawable) drawable;
				return (NetBitmapResourceLoadAsyncTask) asyncBitmapDrawable.getBitmapWorkerTask();
			}
		}
		return null;
	}
	
	// 判斷當前ImageView的圖片資源是否已經開啟線程任務執行下載
	public  boolean bitmapLoadTaskExist(ImageView imageView, String uri) {
		final NetBitmapResourceLoadAsyncTask oldLoadTask = getBitmapTaskFromImageView(imageView);
		if (oldLoadTask != null) {
			final String oldUri = oldLoadTask.uri;
			if (TextUtils.isEmpty(oldUri) || !oldUri.equals(uri) || uri.equals("null")) {
//			if (TextUtils.isEmpty(oldUri) ||   uri.equals("null")) {
//			if (TextUtils.isEmpty(oldUri) ) {
				oldLoadTask.cancel(true);
			} else {
				return true;
			}
		}
		return false;
	}

}
