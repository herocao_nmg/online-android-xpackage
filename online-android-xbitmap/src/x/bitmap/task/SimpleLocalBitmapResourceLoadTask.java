package x.bitmap.task;

import java.lang.ref.WeakReference;

import x.bitmap.BitmapDisplayConfig;
import x.bitmap.BitmapLoadLocalListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class SimpleLocalBitmapResourceLoadTask extends
		CompatibleAsyncTask<Object, Void, Bitmap> implements IPasueTaskListener {

	private static final String TAG = "SimpleLocalBitmapResourceLoadTask";
	private String fileName;
	private final WeakReference<ImageView> targetImageViewReference;
	private final BitmapDisplayConfig displayConfig;
	private final Object pauseTaskLock;
	private boolean pauseTask = false;
	private Context mContext;
	private BitmapLoadLocalListener mBitmapLoadLocalListener;
	
	public SimpleLocalBitmapResourceLoadTask(Context context,BitmapLoadLocalListener loadLocalListener,ImageView imageView,BitmapDisplayConfig displayConfig,Object pauseTaskLock) {
		this.mContext=context;
		targetImageViewReference = new WeakReference<ImageView>(imageView);
		this.displayConfig = displayConfig;
		this.mBitmapLoadLocalListener=loadLocalListener;
		this.pauseTaskLock=pauseTaskLock;
		
		if(null==pauseTaskLock){
			throw new RuntimeException("An error occured while create SimpleLocalBitmapResourceLoadTask");
		}
	}
	
	@Override
	protected Bitmap doInBackground(Object... params) {

		if (null!=params  && params.length > 0 && null!=params[0] && !params[0].equals("null")) {
			fileName = (String) params[0];
		} else {
			return null;
		}
		Bitmap bitmap = null;

		synchronized (pauseTaskLock) {
			while (pauseTask && !this.isCancelled()) {
				try {
					pauseTaskLock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		
		final boolean result= null!=displayConfig && null != displayConfig.getBitmapGlobalConfig();
		if (result) {
				bitmap = displayConfig.getBitmapGlobalConfig()
						.getBitmapCache().loadBitmap(fileName, displayConfig);
			}
		
		synchronized (pauseTaskLock) {
			int count=0;
			while(null==bitmap && result && count<=3){
				count++;
					bitmap = displayConfig.getBitmapGlobalConfig()
						.getBitmapCache().loadBitmap(fileName, displayConfig);
			}
		}
		return bitmap;
	}

	@Override
	public void onPause(boolean pauseTask) {
		this.pauseTask=pauseTask;
	}
	
	@Override
	protected void onPostExecute(Bitmap bitmap) {
		 
			if (null!=mBitmapLoadLocalListener) {
				mBitmapLoadLocalListener.onCompleted(bitmap,displayConfig);
			}else{
				 ImageView imageView = this.getTargetImageView();
				if(null!=imageView){
					imageView.setImageBitmap(bitmap);
				}
			}
	}

	/**
	 * 獲取目標ImageView實例
	 * 
	 * @return
	 */
	private synchronized ImageView getTargetImageView() {
		final ImageView imageView = targetImageViewReference.get();
		final SimpleLocalBitmapResourceLoadTask bitmapWorkerTask = getBitmapTaskFromImageView(imageView);

		if (this == bitmapWorkerTask){
			return imageView;
		} 
		return null;
	}
	
	/**
	 * 獲取當前ImageView的圖片資源下載任務線程
	 * 
	 * @param imageView
	 * @return
	 */
	public synchronized static SimpleLocalBitmapResourceLoadTask getBitmapTaskFromImageView(ImageView imageView) {
		if (imageView != null) {
			final Drawable drawable = imageView.getDrawable();

			if (drawable instanceof AsyncBitmapDrawable) {
				final AsyncBitmapDrawable asyncBitmapDrawable = (AsyncBitmapDrawable) drawable;
				return (SimpleLocalBitmapResourceLoadTask) asyncBitmapDrawable.getBitmapWorkerTask();
			}
		} 
		return null;
	}
	
}
