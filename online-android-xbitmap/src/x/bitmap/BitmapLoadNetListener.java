package x.bitmap;

import android.graphics.Bitmap;

/**
 * 加載網絡圖片資源接口
 * @author Hero
 * @since 2014-01-22
 */
public interface BitmapLoadNetListener {
	
	void onCompleted(Bitmap bitmap,BitmapDisplayConfig displayConfig);
}
