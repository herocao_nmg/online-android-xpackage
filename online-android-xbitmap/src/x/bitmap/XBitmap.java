/*
 *Copyright (c) 2013. ccx (ccxandroid@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package x.bitmap;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import x.bitmap.cache.BitmapCacheListener;
import x.bitmap.callback.ImageLoadCallBack;
import x.bitmap.download.Downloader;
import x.bitmap.task.AsyncBitmapDrawable;
import x.bitmap.task.CombineLocalBitmapResourceLoadTask;
import x.bitmap.task.CompatibleAsyncTask;
import x.bitmap.task.NetBitmapResourceLoadAsyncTask;
import x.bitmap.task.SimpleLocalBitmapResourceLoadTask;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.widget.ImageView;

/**
 * XBitmap 是提供給外部處理圖片的類，內部使用到緩存機制（本地文件緩存和內存緩存） ，調用者不需要關心如何處理緩存
 * <p>
 * 建議使用 XBitmap.create(Context context) 創建XBitmap實例
 * </p>
 * 
 * @author Hero
 * @since 2013-10-22
 */
public class XBitmap {

	private static final String TAG = "XBitmap";
	private static XBitmap mXBitmap;
	private boolean pauseTask = false;
	private final Object pauseTaskLock = new Object();

	private Context mContext;
	private BitmapGlobalConfig globalConfig;
	private BitmapDisplayConfig defaultDisplayConfig;
	private boolean pauseLoadLocalResource=false;
	
	public static LinkedList<CompatibleAsyncTask> bitmapLoadTasks=new LinkedList<CompatibleAsyncTask>();
	public static Map<XBitmap,BitmapGlobalConfig> xBitmapGlobalConfigs=new HashMap<XBitmap, BitmapGlobalConfig>();
	public static Map<BitmapGlobalConfig,BitmapDisplayConfig> xBitmapDisplayConfigs=new HashMap<BitmapGlobalConfig, BitmapDisplayConfig>();
	
	// ///////////////////////////////////////////// create / ///////////////////////////////////////////////////

	private XBitmap() {
	}
	
	/**
	 * 創建XBitmap實例
	 * 
	 * @param context
	 *            上下文
	 */
	public XBitmap(Context context) {
		this(context, null);
	}

	/**
	 * 創建XBitmap
	 * 
	 * @param context
	 *            上下文
	 * @param diskCachePath
	 *            設置設備本地緩存路徑，如果設備掛載SDCard就直接把緩存文件存儲到SDCard中，
	 *            例如：Android/包名/cache/diskCachePath/ 目錄下
	 *            如果設備沒有掛載SDCard就直接把緩存文件存儲到系統的內存空間中，
	 *            例如：/data/data/應用包名/cache/diskCachePath/ 目錄下 <br/>
	 * <br/>
	 *            注意：如果diskCachePath為空（null 或者
	 *            "")就默認使用BitmapGlobalConfig.diskCachePath
	 */
	public XBitmap(Context context, String diskCachePath) {
		this.mContext = context;
		globalConfig = new BitmapGlobalConfig(context, diskCachePath);
		defaultDisplayConfig = new BitmapDisplayConfig(context);
	}

	/**
	 * 創建XBitmap實例，使用線程同步，建議使用create/getInstance1/getInstance2/getInstance3/getInstance4方法創建XBitmap實例,
	 * 因為這五個方法都是創建全局不同XBitmap實例，爲什麽提供五個方法創建五個不同全局XBitmap實例,
	 * 例如 同一個Activity中首先加載縮略圖,然後再加載大圖,若都是使用同一個XBitmap實例,
	 * 則所有加載圖片資源的任務都添加到同一個線程隊列中,這樣加載大圖需要花費很長時間等待線程執行,
	 * 如果希望設置其他參數，可以通過XBitmap實例調用configDefaultXXX()方法進行配置
	 * @param context
	 *            上下文
	 * @return 返回XBitmap實例
	 */
	public synchronized static XBitmap create(Context context) {
		if (mXBitmap == null) {
			mXBitmap = new XBitmap(context.getApplicationContext());
		}
		return mXBitmap;
	}
	
	/**
	 * 創建XBitmap實例，使用線程同步，建議使用create/getInstance1/getInstance2/getInstance3/getInstance4/getInstance5方法創建XBitmap實例,
	 * 因為這五個方法都是創建全局不同XBitmap實例，爲什麽提供五個方法創建五個不同全局XBitmap實例,
	 * 例如 同一個Activity中首先加載縮略圖,然後再加載大圖,若都是使用同一個XBitmap實例,
	 * 則所有加載圖片資源的任務都添加到同一個線程隊列中,這樣加載大圖需要花費很長時間等待線程執行,
	 * 如果希望設置其他參數，可以通過XBitmap實例調用configDefaultXXX()方法進行配置
	 * @param context
	 *            上下文
	 * @return 返回XBitmap實例
	 */
	public synchronized static XBitmap getInstance1(Context context){
		return InnerBitmap.getInstance().xBitmap1.setContext(context);
	}
	
	/**
	 * 創建XBitmap實例，使用線程同步，建議使用create/getInstance1/getInstance2/getInstance3/getInstance4/getInstance5方法創建XBitmap實例,
	 * 因為這五個方法都是創建全局不同XBitmap實例，爲什麽提供五個方法創建五個不同全局XBitmap實例,
	 * 例如 同一個Activity中首先加載縮略圖,然後再加載大圖,若都是使用同一個XBitmap實例,
	 * 則所有加載圖片資源的任務都添加到同一個線程隊列中,這樣加載大圖需要花費很長時間等待線程執行,
	 * 如果希望設置其他參數，可以通過XBitmap實例調用configDefaultXXX()方法進行配置
	 * @param context
	 *            上下文
	 * @return 返回XBitmap實例
	 */
	public synchronized static XBitmap getInstance2(Context context){
		return InnerBitmap.getInstance().xBitmap2.setContext(context);
	}
	
	/**
	 * 創建XBitmap實例，使用線程同步，建議使用create/getInstance1/getInstance2/getInstance3/getInstance4/getInstance5方法創建XBitmap實例,
	 * 因為這五個方法都是創建全局不同XBitmap實例，爲什麽提供五個方法創建五個不同全局XBitmap實例,
	 * 例如 同一個Activity中首先加載縮略圖,然後再加載大圖,若都是使用同一個XBitmap實例,
	 * 則所有加載圖片資源的任務都添加到同一個線程隊列中,這樣加載大圖需要花費很長時間等待線程執行,
	 * 如果希望設置其他參數，可以通過XBitmap實例調用configDefaultXXX()方法進行配置
	 * @param context
	 *            上下文
	 * @return 返回XBitmap實例
	 */
	public synchronized static XBitmap getInstance3(Context context){
		return InnerBitmap.getInstance().xBitmap3.setContext(context);
	}
	
	/**
	 * 創建XBitmap實例，使用線程同步，建議使用create/getInstance1/getInstance2/getInstance3/getInstance4/getInstance5方法創建XBitmap實例,
	 * 因為這五個方法都是創建全局不同XBitmap實例，爲什麽提供五個方法創建五個不同全局XBitmap實例,
	 * 例如 同一個Activity中首先加載縮略圖,然後再加載大圖,若都是使用同一個XBitmap實例,
	 * 則所有加載圖片資源的任務都添加到同一個線程隊列中,這樣加載大圖需要花費很長時間等待線程執行,
	 * 如果希望設置其他參數，可以通過XBitmap實例調用configDefaultXXX()方法進行配置
	 * @param context
	 *            上下文
	 * @return 返回XBitmap實例
	 */
	public synchronized static XBitmap getInstance4(Context context){
		return InnerBitmap.getInstance().xBitmap4.setContext(context);
	}
	
	/**
	 * 創建XBitmap實例，使用線程同步，建議使用create/getInstance1/getInstance2/getInstance3/getInstance4/getInstance5方法創建XBitmap實例,
	 * 因為這五個方法都是創建全局不同XBitmap實例，爲什麽提供五個方法創建五個不同全局XBitmap實例,
	 * 例如 同一個Activity中首先加載縮略圖,然後再加載大圖,若都是使用同一個XBitmap實例,
	 * 則所有加載圖片資源的任務都添加到同一個線程隊列中,這樣加載大圖需要花費很長時間等待線程執行,
	 * 如果希望設置其他參數，可以通過XBitmap實例調用configDefaultXXX()方法進行配置
	 * @param context
	 *            上下文
	 * @return 返回XBitmap實例
	 */
	public synchronized static XBitmap getInstance5(Context context){
		return InnerBitmap.getInstance().xBitmap5.setContext(context);
	}
	

	/**
	 * 創建XBitmap實例
	 * 
	 * @param context
	 *            上下文
	 * @param diskCachePath
	 *            設置設備本地緩存路徑
	 * @param memoryCacheSize
	 *            設置內存緩存大小，單位為Byte，最小為1024*1024*2=2MB，
	 *            在BitmapGlobalConfig.MIN_MEMORY_CACHE_SIZE 中設置
	 */
	public XBitmap(Context context, String diskCachePath, int memoryCacheSize) {
		this(context, diskCachePath);
		globalConfig.setMemoryCacheSize(memoryCacheSize);
	}

	/**
	 * 創建XBitmap實例
	 * 
	 * @param context
	 *            上下文
	 * @param diskCachePath
	 *            設置設備本地緩存路徑
	 * @param memoryCacheSize
	 *            設置內存緩存大小，單位為Byte，最小為1024*1024*2=2MB，
	 *            在BitmapGlobalConfig.MIN_MEMORY_CACHE_SIZE 中設置
	 * @param diskCacheSize
	 *            設置設備本地緩存大小，單位為Byte，最小為1024*1024*10=10MB,
	 *            在BitmapGlobalConfig.MIN_DISK_CACHE_SIZE 中設置
	 */
	public XBitmap(Context context, String diskCachePath, int memoryCacheSize,
			int diskCacheSize) {
		this(context, diskCachePath);
		globalConfig.setMemoryCacheSize(memoryCacheSize);
		globalConfig.setDiskCacheSize(diskCacheSize);
	}

	/**
	 * 創建XBitmap實例
	 * 
	 * @param context
	 *            上下文
	 * @param diskCachePath
	 *            設置設備本地緩存路徑
	 * @param memoryCachePercent
	 *            設置應用緩存在APK總內存的百分比， 優先級高於memoryCacheSize，值的範圍是 0.05 到 0.8 之间
	 */
	public XBitmap(Context context, String diskCachePath,
			float memoryCachePercent) {
		this(context, diskCachePath);
		globalConfig.setMemCacheSizePercent(memoryCachePercent);
	}

	/**
	 * 創建XBitmap實例
	 * 
	 * @param context
	 *            上下文
	 * @param diskCachePath
	 *            設置設備本地緩存路徑
	 * @param memoryCachePercent
	 *            設置應用緩存在APK總內存的百分比， 優先級高於memoryCacheSize，值的範圍是 0.05 到 0.8 之间
	 * @param diskCacheSize
	 *            設置設備本地緩存大小，單位為Byte，最小為1024*1024*10=10MB,
	 *            在BitmapGlobalConfig.MIN_DISK_CACHE_SIZE 中設置
	 */
	public XBitmap(Context context, String diskCachePath,
			float memoryCachePercent, int diskCacheSize) {
		this(context, diskCachePath);
		globalConfig.setMemCacheSizePercent(memoryCachePercent);
		globalConfig.setDiskCacheSize(diskCacheSize);
	}

	// ////////////////////////////////////// config // ////////////////////////////////////////////////////////////////////

	/**
	 * 設置加載圖片過程中默認顯示的圖片
	 * 
	 * @param bitmap
	 * @return
	 */
	public XBitmap configDefaultLoadingImage(Drawable bitmap) {
		defaultDisplayConfig.setLoadingDrawable(bitmap);
		return this;
	}

	/**
	 * 設置設備默認的本地緩存路徑
	 * 
	 * @param diskCachePath
	 *            設置設備的本地緩存路徑，如果設備掛載SDCard就直接把緩存文件存儲到SDCard中，
	 *            例如：Android/應用包名/cache/diskCachePath/ 目錄下
	 *            如果設備沒有掛載SDCard就直接把緩存文件存儲到系統的內存空間中，
	 *            例如：/data/data/應用包名/cache/diskCachePath/ 目錄下 <br/>
	 * <br/>
	 *            注意：如果diskCachePath為空（null 或者
	 *            "")就默認使用BitmapGlobalConfig.diskCachePath
	 * @return
	 */
	public XBitmap configDefaultDiskCachePath(String diskCachePath) {
		if(!TextUtils.isEmpty(diskCachePath))
			globalConfig.setDiskCachePath(diskCachePath);
		return this;
	}

	/**
	 * 設置加載圖片過程中的默認顯示的圖片
	 * 
	 * @param resId
	 *            圖片資源ID
	 * @return
	 */
	public XBitmap configDefaultLoadingImage(int resId) {
		defaultDisplayConfig.setLoadingDrawable(mContext.getResources()
				.getDrawable(resId));
		return this;
	}

	/**
	 * 設置加載圖片失敗之後默認顯示的圖片
	 * 
	 * @param bitmap
	 * @return
	 */
	public XBitmap configDefaultLoadFailedImage(Drawable bitmap) {
		defaultDisplayConfig.setLoadFailedDrawable(bitmap);
		return this;
	}

	/**
	 * 設置加載圖片失敗之後默認顯示的圖片
	 * 
	 * @param resId
	 *            圖片資源ID
	 * @return
	 */
	public XBitmap configDefaultLoadFailedImage(int resId) {
		defaultDisplayConfig.setLoadFailedDrawable(mContext.getResources()
				.getDrawable(resId));
		return this;
	}

	/**
	 * 設置圖片最大顯示的寬度
	 * 
	 * @param bitmapWidth
	 * @return
	 */
	public XBitmap configDefaultBitmapMaxWidth(int bitmapWidth) {
		defaultDisplayConfig.setBitmapMaxWidth(bitmapWidth);
		return this;
	}

	/**
	 * 設置圖片最大顯示的高度
	 * 
	 * @param bitmapHeight
	 * @return
	 */
	public XBitmap configDefaultBitmapMaxHeight(int bitmapHeight) {
		defaultDisplayConfig.setBitmapMaxHeight(bitmapHeight);
		return this;
	}

	/**
	 * 設置加載圖片成功之後的動畫效果
	 * 
	 * @param animation
	 *            動畫
	 * @return
	 */
	public XBitmap configDefaultImageLoadAnimation(Animation animation) {
		defaultDisplayConfig.setAnimation(animation);
		return this;
	}

	/**
	 * 設置圖片加載之後（成功或者失敗）的囘調，可以自定義類實現 x.bitmap.callback.ImageLoadCallBack 接口
	 * 
	 * @param imageLoadCallBack
	 *            囘調
	 * @return
	 */
	public XBitmap configDefaultImageLoadCallBack(
			ImageLoadCallBack imageLoadCallBack) {
		defaultDisplayConfig.setImageLoadCallBack(imageLoadCallBack);
		return this;
	}

	/**
	 * 設置圖片是否按照圖片資源原型加載
	 * 
	 * @param showOriginal
	 *            為true是按照圖片資源原型顯示，如果圖片很大就解析失敗，拋出OOM異常，
	 *            為false是表示根據圖片資源實際大小情況對圖片進行採樣顯示，避免出現OOM異常
	 * @return
	 */
	public XBitmap configDefaultShowOriginal(boolean showOriginal) {
		defaultDisplayConfig.setShowOriginal(showOriginal);
		return this;
	}

	/**
	 * 設置圖片顯示的質量參數，例如 Bitmap.Config.ARGB_8888
	 * 
	 * @param config
	 * @return
	 */
	public XBitmap configDefaultBitmapConfig(Bitmap.Config config) {
		defaultDisplayConfig.setBitmapConfig(config);
		return this;
	}

	/**
	 * 設置圖片顯示的配置
	 * 
	 * @param displayConfig
	 * @return
	 */
	public XBitmap configDefaultDisplayConfig(BitmapDisplayConfig displayConfig) {
		defaultDisplayConfig = displayConfig;
		return this;
	}

	/**
	 * 設置下載圖片的下載器，可以自定義類實現x.bitmap.download.Downloader接口
	 * 
	 * @param downloader
	 * @return
	 */
	public XBitmap configDownloader(Downloader downloader) {
		globalConfig.setDownloader(downloader);
		return this;
	}

	/**
	 * 設置緩存過期時間，單位為毫秒，默認為30天
	 * 
	 * @param defaultExpiry
	 * @return
	 */
	public XBitmap configDefaultCacheExpiry(long defaultExpiry) {
		globalConfig.setDefaultCacheExpiry(defaultExpiry);
		return this;
	}

	/**
	 * 設置下載圖片的線程池開啟的線程數量，默認為5個
	 * 
	 * @param threadPoolSize
	 *            線程池中線程的數量
	 * @return
	 */
	public XBitmap configThreadPoolSize(int threadPoolSize) {
		globalConfig.setThreadPoolSize(threadPoolSize);
		return this;
	}

	/**
	 * 設置內存緩存是否可用
	 * 
	 * @param enabled
	 *            為true可用，為false不可用
	 * @return
	 */
	public XBitmap configMemoryCacheEnabled(boolean enabled) {
		globalConfig.setMemoryCacheEnabled(enabled);
		return this;
	}

	/**
	 * 設置設備本地緩存是否可用
	 * 
	 * @param enabled
	 *            為true可用，為false不可用
	 * @return
	 */
	public XBitmap configDiskCacheEnabled(boolean enabled) {
		globalConfig.setDiskCacheEnabled(enabled);
		return this;
	}

	/**
	 * 設置設備本地緩存的圖片的壓縮質量
	 * 
	 * @param compressQuality
	 *            值的範圍為0-100。 0表示壓縮率最大，也就是圖片質量損失最大，100表示無壓縮，原圖片緩存，默認值為100。
	 * @return
	 */
	public XBitmap configDefaultCompressQuality(int compressQuality) {
		globalConfig.setCompressQuality(compressQuality);
		return this;
	}

	/**
	 * 設置設備本地緩存的圖片的壓縮格式,但是前提條件是必須設置 {@link configDefaultDynamicFormat} 為false才起作用
	 * 
	 * @param compressFormat
	 *            值為Bitmap.CompressFormat.PNG 和 Bitmap.CompressFormat.JPEG ,
	 *            默認為Bitmap.CompressFormat.JPEG
	 * @return
	 */
	public XBitmap configDefaultCompressFormat(
			Bitmap.CompressFormat compressFormat) {
		globalConfig.setCompressFormat(compressFormat);
		return this;
	}

	/**
	 * 設置本地圖片資源合成路徑的分隔符
	 * @param compositePathSeparater 本地圖片資源合成路徑的分隔符,默認為@
	 * @return
	 */
	public XBitmap configDefaultCompositePathSeparater( String compositePathSeparater) {
		if (!TextUtils.isEmpty(compositePathSeparater))
			globalConfig.setCompositePathSeparater(compositePathSeparater);

		return this;
	}
	
	/**
	 * 設置從網絡圖片資源Uri中获取下載到設備本地的缓存目錄的分隔符 , 默認為"//"
	 * @param splitUriStartSeparater 網絡圖片資源Uri中获取下載到設備本地的缓存目錄的分隔符,
	 * 例如網絡圖片資源的Uri為 http://mbook.nmg.hk/upload/2013/reader3/issue/cover/13/10/222.jpg ,
	 * 若設置Uri截取分隔符為"mbook.nmg.hk" ,那麼圖片資源下載到設備本地的目錄為:
	 * 緩存根目錄/upload/2013/reader3/issue/cover/13/10/ , 注意不包含文件名稱
	 * @return
	 */
	public XBitmap configDefaultSplitUriStartSeparater(String splitUriStartSeparater){
		if (!TextUtils.isEmpty(splitUriStartSeparater))
			globalConfig.setSplitUriStartSeparater(splitUriStartSeparater);
		
		return this;
	}

	/**
	 * 設置緩存监听，例如清除緩存完成之後需要執行哪些操作
	 * 
	 * @param listener
	 * @return
	 */
	public XBitmap configBitmapCacheListener(BitmapCacheListener listener) {
		globalConfig.setBitmapCacheListener(listener);
		return this;
	}

	/**
	 * 設置全局配置
	 * 
	 * @param globalConfig
	 * @return
	 */
	public XBitmap configGlobalConfig(BitmapGlobalConfig globalConfig) {
		this.globalConfig = globalConfig;
		return this;
	}
	
	/**
	 * 設置圖片資源壓縮格式是否動態的,默認為true,例如 http://mbook.nmg.hk/upload/ipad2011/issue/cover/331.png ,就以PNG格式壓縮
	 * @param isDynamicFormat 如果為true則動態根據文件名稱後綴作為格式,如果為false則是根據{@link configDefaultCompressFormat} 的格式
	 * @return
	 */
	public XBitmap configDefaultDynamicFormat(boolean isDynamicFormat){
		this.globalConfig.setDynamicFormat(isDynamicFormat);
		return this;
	}

	// ///////////////////////// get ////////////////////////////////

	/**
	 * 獲取到Bitmap默認顯示的配置
	 * 
	 * @return
	 */
	public BitmapDisplayConfig getDefaultDisplayConfig() {
		return this.defaultDisplayConfig;
	}

	// ////////////////////////// set ////////////////////////////////////
	
	/**
	 * 設置上下文,類內部使用的
	 * @param context 上下文
	 * @return
	 */
	private XBitmap setContext(Context context){
		this.mContext=context;
		globalConfig=xBitmapGlobalConfigs.get(this);
		if(null==(globalConfig)){
			globalConfig = new BitmapGlobalConfig(context, null);
			xBitmapGlobalConfigs.put(this, globalConfig);
		} 
		defaultDisplayConfig=xBitmapDisplayConfigs.get(globalConfig);
		if(null==defaultDisplayConfig){
			defaultDisplayConfig = new BitmapDisplayConfig(context);
			xBitmapDisplayConfigs.put(globalConfig, defaultDisplayConfig);
		}
		defaultDisplayConfig.setBitmapGlobalConfig(globalConfig);
		return this;
	}


	// //////////////////////// display ////////////////////////////////////

	/**
	 * 顯示圖片
	 * @param imageView
	 * @param uri 網絡圖片資源Uri
	 */
	public void display(ImageView imageView, String uri) {
		display(imageView, uri, null,-1);
	}
	
	/**
	 * 顯示圖片
	 * @param imageView
	 * @param uri 網絡圖片資源Uri
	 */
	public void display(ImageView imageView, String uri,long lastModify) {
		display(imageView, uri, null,lastModify);
	}
	

	/**
	 * 顯示圖片
	 * @param imageView ImageView實例
	 * @param uri 網絡圖片資源Uri
	 * @param displayConfig 圖片顯示配置
	 */
	public void display(ImageView imageView, String uri,
			BitmapDisplayConfig displayConfig,long lastModify ) {
		display(null, imageView, uri, displayConfig, lastModify);
	}
	
	/**
	 * 顯示圖片
	 * @param loadNetListener
	 * @param imageView
	 * @param uri
	 * @param lastModify
	 */
	public void display(BitmapLoadNetListener loadNetListener,ImageView imageView,String uri,long lastModify){
		display(loadNetListener, imageView, uri, null, lastModify);
	}
	
	/**
	 * 顯示圖片
	 * @param loadNetListener
	 * @param imageView
	 * @param uri
	 * @param displayConfig
	 * @param lastModify
	 */
	public void display(BitmapLoadNetListener loadNetListener,ImageView imageView,String uri,BitmapDisplayConfig displayConfig,long lastModify){
		doDisplay(loadNetListener, imageView, uri, displayConfig, lastModify);
	}
	
	private void doDisplay(BitmapLoadNetListener loadNetListener,ImageView imageView, String uri,BitmapDisplayConfig displayConfig,long lastModify) {
		boolean result=initParameter(null, imageView, uri, displayConfig);
		if(!result) return;

		Bitmap bitmap = null;
		
		if (displayConfig == null) {
			displayConfig = defaultDisplayConfig;
		}

		bitmap = globalConfig.getBitmapCache().getBitmapFromMemCache(uri,
				displayConfig,lastModify);

		 if(null!=bitmap){
			 if(null!=loadNetListener){
				 loadNetListener.onCompleted(bitmap, displayConfig);
				 return;
			 }
				imageView.setImageBitmap(bitmap);
				return;
		} 

		displayConfig.setBitmapGlobalConfig(globalConfig);

		final NetBitmapResourceLoadAsyncTask loadTask = new NetBitmapResourceLoadAsyncTask(
				mContext,loadNetListener, imageView, displayConfig, lastModify, pauseTaskLock);

		final AsyncBitmapDrawable asyncBitmapDrawable = new AsyncBitmapDrawable(
				displayConfig.getLoadingDrawable(), loadTask);
		imageView.setImageDrawable(asyncBitmapDrawable);

		// 從Uri或者設備本地加載Bitmap
		loadTask.executeOnExecutor(globalConfig.getBitmapLoadExecutor(), uri);
	}
	

	private void setDefaultLoadResource(BitmapLoadLocalListener loadLocalListener,ImageView imageView,BitmapDisplayConfig displayConfig){
		Drawable drawable = imageView.getDrawable();
		
		if(null!=drawable && null!=loadLocalListener){
			displayConfig.setLoadingDrawable(drawable);
			displayConfig.setLoadFailedDrawable(drawable);
		}
	}
	
	private void doSimpleLoad(BitmapLoadLocalListener loadLocalListener, ImageView imageView, String filename,BitmapDisplayConfig displayConfig ) {
		boolean result=initParameter(loadLocalListener, imageView, filename, displayConfig);
		
		if(!result) return;
		synchronized (pauseTaskLock) {
			if(null!=imageView.getDrawable() && pauseLoadLocalResource){
				//暫停加載大圖
				return;
			}
		}
		
		if (displayConfig == null) {
			displayConfig = defaultDisplayConfig;
		}
		setDefaultLoadResource(loadLocalListener, imageView, displayConfig);
		Bitmap bitmap= globalConfig.getBitmapCache().getBitmapFromMemoryCacheByLocalFile(filename, displayConfig);
		if (null != bitmap && null!=loadLocalListener) {
			loadLocalListener.onCompleted( bitmap, displayConfig);
			return;
		}
		displayConfig.setBitmapGlobalConfig(globalConfig);
		synchronized (pauseTaskLock) {
			final SimpleLocalBitmapResourceLoadTask loadTask = new SimpleLocalBitmapResourceLoadTask(
					mContext, loadLocalListener, imageView, displayConfig,
					pauseTaskLock);

			final AsyncBitmapDrawable asyncBitmapDrawable = new AsyncBitmapDrawable(
					displayConfig.getLoadingDrawable(), loadTask);
			imageView.setImageDrawable(asyncBitmapDrawable);

			// 從本地資源加載Bitmap
			loadTask.executeOnExecutor(globalConfig.getBitmapLoadExecutor(),
					filename);
		}
	}

	private boolean  initParameter(BitmapLoadLocalListener loadLocalListener, ImageView imageView,
			String uri, BitmapDisplayConfig displayConfig) {
		
		if (imageView == null) {
			return false;
		}
		
		if (TextUtils.isEmpty(uri) || uri.equals("null")) {
			if(null!=loadLocalListener){
				loadLocalListener.onCompleted(null, displayConfig);
			}else{
				displayConfig.getImageLoadCallBack().loadFailed(imageView,
					displayConfig.getLoadFailedDrawable());
			}
			return false;
		}
		return true;
	}
	
	
	private void doCombineLoad(BitmapLoadLocalListener loadLocalListener, ImageView imageView, String combineFileName,BitmapDisplayConfig displayConfig ) {
		boolean result=initParameter(loadLocalListener, imageView, combineFileName, displayConfig);
		if(!result) return;
		
		synchronized (pauseTaskLock) {
			if(null!=imageView.getDrawable() && pauseLoadLocalResource){
				//暫停加載大圖
				return;
			}
		}
		
		if (displayConfig == null) {
			displayConfig = defaultDisplayConfig;
		}
		
		setDefaultLoadResource(loadLocalListener, imageView, displayConfig);
		Bitmap bitmap= globalConfig.getBitmapCache().getBitmapFromMemoryCacheByLocalFile(combineFileName, displayConfig);
		if (null != bitmap && null!=loadLocalListener) {
			loadLocalListener.onCompleted(bitmap, displayConfig);
			return;
		}
		displayConfig.setBitmapGlobalConfig(globalConfig);
		synchronized (pauseTaskLock) {
			final CombineLocalBitmapResourceLoadTask loadTask = new CombineLocalBitmapResourceLoadTask(
					mContext, loadLocalListener, imageView, displayConfig,
					pauseTaskLock);

			// 設置正在加載的圖片資源
			final AsyncBitmapDrawable asyncBitmapDrawable = new AsyncBitmapDrawable(
					displayConfig.getLoadingDrawable(), loadTask);
			imageView.setImageDrawable(asyncBitmapDrawable);

			// 從設備本地資源加載Bitmap
			loadTask.executeOnExecutor(globalConfig.getBitmapLoadExecutor(),
					combineFileName);
		}
	}
	
	

	/**
	 * 從設備本地加載圖片資源
	 * @param imageView ImageView實例
	 * @param filename 設備本地圖片資源全路徑名稱
	 */
	public void load(ImageView imageView, String filename) {
		load(imageView, filename, null);
	}
	
	public void load(BitmapLoadLocalListener loadLocalListener,ImageView imageView, String filename) {
		load(loadLocalListener,imageView, filename, null);
	}
	
	/**
	 * 從設備本地加載圖片資源
	 * @param filename
	 *            設備本地圖片資源文件全路徑名稱
	 * @param displayConfig
	 *            顯示配置
	 */
	public void load(BitmapLoadLocalListener loadLocalListener,ImageView imageView, String filename,
			BitmapDisplayConfig displayConfig) {
		doSimpleLoad(loadLocalListener, imageView, filename, displayConfig);
	}

	/**
	 * 從設備本地加載圖片資源
	 * @param filename
	 *            設備本地圖片資源文件全路徑名稱
	 * @param displayConfig
	 *            顯示配置
	 */
	public void load(ImageView imageView, String filename,
			BitmapDisplayConfig displayConfig) {
		load(null, imageView, filename,displayConfig);
	}
	
	/**
	 * 從設備本地加載合成路徑的圖片資源 
	 * @param imageView ImageView實例
	 * @param filename 設備本地圖片資源合成全路徑名稱,默認的合成路徑分隔符為@
	 * 例如: /mnt/sdcard/aa_reader3/book/3000034/book40/1.jpg@/mnt/sdcard/aa_reader3/book/3000034/book40/2.jpg
	 */
	public void loadCombineResource(ImageView imageView, String compositeFilename) {
		loadCombineResource(imageView, compositeFilename, null);
	}
	
	/**
	 * 從設備本地加載合成路徑的圖片資源 
	 * @param filename
	 *            設備本地圖片資源文件合成全路徑名稱,默認的合成路徑分隔符為@
	 * @param displayConfig
	 *            顯示配置
	 */
	public void loadCombineResource(BitmapLoadLocalListener loadLocalListener, ImageView imageView, String combineFileName) {
		loadCombineResource(loadLocalListener, imageView, combineFileName,null);
	}
	
	/**
	 * 從設備本地加載合成路徑的圖片資源 
	 * @param filename
	 *            設備本地圖片資源文件合成全路徑名稱,默認的合成路徑分隔符為@
	 * @param displayConfig
	 *            顯示配置
	 */
	public void loadCombineResource(BitmapLoadLocalListener loadLocalListener, ImageView imageView, String combineFileName,
			BitmapDisplayConfig displayConfig) {
		doCombineLoad(loadLocalListener, imageView, combineFileName, displayConfig);
	}
	
	/**
	 * 從設備本地加載合成路徑的圖片資源 
	 * @param filename
	 *            設備本地圖片資源文件合成全路徑名稱,默認的合成路徑分隔符為@
	 * @param displayConfig
	 *            顯示配置
	 */
	public void loadCombineResource(ImageView imageView, String combineFileName,
			BitmapDisplayConfig displayConfig) {
		loadCombineResource(null, imageView, combineFileName,displayConfig);
	}

	// ///////////////////////////////////////////// cache // /////////////////////////////////////////////////////////////////

	/**
	 * 清除所有的緩存，包含內存緩存和設備本地緩存
	 */
	public void clearCache() {
		globalConfig.clearCache();
	}

	/**
	 * 清除內存緩存
	 */
	public void clearMemoryCache() {
		globalConfig.clearMemoryCache();
	}

	/**
	 * 清除設備本地緩存
	 */
	public void clearDiskCache() {
		globalConfig.clearDiskCache();
	}

	/**
	 * 根據Uri清除緩存，包含內存緩存和設備本地緩存
	 * 
	 * @param uri
	 * @param config
	 */
	public void clearCache(String uri, BitmapDisplayConfig config) {
		if (config == null) {
			config = defaultDisplayConfig;
		}
		globalConfig.clearCache(uri, config);
	}

	/**
	 * 根據Uri釋放內存緩存
	 * 
	 * @param uri
	 * @param config
	 */
	public void clearMemoryCache(String uri, BitmapDisplayConfig config) {
		if (config == null) {
			config = defaultDisplayConfig;
		}
		globalConfig.clearMemoryCache(uri, config);
	}

	/**
	 * 根據Uri清除設備本地緩存
	 * 
	 * @param uri
	 */
	public void clearDiskCache(String uri) {
		globalConfig.clearDiskCache(uri);
	}

	/**
	 * 根據Uri從設備本地緩存中獲取文件
	 * 
	 * @param uri
	 * @return
	 */
	public File getBitmapFileFromDiskCache(String uri) {
		return globalConfig.getBitmapCache().getBitmapFileFromDiskCache(uri);
	}

	/**
	 * 根據Uri從內存緩存中獲取Bitmap
	 * 
	 * @param uri
	 * @param displayConfig
	 * @return
	 */
	public Bitmap getBitmapFromMemCache(String uri,
			BitmapDisplayConfig displayConfig) {
		return globalConfig.getBitmapCache().getBitmapFromMemCache(uri,
				displayConfig);
	}

	// //////////////////////////////////////// tasks // //////////////////////////////////////////////////////////////////////

	/**
	 * 重新恢復下載圖片資源的線程任務
	 */
	public void resumeTasks() {
		pauseTask = false;
		synchronized (pauseTaskLock) {
			pauseTaskLock.notifyAll();
		}
	}
	
	/**
	 * 獲取目標線程任務實例
	 * @param imageView
	 * @return
	 */
	public static CompatibleAsyncTask  getBitmapTaskFromImageView(ImageView imageView) {
		if (imageView != null) {
			final Drawable drawable = imageView.getDrawable();
			if (drawable instanceof AsyncBitmapDrawable) {
				final AsyncBitmapDrawable asyncBitmapDrawable = (AsyncBitmapDrawable) drawable;
				return asyncBitmapDrawable.getBitmapWorkerTask();
			} 
		}
		return null;
	}

	/**
	 * 暫停下載資源的線程任務
	 */
	public void pauseTasks() {
		pauseTask = true;
//		flushCache();
	}

	/**
	 * 停住下載資源的線程任務
	 */
	public void stopTasks() {
		pauseTask = true;
		synchronized (pauseTaskLock) {
			pauseTaskLock.notifyAll();
		}
	}
	
	/**
	 * 如果是先加載縮略圖再加載大圖就可以滑動事件中調用暫停加載大圖
	 */
	public void pauseLoadLocalResource(){
		pauseLoadLocalResource=true;
	}
	
	/**
	 * 如果是先加載縮略圖再加載大圖就可以滑動事件中調用開啟加載大圖
	 */
	public void resumeLoadLocalResource(){
		pauseLoadLocalResource=false;
	}
	
	@SuppressWarnings("rawtypes")
	public synchronized static void cancelTasks(int count) {
		if(count<=2)return;

		if (XBitmap.bitmapLoadTasks.size() >=count) {
			for (int i = XBitmap.bitmapLoadTasks.size() - 1; i >= count; i--) {
				CompatibleAsyncTask task = XBitmap.bitmapLoadTasks.remove(i);
				if (!task.isCancelled()) {
					task.cancel(true);
				}
				task = null;
			}
		}
	}
	
	@SuppressWarnings("rawtypes")
	public synchronized static void cancelTasksAll(){
		for (int i = XBitmap.bitmapLoadTasks.size() - 1; i >= 0; i--) {
			CompatibleAsyncTask task = XBitmap.bitmapLoadTasks.remove(i);
			if (!task.isCancelled()) {
				task.cancel(true);
			}
			task = null;
		}
		bitmapLoadTasks.clear();
	}

	/**
	 * 可以在Activity的 onResume()方法中調用，重新恢復線程任務執行
	 */
	public void onResume() {
		resumeTasks();
	}

	/**
	 * 可以在Activity的onPause()方法中調用，暫停線程任務執行
	 * <p>
	 * 建議使用 XBitmap.create(Context context) 創建XBitmap實例
	 * </p>
	 */
	public void onPause() {
		pauseTasks();
	}

	/**
	 * 可以在Activity的onStop()方法中調用，停止線程任務執行
	 * <p>
	 * 建議使用 XBitmap.create(Context context) 創建XBitmap實例
	 * </p>
	 */
	public void onStop() {
		stopTasks();
	}

	/**
	 * 可以在Activity的onDestroy()方法中調用，釋放內存緩存
	 * <p>
	 * 建議使用 XBitmap.create(Context context) 創建XBitmap實例
	 * </p>
	 */
	public void onDestroy() {
		clearMemoryCache();
	}


	
	/**
	 * 內部類
	 * @author Hero
	 */
	private static  class InnerBitmap{
		private static  final  InnerBitmap instance=new InnerBitmap();
		private InnerBitmap(){
		}
		private static InnerBitmap getInstance(){
			return instance;
		}
		
		private XBitmap xBitmap1=new XBitmap();
		private XBitmap xBitmap2=new XBitmap();
		private XBitmap xBitmap3=new XBitmap();
		private XBitmap xBitmap4=new XBitmap();
		private XBitmap xBitmap5=new XBitmap();
	}
}
