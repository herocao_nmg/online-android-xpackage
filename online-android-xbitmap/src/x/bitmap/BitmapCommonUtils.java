/*
 *Copyright (c) 2013. ccx (ccxandroid@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package x.bitmap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;

import nmg.util.XOR;
import x.util.IOUtils;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;

/**
 * Bitmap公共工具類
 * @author Hero
 * @since 2013-10-21
 */
public class BitmapCommonUtils {

    private static final String TAG = "BitmapCommonUtils";
    
    private static Bitmap mBitmap;
  	private static Canvas mCanvas;
  	private static int mFirstWidth;
  	private static int mFirstHeight;
  	private static int mSecondWidth;
  	
  	private static final Config config=Config.RGB_565;

    private BitmapCommonUtils(){
    }
    
    /**
     * 獲取SDCard的緩存目錄
     * @param context
     * @param dirName
     * @return
     */
	public static String getDiskCacheDir(Context context, String dirName) {
    	 String cachePath = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ?
               context.getExternalCacheDir().getPath() : context.getCacheDir().getPath();
    
        return cachePath + File.separator + dirName;
    }
    
	/**
	 * 獲取Bitmap對應的大小，單位為byte
	 * @param bitmap
	 * @return
	 */
    public static int getBitmapSize(Bitmap bitmap) {
        if (bitmap == null) return 0;
        return bitmap.getRowBytes() * bitmap.getHeight();
    }

    /**
     * 獲取指定目錄的可用空間大小，單位byte
     * @param dir
     * @return
     */
    @SuppressWarnings("deprecation")
	public static long getAvailableSpace(File dir) {
        try {
            final StatFs stats = new StatFs(dir.getPath());
            return (long) stats.getBlockSize() * (long) stats.getAvailableBlocks();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }

    }
    
    /**
     * 將byte的單位的數據轉換為合適的單位，便於閱讀，例如：1024byte*1024byte —> 1KB
     * @param size
     * @return
     */
    public static String getByteSizeData(long size){
    	if(size<=0) return "size error";
    	
		DecimalFormat decimalFormat=new DecimalFormat("####.00");
		
		if(size<1024f){
			return decimalFormat.format(size)+"byte";
		}else if(size<1024f*1024f){  
			float kbSize=size/(1024f);   
			return decimalFormat.format(kbSize)+"KB";
		}else if(size<1024f*1024f*1024f){
			float mbSize=size/(1024f*1024f);
			return decimalFormat.format(mbSize)+"MB";
		}else if(size<1024f*1024f*1024f*1024){
			float gbSize=size/(1024f*1024*1024);
			return decimalFormat.format(gbSize)+"GB";
		}else{
			return "size error";
		}
	}
    
   /**
    * 獲取設備系統內部可用內存大小，單位為byte
    * @return
    */
	public static long getAvailableInternalMemorySize() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}

	/**
	 * 獲取設備系統內部空間大小，單位為byte
	 * @return
	 */
	public static long getTotalInternalMemorySize() {
		File path = Environment.getDataDirectory();
		StatFs stat = new StatFs(path.getPath());
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return totalBlocks * blockSize;
	}

	/**
	 * 獲取設備外部存儲卡可用內存空間大小，單位為byte
	 * @return
	 */
	public static long getAvailableExternalMemorySize() {
		if (externalMemoryAvailable()) {
			File path = Environment.getExternalStorageDirectory();
			StatFs stat = new StatFs(path.getPath());
			long blockSize = stat.getBlockSize();
			long availableBlocks = stat.getAvailableBlocks();
			return availableBlocks * blockSize;
		} else {
			throw new RuntimeException("Don't have sdcard.");
		}
	}

	/**
	 * 获取手机外部空间大小
	 * 獲取設備外部存儲卡空間大小，單位為byte
	 * @return
	 */
	public static long getTotalExternalMemorySize() {
		if (externalMemoryAvailable()) {
			File path = Environment.getExternalStorageDirectory();
			StatFs stat = new StatFs(path.getPath());
			long blockSize = stat.getBlockSize();
			long totalBlocks = stat.getBlockCount();
			return totalBlocks * blockSize;
		} else {
			throw new RuntimeException("Don't have sdcard.");
		}
	}
	
	
	/**
	 * 返回Bitmap寬高度比值
	 * @param bitmap
	 * @return
	 */
	public static double getBitmapWHRatio(Bitmap bitmap){
		int imgWidth = bitmap.getWidth();
		int imgHeight = bitmap.getHeight();
		double w_divide_h = (double) imgWidth / (double) imgHeight; //宽高比
		return w_divide_h;
	}
	

	/**
	 * 是否掛載SDCard
	 * @return
	 */
	public static boolean externalMemoryAvailable() {
		return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	}

	/**
	 * 從圖片資源文件中獲取到Byte[]
	 * @param filepath 圖片資源文件全路徑名稱
	 * @return
	 */
	public static byte[] getBitmapByteFromFile(String filepath){
		
		boolean isXOR = false;
		
		if(null==filepath) return null;
		
		filepath = filepath.toLowerCase();
		if(filepath.endsWith(".xjpg")){
			isXOR = true;
		}
		
		try{
		
		FileInputStream fis=new FileInputStream(filepath);
		
		byte[] buff=new byte[1024];
		int len=-1;
		
		ByteArrayOutputStream baos=new ByteArrayOutputStream();
		
		while((len=fis.read(buff))!=-1){
				baos.write(buff, 0, len);
		}
		baos.flush();
		
		byte[] data=baos.toByteArray();
		
		IOUtils.close(baos);
		IOUtils.close(fis);
		
		if(isXOR){
			data = XOR.encrypt(data);
		}	
		return data;
		}catch(Exception e){
			e.printStackTrace();
		}
	return null;
	}
	
	/**
	 * 合成Bitmap
	 * @param firstBitmap 第一個Bitmap實例
	 * @param secondBitmap 第二個Bitmap實例
	 * @return
	 */
	public static Bitmap compositeBitmap(Bitmap firstBitmap, Bitmap secondBitmap) {
		if (null==firstBitmap || null==secondBitmap) {
			return null;
		}

		int bgWidth = firstBitmap.getWidth();
		int bgHeight = firstBitmap.getHeight();
		int fgWidth = secondBitmap.getWidth();
		
		Bitmap bitmap=null;
		try{
		bitmap= Bitmap.createBitmap(bgWidth + fgWidth, bgHeight, Config.RGB_565);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		Canvas canvas = new Canvas(bitmap);
		canvas.drawBitmap(firstBitmap, 0, 0, null); 
		canvas.drawBitmap(secondBitmap, bgWidth, 0, null); 
//		cv.save(Canvas.ALL_SAVE_FLAG);// 保存
//		cv.restore();// 存儲
		canvas=null;
		return bitmap;
	}
	
	/**
	 * 合成縮略圖Bitmap
	 * @param firstBitmap 第一個Bitmap實例
	 * @param secondBitmap 第二個Bitmap實例
	 * @param config 
	 * @return
	 */
	public static Bitmap compositeThumbnailBitmap(Bitmap firstBitmap, Bitmap secondBitmap,Config config) {
		if (null==firstBitmap || null==secondBitmap) {
			return null;
		}
		
		if(null==config) config=BitmapCommonUtils.config;
		
		int firstWidth = firstBitmap.getWidth();
		int firstHeight = firstBitmap.getHeight();
		int secondWidth = secondBitmap.getWidth();
		Bitmap bitmap = Bitmap.createBitmap(firstWidth + secondWidth,
				firstHeight, config);

		Canvas canvas = new Canvas(bitmap);
		canvas.drawBitmap(firstBitmap, 0, 0, null);
		canvas.drawBitmap(secondBitmap, firstWidth, 0, null);
		canvas = null;
		return bitmap;
	}
	
	/**
	 * 合成大圖Bitmap
	 * @param firstBitmap 第一個Bitmap實例
	 * @param secondBitmap 第二個Bitmap實例
	 * @param config 
	 * @return
	 */
	public static Bitmap compositeBigpictureBitmap(Bitmap firstBitmap, Bitmap secondBitmap,Config config) {
		if (null == firstBitmap || null == secondBitmap) {
			return null;
		}

		if (null == config)
			config = BitmapCommonUtils.config;

		int firstWidth = firstBitmap.getWidth();
		int firstHeight = firstBitmap.getHeight();
		int secondWidth = secondBitmap.getWidth();

		if (null == mBitmap || mFirstHeight != firstHeight
				|| mFirstWidth != firstWidth || mSecondWidth != secondWidth) {
			mBitmap = Bitmap.createBitmap(firstWidth + secondWidth,
					firstHeight, config);
			mFirstHeight = firstHeight;
			mFirstWidth = firstWidth;
			mSecondWidth = secondWidth;
		}

		if (null == mCanvas || mFirstHeight != firstHeight
				|| mFirstWidth != firstWidth || mSecondWidth != secondWidth) {
			mCanvas = new Canvas(mBitmap);
		}

		mCanvas.drawBitmap(firstBitmap, 0, 0, null);
		mCanvas.drawBitmap(secondBitmap, firstWidth, 0, null);
		return mBitmap;
	}
	
	/**
	 * 根據PNG格式獲取Uri
	 * @param uri 圖片資源Uri
	 * @return
	 */
	public static String getPathByCompressFormat(BitmapGlobalConfig globalConfig,String uri) {
		if(null!=uri &&null!=globalConfig && globalConfig.getCompressFormat()==CompressFormat.PNG){
			uri=uri.substring(0, uri.lastIndexOf(".")+1)+"png";
		}
		return uri;
	}
	
	
	/**
	 * 根據PNG格式獲取Uri
	 * @param uri 圖片資源Uri
	 * @return
	 */
	public static String getPathByCompressFormat(CompressFormat compressFormat ,String uri) {
		if(null!=uri &&  compressFormat==CompressFormat.PNG){
			uri=uri.substring(0, uri.lastIndexOf(".")+1)+"png";
		}
		return uri;
	}
	
	/**
	 * 獲取路徑,動態文件格式
	 * @param globalConfig
	 * @param uri
	 * @return
	 */
	public static String getPath(BitmapGlobalConfig globalConfig,String uri){
		if(TextUtils.isEmpty(uri) || null==globalConfig) return null;
		
		if(!globalConfig.isDynamicFormat() && null!=globalConfig.getCompressFormat()){
			if(globalConfig.getCompressFormat()==Bitmap.CompressFormat.PNG){
				return getPath(globalConfig, uri)+".png";
			}else{
				return getPath(globalConfig, uri)+".jpg";
			}
		}
		return uri;
	}
	
	/**
	 * 獲取壓縮的質量
	 * @param globalConfig
	 * @return
	 */
	public static int getCompressQuality(BitmapGlobalConfig globalConfig){
		if (globalConfig.getCompressQuality() < 0 || globalConfig.getCompressQuality() > 100)  
			return 100;
		return globalConfig.getCompressQuality();
	}
	
	/**
	 * 獲取內存緩存的Key
	 * @param uri
	 * @param lastModify
	 * @return
	 */
	public static String getNetResourceMemoryCacheKey(BitmapDisplayConfig displayConfig,String uri,long lastModify){
		if (lastModify >= 0)
			return uri + lastModify + displayConfig.toString();
		return uri + displayConfig.toString();
	}
	
	/**
	 * 去掉Uri的後綴
	 * @param uri
	 * @param isCantainDot 是否包含點(.),如果為true則包含,如果為false則不包含
	 * @return
	 */
	public static String removeSuffix(String uri,boolean isCantainDot){
		if(TextUtils.isEmpty(uri)) return null;
		if(isCantainDot)
			return uri.substring(0,uri.lastIndexOf(".")+1);
		return uri.substring(0, uri.lastIndexOf("."));
	}
	
	/**
	 * 獲取壓縮格式
	 * @param globalConfig
	 * @param filename
	 * @return
	 */
	public static CompressFormat getCompressFormat(BitmapGlobalConfig globalConfig,String filename){
		if(null==globalConfig || TextUtils.isEmpty(filename)) return CompressFormat.JPEG;
		 if("png".equalsIgnoreCase(getSuffix(filename)))
				 return CompressFormat.PNG;
		 return CompressFormat.JPEG;
	}
	
	/**
	 * 獲取文件名稱
	 * @param globalConfig
	 * @param filename 文件名稱,例如: 123.jpg ,不要包含路徑例如: 11/22/33/456.jpg
	 * @return
	 */
	public static String getFileName(BitmapGlobalConfig globalConfig,String filename){
		if(TextUtils.isEmpty(filename) || null==globalConfig) return null;
		
		if(!globalConfig.isDynamicFormat() && null!=globalConfig.getCompressFormat()){
			if(globalConfig.getCompressFormat()==Bitmap.CompressFormat.PNG){
				return getFileName(filename, false)+".png";
			}else{
				return getFileName(filename, false)+".jpg";
			}
		}
		return filename;
	}
	
	
	/**
	 * 獲取文件名稱
	 * @param uri
	 * @param globalConfig
	 * @return
	 */
	public static String getFileName(String uri,BitmapGlobalConfig globalConfig){
		if(TextUtils.isEmpty(uri) || null==globalConfig) return null;
		return getFileName(globalConfig, getFileName(uri));
	}
	
	/**
	 * 根據Uri獲取文件名稱,包含後綴,例如 test.jpg
	 * @param uri 
	 * @return
	 */
	public  static String getFileName(String uri) {
		if (TextUtils.isEmpty(uri))
			return null;
		return uri.substring(uri.lastIndexOf("/") + 1);
	}
	
	/**
	 * 獲取文件名稱
	 * @param uri
	 * @param isContainSuffix 是否包含後綴,如果為true則包含,如果為false則不包含,例如 test 和 test.jpg
	 * @return
	 */
	public  static String getFileName(String uri,boolean isContainSuffix) {
		if (null == uri)
			return null;
		if(isContainSuffix)
			return uri.substring(uri.lastIndexOf("/") + 1);
		return uri.substring(uri.lastIndexOf("/") + 1,uri.lastIndexOf("."));
	}
	
	/**
	 *獲取文件的後綴,不包含點(.),例如: 11/test.txt 得到 txt
	 * @param filename
	 * @return
	 */
	public static String getSuffix(String filename){
		return getSuffix(filename, false);
	}
	
	/**
	 * 獲取文件的後綴
	 * @param filename
	 * @param isCantainDot 是否包含點(.),如果為true則包含,false則不包含 ,例如 .jpg 和 jpg
	 * @return
	 */
	public static String getSuffix(String filename,boolean isCantainDot){
		if(TextUtils.isEmpty(filename)) return null;
		if(isCantainDot) 
			return filename.substring(filename.lastIndexOf("."), filename.length());
		return filename.substring(filename.lastIndexOf(".")+1, filename.length());
	}
	
	/**
	 * 根據Uri得到下載圖片資源的目錄
	 * @param uri 網絡圖片資源Uri
	 * @param globalConfig Bitmap全局配置
	 * @return
	 */
	/*public static String getDownloadDir(String uri,BitmapGlobalConfig globalConfig){
		
		if(null==uri || globalConfig==null) return null;
		
		String filename=getFileName(uri);
		uri=uri.replace(filename, "");
		String startSeparater=globalConfig.getSplitUriStartSeparater();
		String startDir=null;
		if(!TextUtils.isEmpty(startSeparater)){
				String[] data=uri.split(startSeparater);
				if(null==data || data.length<2){
					startDir="";
				}else{
					startDir=data[1];
				}
			}else{
				startDir="";
			}
	
		String root=globalConfig.getDiskCachePath();
		
		if(root.endsWith("/")){
			root=root.substring(0, root.lastIndexOf("/"));
		}
		
		if(root.endsWith("\\")){
			root=root.substring(0,root.lastIndexOf("\\"));
		}
		
		if(null!=startDir && startDir.startsWith("/")){
			startDir=startDir.substring(1);
		}
		
		if(null!=startDir && startDir.startsWith("\\")){
			startDir=startDir.substring(1);
		}
		
		File dir=new File(root+File.separator+startDir);
		if(!dir.exists()){
			dir.mkdirs();
		} 
		String dir=root+File.separator+startDir;
		
		filename=startSeparater=startDir=root=null;
		return dir;
	}*/
	
	
	/**
	 * 根據Uri得到下載圖片資源的目錄
	 * @param uri 網絡圖片資源Uri
	 * @param globalConfig Bitmap全局配置
	 * @return
	 */
	public static String getDownloadDir(String uri,BitmapGlobalConfig globalConfig){
		
		if(TextUtils.isEmpty(uri) || globalConfig==null) return null;
		
		uri=uri.substring(0,uri.lastIndexOf("."));
		
		String startSeparater=globalConfig.getSplitUriStartSeparater();
		String startDir=null;
		if(!TextUtils.isEmpty(startSeparater)){
				String[] data=uri.split(startSeparater);
				if(null==data || data.length<2){
					startDir="";
				}else{
					startDir=data[1];
				}
			}else{
				startDir="";
			}
	
		String root=globalConfig.getDiskCachePath();
		
		if(root.endsWith("/")){
			root=root.substring(0, root.lastIndexOf("/"));
		}
		
		if(root.endsWith("\\")){
			root=root.substring(0,root.lastIndexOf("\\"));
		}
		
		if(null!=startDir && startDir.startsWith("/")){
			startDir=startDir.substring(1);
		}
		
		if(null!=startDir && startDir.startsWith("\\")){
			startDir=startDir.substring(1);
		}
		String dir=root+File.separator+startDir;
		
		startSeparater=startDir=root=null;
		return dir;
	}
	
	public static File getOldResource(String uri,BitmapGlobalConfig globalConfig){
		String dir = getDownloadDir(uri, globalConfig);
		if(!TextUtils.isEmpty(dir)){
			File file=new File(dir);
			// 空指針異常, 必須 
			if(file.exists()){
			System.out.println("getOldResource,file="+file+" , dir="+dir+" , 是否存在file="+file.exists());
			 File[] files = file.listFiles();
			 System.out.println("getOldResource,files="+files);
			 System.out.println("getOldResource,files.length="+files.length);
			for(File f: files){
				if(f.isFile() ){
					return f;
				}
			}
			}
		}
		return null;
	}
	
	
	public static Bitmap getOldBitmap(String uri,
			BitmapDisplayConfig config, byte[] data){
		
		if (TextUtils.isEmpty(uri) || data == null || data.length<=0 || null==config || null==config.getBitmapGlobalConfig()) {
			return null;
		}
		
		Bitmap bitmap = null;
	
				if (null!=config&&config.isShowOriginal()) {
					bitmap = BitmapDecoder.decodeByteArray(data);
				} else {
					bitmap = BitmapDecoder.decodeSampledBitmapFromByteArray(
							data, config.getBitmapMaxWidth(),
							config.getBitmapMaxHeight(), config.getBitmapConfig());
				}
			
		 
			if (bitmap == null) {
				return null;
			}
			
			//暫時先不加到內存中
//			uri=BitmapCommonUtils.getPathByCompressFormat(config.getBitmapGlobalConfig(),uri);
//			String key = uri + config.toString();
			//添加到內存緩存中
//			if (config.getBitmapGlobalConfig().isMemoryCacheEnabled() && mMemoryCache != null) {
//				mMemoryCache.put(key, new SoftReference<Bitmap>(bitmap),
//						bitmapMeta.expiryTimestamp);
//			}
			return bitmap;
	}
	
}
