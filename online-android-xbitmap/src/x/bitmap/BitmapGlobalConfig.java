/*
 *Copyright (c) 2013. ccx (ccxandroid@gmail.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package x.bitmap;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import x.bitmap.cache.BitmapCache;
import x.bitmap.cache.BitmapCacheListener;
import x.bitmap.download.Downloader;
import x.bitmap.download.SimpleDownloader;
import x.bitmap.task.CompatibleAsyncTask;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;


/**
 * Bitmap全局配置
 * @author Hero
 * @since 2013-10-21
 */
public class BitmapGlobalConfig {

    private String diskCachePath;
	private static final String TAG = "BitmapGlobalConfig";
    public final static int MIN_MEMORY_CACHE_SIZE = 1024 * 1024 * 2; // 2M
    private int memoryCacheSize = 1024 * 1024 * 4; // 6MB
    public final static int MIN_DISK_CACHE_SIZE = 1024 * 1024 * 10; // 10M

    private int diskCacheSize = 1024 * 1024 * 50;  // 50M

    private boolean memoryCacheEnabled = true;
    private boolean diskCacheEnabled = true;

    private Downloader downloader;
    private BitmapCache bitmapCache;

    private int threadPoolSize = 5;
    private boolean _dirty_params_bitmapLoadExecutor = true;
    private ExecutorService bitmapLoadExecutor;

    private long defaultCacheExpiry = 1000L * 60 * 60 * 24 * 30; //緩存默認30天過期

    
    private boolean generateSingleFile=true;
    private int compressQuality=100;
    private Bitmap.CompressFormat compressFormat=Bitmap.CompressFormat.JPEG;
    private boolean loadLocalResources=false;
    private String compositePathSeparater="@";
    private boolean compositePath=false;
    private String splitUriStartSeparater="//";
    private boolean isDynamicFormat=true;
    
    private BitmapCacheListener bitmapCacheListener;

    private Context mContext;

    /**
     * 創建BitmapGlobalConfig實例
     * @param context 上下文
     * @param diskCachePath 設備本地緩存路徑
     */
    public BitmapGlobalConfig(Context context, String diskCachePath) {
        this.mContext = context;
        this.diskCachePath = diskCachePath;
        initBitmapCache();
    }

    /**
     * 初始化緩存
     */
    private void initBitmapCache() {
        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_INIT_MEMORY_CACHE);
        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_INIT_DISK_CACHE);
    }

    /**
     * 獲取設備本地緩存路徑
     * @return
     */
    public String getDiskCachePath() {
        if (TextUtils.isEmpty(diskCachePath)) {
            diskCachePath = BitmapCommonUtils.getDiskCacheDir(mContext, "XBitmapCache");
        }
        return diskCachePath;
    }
    
    /**
     * 設置設備本地緩存路徑
     * @param diskCachePath
     */
    public void setDiskCachePath(String diskCachePath) {
        this.diskCachePath= BitmapCommonUtils.getDiskCacheDir(mContext, diskCachePath);
    }

    /**
     * 獲取資源下載器
     * @return
     */
    public Downloader getDownloader() {
        if (downloader == null) {
            downloader = new SimpleDownloader();
            downloader.setDefaultExpiry(getDefaultCacheExpiry());
        }
        return downloader;
    }

    /**
     * 設置資源下載器
     * @param downloader
     */
    public void setDownloader(Downloader downloader) {
        this.downloader = downloader;
        this.downloader.setDefaultExpiry(getDefaultCacheExpiry());
    }

    /**
     * 獲取默認的緩存可用週期
     * @return
     */
    public long getDefaultCacheExpiry() {
        return defaultCacheExpiry;
    }

    /**
     * 設置默認的緩存可用週期
     * @param defaultCacheExpiry 可用週期，單位為毫秒
     */
    public void setDefaultCacheExpiry(long defaultCacheExpiry) {
        this.defaultCacheExpiry = defaultCacheExpiry;
        this.getDownloader().setDefaultExpiry(defaultCacheExpiry);
    }

    /**
     * 獲取BitmapCache實例
     * @return
     */
    public BitmapCache getBitmapCache() {
        if (bitmapCache == null) {
            bitmapCache = new BitmapCache(this);
        }
        return bitmapCache;
    }

    /**
     * 獲取內存緩存大小，單位為byte
     * @return
     */
    public int getMemoryCacheSize() {
        return memoryCacheSize;
    }

    /**
     * 設置內存緩存大小
     * @param memoryCacheSize 內存緩存大小，不能小於最小內存空間1024*1024*2=2MB，單位為byte
     */
    public void setMemoryCacheSize(int memoryCacheSize) {
        if (memoryCacheSize >= MIN_MEMORY_CACHE_SIZE) {
            this.memoryCacheSize = memoryCacheSize;
            if (bitmapCache != null) {
                bitmapCache.setMemoryCacheSize(this.memoryCacheSize);
            }
        } else {
            this.setMemCacheSizePercent(0.3f);//Set the default memory cache size
        }
    }

    /**
     * 設置應用緩存在APK總內存的百分比
     * @param 應用緩存在APK總內存的百分比，
     * 優先級高於memoryCacheSize，值的範圍是 0.05 到 0.8 之间
     */
    public void setMemCacheSizePercent(float percent) {
        if (percent < 0.05f || percent > 0.8f) {
            throw new IllegalArgumentException("percent must be between 0.05 and 0.8 (inclusive)");
        }
        this.memoryCacheSize = Math.round(percent * getMemoryClass() * 1024 * 1024);
        if (bitmapCache != null) {
            bitmapCache.setMemoryCacheSize(this.memoryCacheSize);
        }
    }

    /**
     * 獲取設備本地緩存大小，單位為byte
     * @return
     */
    public int getDiskCacheSize() {
        return diskCacheSize;
    }

    /**
     * 設置設備本地緩存大小
     * @param diskCacheSize 設備本地緩存大小，不能小於最小內存空間1024*1024*10=10MB，單位為byte
     */
    public  void setDiskCacheSize(int diskCacheSize) {
        if (diskCacheSize >= MIN_DISK_CACHE_SIZE) {
            this.diskCacheSize = diskCacheSize;
            /*if (bitmapCache != null) {
                bitmapCache.setDiskCacheSize(this.diskCacheSize);
            }*/
        }
    }

    /**
     * 獲取線程池中線程數量
     * @return
     */
    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    /**
     * 設置線程池中執行的線程數量，默認為5個
     * @param threadPoolSize 線程池中執行的線程數量
     */
    public void setThreadPoolSize(int threadPoolSize) {
        if (threadPoolSize > 0 && threadPoolSize != this.threadPoolSize) {
            _dirty_params_bitmapLoadExecutor = true;
            this.threadPoolSize = threadPoolSize;
        }
    }

    /**
     * 獲取加載圖片資源的線程執行服務（運行器）
     * @return
     */
    public ExecutorService getBitmapLoadExecutor() {
        if (_dirty_params_bitmapLoadExecutor || bitmapLoadExecutor == null) {
            bitmapLoadExecutor = Executors.newFixedThreadPool(getThreadPoolSize(), new ThreadFactory() {
                @Override
                public Thread newThread(Runnable r) {
                    Thread t = new Thread(r);
                    t.setPriority(Thread.NORM_PRIORITY - 1);
                    return t;
                }
            });
            _dirty_params_bitmapLoadExecutor = false;
        }
        return bitmapLoadExecutor;
    }

    /**
     * 獲取內存緩存是否可用
     * @return
     */
    public boolean isMemoryCacheEnabled() {
        return memoryCacheEnabled;
    }

    /**
     * 設置內存緩存是否可用
     * @param memoryCacheEnabled
     */
    public void setMemoryCacheEnabled(boolean memoryCacheEnabled) {
        this.memoryCacheEnabled = memoryCacheEnabled;
    }

    /**
     * 獲取設備本地緩存是否可用
     * @return
     */
    public boolean isDiskCacheEnabled() {
        return diskCacheEnabled;
    }

    /**
     * 設置設備本地緩存是否可用
     * @param diskCacheEnabled
     */
    public void setDiskCacheEnabled(boolean diskCacheEnabled) {
        this.diskCacheEnabled = diskCacheEnabled;
    }


    /**
     * 獲取BitmapCache實例的監聽器
     * @return
     */
    public BitmapCacheListener getBitmapCacheListener() {
        return bitmapCacheListener;
    }

    /**
     * 設置BitmapCache實例的監聽器
     * @param bitmapCacheListener
     */
    public void setBitmapCacheListener(BitmapCacheListener bitmapCacheListener) {
        this.bitmapCacheListener = bitmapCacheListener;
    }

    /**
     * 獲取設備內存類
     * @return
     */
    private int getMemoryClass() {
        return ((ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
    }

    ////////////////////////////////// bitmap cache management task ///////////////////////////////////////
   /**
    * BitmapCache實例管理任務
    * @author Hero
    *
    */
    private class BitmapCacheManagementTask extends CompatibleAsyncTask<Object, Void, Object[]> {
        public static final int MESSAGE_INIT_MEMORY_CACHE = 0;
        public static final int MESSAGE_INIT_DISK_CACHE = 1;
        public static final int MESSAGE_FLUSH = 2;
        public static final int MESSAGE_CLOSE = 3;
        public static final int MESSAGE_CLEAR = 4;
        public static final int MESSAGE_CLEAR_MEMORY = 5;
        public static final int MESSAGE_CLEAR_DISK = 6;
        public static final int MESSAGE_CLEAR_BY_KEY = 7;
        public static final int MESSAGE_CLEAR_MEMORY_BY_KEY = 8;
        public static final int MESSAGE_CLEAR_DISK_BY_KEY = 9;

        @Override
        protected Object[] doInBackground(Object... params) {
            BitmapCache cache = getBitmapCache();
            if (cache != null) {
                try {
                    switch ((Integer) params[0]) {
                        case MESSAGE_INIT_MEMORY_CACHE:
                            cache.initMemoryCache();
                            break;
                        case MESSAGE_INIT_DISK_CACHE:
                            cache.initDiskCache();
                            break;
                        case MESSAGE_FLUSH:
                            cache.clearMemoryCache();
//                            cache.flush();
                            break;
                        case MESSAGE_CLOSE:
                            cache.clearMemoryCache();
//                            cache.close();
                            break;
                        case MESSAGE_CLEAR:
                            cache.clearCache();
                            break;
                        case MESSAGE_CLEAR_MEMORY:
                            cache.clearMemoryCache();
                            break;
                        case MESSAGE_CLEAR_DISK:
                            cache.clearDiskCache();
                            break;
                        case MESSAGE_CLEAR_BY_KEY:
                            cache.clearCache(String.valueOf(params[1]), (BitmapDisplayConfig) params[2]);
                            break;
                        case MESSAGE_CLEAR_MEMORY_BY_KEY:
                            cache.clearMemoryCache(String.valueOf(params[1]), (BitmapDisplayConfig) params[2]);
                            break;
                        case MESSAGE_CLEAR_DISK_BY_KEY:
                            cache.clearDiskCache(String.valueOf(params[1]));
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return params;
        }

        // add return Object[]
        @Override
        protected void onPostExecute(Object[] params) {
            if (bitmapCacheListener == null || params == null || params.length < 1) {
            	// add return null
                return;
            }
            try {
                switch ((Integer) params[0]) {
                    case MESSAGE_INIT_MEMORY_CACHE:
                        bitmapCacheListener.onInitMemoryCacheFinished();
                        break;
                    case MESSAGE_INIT_DISK_CACHE:
                        bitmapCacheListener.onInitDiskFinished();
                        break;
                    case MESSAGE_FLUSH:
                        bitmapCacheListener.onFlushCacheFinished();
                        break;
                    case MESSAGE_CLOSE:
                        bitmapCacheListener.onCloseCacheFinished();
                        break;
                    case MESSAGE_CLEAR:
                        bitmapCacheListener.onClearCacheFinished();
                        break;
                    case MESSAGE_CLEAR_MEMORY:
                        bitmapCacheListener.onClearMemoryCacheFinished();
                        break;
                    case MESSAGE_CLEAR_DISK:
                        bitmapCacheListener.onClearDiskCacheFinished();
                        break;
                    case MESSAGE_CLEAR_BY_KEY:
                        bitmapCacheListener.onClearCacheFinished(String.valueOf(params[1]), (BitmapDisplayConfig) params[2]);
                        break;
                    case MESSAGE_CLEAR_MEMORY_BY_KEY:
                        bitmapCacheListener.onClearMemoryCacheFinished(String.valueOf(params[1]), (BitmapDisplayConfig) params[2]);
                        break;
                    case MESSAGE_CLEAR_DISK_BY_KEY:
                        bitmapCacheListener.onClearDiskCacheFinished(String.valueOf(params[1]));
                        break;
                    default:
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 釋放緩存（包含內存緩存和設備本地緩存）
     */
    public void clearCache() {
        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_CLEAR);
    }

    /**
     * 釋放內存緩存
     */
    public void clearMemoryCache() {
        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_CLEAR_MEMORY);
    }

    /**
     * 釋放設備本地緩存
     */
    public void clearDiskCache() {
        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_CLEAR_DISK);
    }

    /**
     * 根據Uri釋放緩存，包含內存緩存和設備本地緩存
     * @param uri 圖片的Uri
     * @param config 顯示圖片的配置
     */
    public void clearCache(String uri, BitmapDisplayConfig config) {
        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_CLEAR_BY_KEY, uri, config);
    }

    /**
     * 根據Uri釋放內存緩存
     * @param uri 圖片的Uri
     * @param config 顯示圖片的配置
     */
    public void clearMemoryCache(String uri, BitmapDisplayConfig config) {
        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_CLEAR_MEMORY_BY_KEY, uri, config);
    }

    /**
     * 根據Uri釋放設備本地緩存
     * @param uri 圖片的Uri
     */
    public void clearDiskCache(String uri) {
        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_CLEAR_DISK_BY_KEY, uri);
    }

//    /**
//     * 刷新內存緩存
//     */
//    public void flushCache() {
//        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_FLUSH);
//    }

    /**
     * 關閉緩存
     */
    public void closeCache() {
        new BitmapCacheManagementTask().execute(BitmapCacheManagementTask.MESSAGE_CLOSE);
    }

    /**
     * 獲取是否把單獨圖片資源文件生成緩存在設備本地
     * @return
     */
	public boolean getGenerateSingleFile() {
		return generateSingleFile;
	}

	/**
	 * 設置是否把單獨圖片資源文件生成緩存在設備本地
	 * @param generateSingleFile 為true生成緩存到設備本地，為false不生成緩存到設備本地
	 */
	public void setGenerateSingleFile(boolean generateSingleFile) {
		this.generateSingleFile = generateSingleFile;
	}

	/**
	 * 獲取設備本地緩存的圖片資源文件存儲的壓縮質量，0表示壓縮率最大，也就是圖片質量損失最大，100表示無壓縮，原圖片緩存，默認值為100。
	 * @return
	 */
	public int getCompressQuality() {
		return compressQuality;
	}

	/**
	 * 設置設備本地緩存的圖片資源文件存儲的壓縮質量
	 * @param quality 值的範圍為0-100。 0表示壓縮率最大，也就是圖片質量損失最大，100表示無壓縮，原圖片緩存，默認值為100。
	 */
	public void setCompressQuality(int compressQuality) {
		this.compressQuality = compressQuality;
	}

	/**
	 * 獲取設備本地緩存圖片資源文件的存儲壓縮格式
	 * @return
	 */
	public Bitmap.CompressFormat getCompressFormat() {
		return compressFormat;
	}

	/**
	 * 設置設備本地緩存圖片資源文件的存儲壓縮格式
	 * @param compressFormat 值為Bitmap.CompressFormat.PNG 和 Bitmap.CompressFormat.JPEG ,
     * 默認為Bitmap.CompressFormat.JPEG 
	 */
	public void setCompressFormat(Bitmap.CompressFormat compressFormat) {
		this.compressFormat = compressFormat;
	}

	/**
	 * 獲取是否加載設備本地資源
	 * @return
	 */
	public boolean getLoadLocalResources() {
		return loadLocalResources;
	}

	/**
	 * 設置是否加載設備本地資源
	 * @param loadLocalResources 為true是設備本地資源，為false是非設備本地資源，例如從服務器獲取
	 */
	public void setLoadLocalResources(boolean loadLocalResources) {
		this.loadLocalResources = loadLocalResources;
	}

	public String getCompositePathSeparater() {
		return compositePathSeparater;
	}

	public void setCompositePathSeparater(String compositePathSeparater) {
		if(!TextUtils.isEmpty(compositePathSeparater)){
		this.compositePathSeparater = compositePathSeparater;
		}
	}

	public boolean isCompositePath() {
		return compositePath;
	}

	public void setCompositePath(boolean compositePath) {
		this.compositePath = compositePath;
	}

	public String getSplitUriStartSeparater() {
		return splitUriStartSeparater;
	}

	public void setSplitUriStartSeparater(String splitUriStartSeparater) {
		this.splitUriStartSeparater = splitUriStartSeparater;
	}

	public boolean isDynamicFormat() {
		return isDynamicFormat;
	}

	public void setDynamicFormat(boolean isDynamicFormat) {
		this.isDynamicFormat = isDynamicFormat;
	}

}