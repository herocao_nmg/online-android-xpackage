package x.util.test;

import java.io.File;
import java.io.IOException;

import x.util.FileUtils;

import junit.framework.Assert;

import android.os.Environment;
import android.test.AndroidTestCase;

public class TestFileUtils extends AndroidTestCase {

//	File dstFile = new File(Environment.getExternalStorageDirectory(),"video_copy7.mp3");
//	private final String srcName="video.mp3";
	private final String srcName="video.mp3";
	
/*	public void testCopy1(){
		try {
			Assert.assertEquals(true, FileUtils.copy(new File(Environment.getExternalStorageDirectory(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_01_te.mp3")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testCopy2(){
		try {
			Assert.assertEquals(true, FileUtils.copy(new File(Environment.getExternalStorageDirectory(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_02.mp3").getPath()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
public void testCopy3(){
		try {
			Assert.assertEquals(true, FileUtils.copy(new File(Environment.getExternalStorageDirectory(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_03.mp3").getPath()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

public void testCopy4(){
	try {
		Assert.assertEquals(true, FileUtils.copy(new File(Environment.getExternalStorageDirectory(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_04.mp3")));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}*/

public void testCopy4_1(){
	try {
//		Assert.assertEquals(true, FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_04_1.mp3"),true));
		boolean result =FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_04_1.mp3"),true);
		System.out.println("TestFileUtils,testCopy4_1,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testCopy4_1,error="+e.getMessage());
	}
}

public void testCopy4_2(){
	try {
//		Assert.assertEquals(true, FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_04_2.mp3"),false));
		boolean result= FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_04_2.mp3"),false);
		System.out.println("TestFileUtils,testCopy4_2,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public void testCopy3_1(){
	try {
//		Assert.assertEquals(true, FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_03_1.mp3").getPath(),true));
		boolean result= FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_03_1.mp3").getPath(),true);
	
		System.out.println("TestFileUtils,testCopy3_1,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testCopy3_1,error="+e.getMessage());
	}
}

public void testCopy3_2(){
	try {
//		Assert.assertEquals(true, FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_03_2.mp3").getPath(),false));
		boolean result=FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_copy_03_2.mp3").getPath(),false);
		System.out.println("TestFileUtils,testCopy3_2,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testCopy3_2,error="+e.getMessage());
	}
}


public void testCopy2_1(){
	try {
//		Assert.assertEquals(true, FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_02_1.mp3").getPath(),true));
		boolean result= FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_02_1.mp3").getPath(),true);
		
		System.out.println("TestFileUtils,testCopy2_1,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testCopy2_1,error="+e.getMessage());
	}
}

public void testCopy2_2(){
	try {
//		Assert.assertEquals(true, FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_02_2.mp3").getPath(),false));
		boolean result=FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_02_2.mp3").getPath(),false);
	
		System.out.println("TestFileUtils,testCopy2_2,result="+result);
	
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		System.out.println("TestFileUtils,testCopy2_2,error="+e.getMessage());
	}
}

public void testCopy1_1(){
	try {
//		Assert.assertEquals(true, FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_01_1.mp3"),true));
		boolean result=FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_01_1.mp3"),true);
	
		System.out.println("TestFileUtils,testCopy1_1,result="+result);
	
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		System.out.println("TestFileUtils,testCopy1_1,error="+e.getMessage());
	}
}
	
	public void testCopy1_2(){
		try {
//			Assert.assertEquals(true, FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_01_2.mp3"),false));
			boolean result= FileUtils.copy(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_copy_01_2.mp3"),false);
		
			System.out.println("TestFileUtils,testCopy1_2,result="+result);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			System.out.println("TestFileUtils,testCopy1_2,error="+e.getMessage());
		}
}
	
	
	
	
}
