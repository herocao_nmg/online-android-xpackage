package x.util.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import x.util.FileUtils;

import junit.framework.Assert;

import android.os.Environment;
import android.test.AndroidTestCase;

public class TestFileUtils3 extends AndroidTestCase {

//	File dstFile = new File(Environment.getExternalStorageDirectory(),"video_Save7.mp3");
//	private final String srcName="video.mp3";
	private final String srcName="video.mp3";
	
	private byte[] data;
	private InputStream is;
	
	@Override
	protected void setUp() throws Exception {
		try {
		System.out.println("TestFileUtils3,setUp,...");
		File file=null;
		data=FileUtils.read(file =new File(getContext().getCacheDir(),srcName));
		System.out.println("TestFileUtils3,setUp,data="+data+" , getCacheDir()="+getContext().getCacheDir());
		
		is=new FileInputStream(file);
		}catch(IOException e){
//			e.printStackTrace();
			System.out.println("TestFileUtils3,setUp,error="+e.getMessage());
		}
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}
	
/*	public void testSave1(){
		System.out.println("TestFileUtils3,testSave1,...");
		try {
//			FileUtils.save(data, file);
			Assert.assertEquals(true, FileUtils.save(data,new File(Environment.getExternalStorageDirectory(),"video_Save_01.mp3")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testSave2(){
		try {
//			FileUtils.save(data, filename);
			Assert.assertEquals(true, FileUtils.save(data, new File(Environment.getExternalStorageDirectory(),"video_Save_02.mp3").getPath()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
public void testSave3(){
		try {
//			FileUtils.save(is, filename)
			Assert.assertEquals(true, FileUtils.save(is, new File(Environment.getExternalStorageDirectory(),"video_Save_03.mp3").getPath()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

public void testSave4(){
	try {
//		FileUtils.save(is, file)
		Assert.assertEquals(true, FileUtils.save(is, new File(Environment.getExternalStorageDirectory(),"video_Save_04.mp3")));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public void testSave5(){
	try {
//		FileUtils.save(is, file)
		File file=null;
		Assert.assertEquals(true, FileUtils.save(is, file=new File(getContext().getCacheDir(),"video_Save_06.mp3")));
		
		System.out.println("TestFileUtils3,testSave5,file="+file.getPath());
		System.out.println("TestFileUtils3,testSave5,video_Save_06.mp3 ,是否存在="+new File(getContext().getCacheDir(),"video_Save_06.mp3").exists());
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}*/



public void testSave4_1(){
	try {
//		Assert.assertEquals(true, FileUtils.Save(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Save_04_1.mp3"),true));
		boolean result =FileUtils.save(getContext(),is, new File(Environment.getExternalStorageDirectory(),"video_Save_04_1.mp3"),true,false);
		System.out.println("TestFileUtils,testSave4_1,result="+result+" , 是否關閉is="+is);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testSave4_1,error="+e.getMessage());
	}
}

public void testSave4_2(){
	try {
//		Assert.assertEquals(true, FileUtils.Save(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Save_04_2.mp3"),false));
		boolean result= FileUtils.save(getContext(),is, new File(Environment.getExternalStorageDirectory(),"video_Save_04_2.mp3"),false,false);
		System.out.println("TestFileUtils,testSave4_2,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public void testSave3_1(){
	try {
//		Assert.assertEquals(true, FileUtils.Save(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Save_03_1.mp3").getPath(),true));
		boolean result= FileUtils.save(getContext(),is, new File(Environment.getExternalStorageDirectory(),"video_Save_03_1.mp3").getPath(),true,false);
	
		System.out.println("TestFileUtils,testSave3_1,result="+result+" , 是否關閉is="+is);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testSave3_1,error="+e.getMessage());
	}
}

public void testSave3_2(){
	try {
//		Assert.assertEquals(true, FileUtils.Save(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Save_03_2.mp3").getPath(),false));
		boolean result=FileUtils.save(getContext(),is, new File(Environment.getExternalStorageDirectory(),"video_Save_03_2.mp3").getPath(),false,true);
		System.out.println("TestFileUtils,testSave3_2,result="+result+" , 為true ,是否關閉is="+is);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testSave3_2,error="+e.getMessage());
	}
}


public void testSave2_1(){
	try {
//		Assert.assertEquals(true, FileUtils.Save(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Save_02_1.mp3").getPath(),true));
		boolean result= FileUtils.save(getContext(),data, new File(Environment.getExternalStorageDirectory(),"video_Save_02_1.mp3").getPath(),true);
		
		System.out.println("TestFileUtils,testSave2_1,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testSave2_1,error="+e.getMessage());
	}
}

public void testSave2_2(){
	try {
//		Assert.assertEquals(true, FileUtils.Save(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Save_02_2.mp3").getPath(),false));
		boolean result=FileUtils.save(getContext(),data, new File(Environment.getExternalStorageDirectory(),"video_Save_02_2.mp3").getPath(),false);
	
		System.out.println("TestFileUtils,testSave2_2,result="+result);
	
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		System.out.println("TestFileUtils,testSave2_2,error="+e.getMessage());
	}
}

public void testSave1_1(){
	try {
//		Assert.assertEquals(true, FileUtils.Save(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Save_01_1.mp3"),true));
		boolean result=FileUtils.save(getContext(),data, new File(Environment.getExternalStorageDirectory(),"video_Save_01_1.mp3"),true);
	
		System.out.println("TestFileUtils,testSave1_1,result="+result);
	
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		System.out.println("TestFileUtils,testSave1_1,error="+e.getMessage());
	}
}
	
	public void testSave1_2(){
		try {
//			Assert.assertEquals(true, FileUtils.Save(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Save_01_2.mp3"),false));
			boolean result= FileUtils.save(getContext(),data, new File(Environment.getExternalStorageDirectory(),"video_Save_01_2.mp3"),false);
		
			System.out.println("TestFileUtils,testSave1_2,result="+result);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			System.out.println("TestFileUtils,testSave1_2,error="+e.getMessage());
		}
}
	
}
