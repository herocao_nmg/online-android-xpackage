package x.util.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import x.util.FileUtils;

import junit.framework.Assert;

import android.os.Environment;
import android.test.AndroidTestCase;

public class TestFileUtils4 extends AndroidTestCase {

	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		// TODO Auto-generated method stub
		super.tearDown();
	}
	
	/*public void testDelete(){
		
		//  
		
		File file=new File(Environment.getExternalStorageDirectory().getPath()+"/taobao/cache/Dd");
		
		System.out.println("TestFileUtils4,testDelete,file="+file.getPath());
		FileUtils.delete(file,true);
		
		System.out.println("TestFileUtils4,testDelete,是否存在,file="+file.exists());
		
	}*/

	
	/*public void testGetSuffix(){
		String suffix = FileUtils.getSuffix("/test/11/22/33/ss.json");
		System.out.println("TestFileUtils4,suffix="+suffix);
	}
	
	
	public void testRead() throws IOException{
		
		byte[] data = FileUtils.read(Environment.getExternalStorageDirectory().getPath()+"/taobao/cache/ZipUtil.java");
		
		System.out.println("TestFileUtils4,testRead---------Begin------------\r\n\r\n");
		System.out.println("TestFileUtils4,testRead,data="+new String(data));
		System.out.println("TestFileUtils4,testRead--------End-------------\r\n\r\n");
		
	}
	
	public void testGetFileName(){
		String file="/11/22/33/test.java";
		String fileName = FileUtils.getFileName(file);
		
		System.out.println("TestFileUtils4,testGetFileName,fileName="+fileName);
	}*/
	
	public void testIsExternalStoragePath(){
		//isExternalStoragePath
		
		boolean isExternalStoragePath = FileUtils.isExternalStoragePath(Environment.getExternalStorageDirectory().getPath());
		
		
		System.out.println("TestFileUtils4,testIsExternalStoragePath,isExternalStoragePath="+isExternalStoragePath);
		
		boolean isExternalStoragePath2 = FileUtils.isExternalStoragePath(getContext().getCacheDir().getPath());
		
		
		System.out.println("TestFileUtils4,testIsExternalStoragePath,isExternalStoragePath2="+isExternalStoragePath2);
		
		
	}
	
	public void testIsFulledExternalStorage(){
		
		boolean fulledExternalStorage = FileUtils.isFulledExternalStorage();
		
		System.out.println("TestFileUtils4,testIsFulledExternalStorage,getExternalStorageAvailableSize="+FileUtils.getExternalStorageAvailableSize()+" , min="+FileUtils.minExternalStorageAvailableSize);
		
		System.out.println("TestFileUtils4,testIsFulledExternalStorage,getExternalStorageAvailableSize="+FileUtils.format(FileUtils.getExternalStorageAvailableSize()) +" , min="+FileUtils.format(FileUtils.minExternalStorageAvailableSize));
		System.out.println("TestFileUtils4,testIsFulledExternalStorage,fulledExternalStorage="+fulledExternalStorage);
		
	}
	
	public void testIsMountedExternalStorage(){
		
		boolean mountedExternalStorage = FileUtils.isMountedExternalStorage();
		
		System.out.println("TestFileUtils4,testIsMountedExternalStorage,mountedExternalStorage="+mountedExternalStorage);
	}
	
	public void testMakeDirectory() throws IOException{
		
		boolean makeDirectory = FileUtils.makeDirectory(Environment.getExternalStorageDirectory()+"/md23/md22/md22/test_make_direc02",true);
		
		System.out.println("TestFileUtils4,testMakeDirectory,makeDirectory="+makeDirectory);
		
		
//		boolean makeDirectory2 = FileUtils.makeDirectory(Environment.getExternalStorageDirectory()+"/md1/md2/md3/test_make_direc01");
//		
//		System.out.println("TestFileUtils4,testMakeDirectory,makeDirectory="+makeDirectory);
		
		boolean createFileAtCacheDir = FileUtils.createFileAtCacheDir(getContext(), "test1122.txt");
		
		System.out.println("TestFileUtils4,testMakeDirectory,createFileAtCacheDir="+createFileAtCacheDir);
		
		boolean createFileAtExternalStorage = FileUtils.createFileAtExternalStorage("test88.java");
		
		System.out.println("TestFileUtils4,testMakeDirectory,createFileAtExternalStorage="+createFileAtExternalStorage);
		
		boolean createNewFile = FileUtils.createNewFile(new File(Environment.getExternalStorageDirectory()+"/mf1/mf2/mf3","mf.txt"), true);

		
		System.out.println("TestFileUtils4,testMakeDirectory,createNewFile="+createNewFile);
		
	}
	
	
}
