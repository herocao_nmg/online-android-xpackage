package x.util.test;

import java.io.File;
import java.io.IOException;

import x.util.FileUtils;

import junit.framework.Assert;

import android.os.Environment;
import android.test.AndroidTestCase;

public class TestFileUtils2 extends AndroidTestCase {

//	File dstFile = new File(Environment.getExternalStorageDirectory(),"video_Move7.mp3");
//	private final String srcName="video.mp3";
	private final String srcName1="video_copy_01.mp3";
	private final String srcName2="video_copy_02.mp3";
	private final String srcName3="video_copy_03.mp3";
	private final String srcName4="video_copy_04.mp3";
	private final String srcName5="ww_video.h264";
	
	public void testMove1(){
		try {
			Assert.assertEquals(true, FileUtils.move(new File(Environment.getExternalStorageDirectory(),srcName1), new File(Environment.getExternalStorageDirectory(),"video_Move_01.mp3")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testMove2(){
		try {
			Assert.assertEquals(true, FileUtils.move(new File(Environment.getExternalStorageDirectory(),srcName2), new File(Environment.getExternalStorageDirectory(),"video_Move_02.mp3").getPath()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
public void testMove3(){
		try {
			Assert.assertEquals(true, FileUtils.move(new File(Environment.getExternalStorageDirectory(),srcName3).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_03.mp3").getPath()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

public void testMove4(){
	try {
		Assert.assertEquals(true, FileUtils.move(new File(Environment.getExternalStorageDirectory(),srcName4).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_04.mp3")));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public void testMove5(){
	try {
		
		System.out.println("TestFileUtils2,testMove5,getContext().getCacheDir()="+getContext().getCacheDir());
		System.out.println("TestFileUtils2,testMove5,Environment.getExternalStorageDirectory()="+Environment.getExternalStorageDirectory());
		Assert.assertEquals(true, FileUtils.move(new File(Environment.getExternalStorageDirectory(),srcName5).getPath(), new File(getContext().getCacheDir(),"ww_video_move_5.h264")));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		System.out.println("TestFileUtils2,testMove5,error="+e.getMessage());
	}
}

/*public void testMove4_1(){
	try {
//		Assert.assertEquals(true, FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_04_1.mp3"),true));
		boolean result =FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_04_1.mp3"),true);
		System.out.println("TestFileUtils,testMove4_1,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testMove4_1,error="+e.getMessage());
	}
}

public void testMove4_2(){
	try {
//		Assert.assertEquals(true, FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_04_2.mp3"),false));
		boolean result= FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_04_2.mp3"),false);
		System.out.println("TestFileUtils,testMove4_2,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public void testMove3_1(){
	try {
//		Assert.assertEquals(true, FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_03_1.mp3").getPath(),true));
		boolean result= FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_03_1.mp3").getPath(),true);
	
		System.out.println("TestFileUtils,testMove3_1,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testMove3_1,error="+e.getMessage());
	}
}

public void testMove3_2(){
	try {
//		Assert.assertEquals(true, FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_03_2.mp3").getPath(),false));
		boolean result=FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName).getPath(), new File(Environment.getExternalStorageDirectory(),"video_Move_03_2.mp3").getPath(),false);
		System.out.println("TestFileUtils,testMove3_2,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testMove3_2,error="+e.getMessage());
	}
}


public void testMove2_1(){
	try {
//		Assert.assertEquals(true, FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Move_02_1.mp3").getPath(),true));
		boolean result= FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Move_02_1.mp3").getPath(),true);
		
		System.out.println("TestFileUtils,testMove2_1,result="+result);
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		
		System.out.println("TestFileUtils,testMove2_1,error="+e.getMessage());
	}
}

public void testMove2_2(){
	try {
//		Assert.assertEquals(true, FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Move_02_2.mp3").getPath(),false));
		boolean result=FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Move_02_2.mp3").getPath(),false);
	
		System.out.println("TestFileUtils,testMove2_2,result="+result);
	
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		System.out.println("TestFileUtils,testMove2_2,error="+e.getMessage());
	}
}

public void testMove1_1(){
	try {
//		Assert.assertEquals(true, FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Move_01_1.mp3"),true));
		boolean result=FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Move_01_1.mp3"),true);
	
		System.out.println("TestFileUtils,testMove1_1,result="+result);
	
	} catch (IOException e) {
		// TODO Auto-generated catch block
//		e.printStackTrace();
		System.out.println("TestFileUtils,testMove1_1,error="+e.getMessage());
	}
}
	
	public void testMove1_2(){
		try {
//			Assert.assertEquals(true, FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Move_01_2.mp3"),false));
			boolean result= FileUtils.Move(getContext(),new File(getContext().getCacheDir(),srcName), new File(Environment.getExternalStorageDirectory(),"video_Move_01_2.mp3"),false);
		
			System.out.println("TestFileUtils,testMove1_2,result="+result);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			System.out.println("TestFileUtils,testMove1_2,error="+e.getMessage());
		}
}*/
	
	
	
	
}
