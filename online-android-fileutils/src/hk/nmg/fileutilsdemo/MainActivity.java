package hk.nmg.fileutilsdemo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import x.util.FileUtils;
import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		long rootDirSize = FileUtils.getUsableSpace(Environment.getRootDirectory());
		long externalStorageSize = FileUtils.getUsableSpace(Environment.getExternalStorageDirectory());
		long cacheDirSize = FileUtils.getUsableSpace(getCacheDir());
//		long externalCacheDirSize = FileUtils.getUsableSpace(getExternalCacheDir());
		long filedirSize = FileUtils.getUsableSpace(getFilesDir());
		long dataDirectorySize = FileUtils.getUsableSpace(Environment.getDataDirectory());
		
//		this.getCacheDir();
		
		//  getRootDirectory() ---> /system
		System.out.println("MainActivity,getRootDirectory()="+Environment.getRootDirectory().getAbsolutePath());
		// getExternalStorageDirectory() ---> /storage/sdcard0
		System.out.println("MainActivity,getExternalStorageDirectory()="+Environment.getExternalStorageDirectory().getAbsolutePath());
		
//		this.getExternalCacheDir();
		
		//getCacheDir()=/data/data/hk.nmg.fileutilsdemo/cache
		System.out.println("MainActivity,getCacheDir()="+getCacheDir().getAbsolutePath());
//		System.out.println("MainActivity,getExternalCacheDir()="+getExternalCacheDir().getAbsolutePath());  // null 
		// getFilesDir()=/data/data/hk.nmg.fileutilsdemo/files
		System.out.println("MainActivity,getFilesDir()="+getFilesDir().getAbsolutePath());
		// getDataDirectory()=/data
		System.out.println("MainActivity,getDataDirectory()="+Environment.getDataDirectory().getAbsolutePath());
		
		File downloadCacheDirectory = Environment.getDownloadCacheDirectory();
		
//		 context.getExternalCacheDir().getPath() : context.getCacheDir().getPath()
		
		
		// Activity,getExternalCacheDir()=/storage/sdcard0/Android/data/hk.nmg.fileutilsdemo/cache

		System.out.println("MainActivity,getExternalCacheDir()="+this.getExternalCacheDir());
		
		String fileName2 = FileUtils.getFileName("/11/22/33/test_22.txt", true);
		String fileName3 = FileUtils.getFileName("/11/22/33/test_22.txt", false);
		
		String suffix = FileUtils.getSuffix("/11/22/33/test_22.jpg", true);
		String suffix2 = FileUtils.getSuffix("/11/22/33/test_22.jpg", false);
		
		 
		System.out.println("MainActivity,fileName2="+fileName2+" , fileName3="+fileName3+" , suffix="+suffix+" , suffix2="+suffix2);
		
		System.out.println("MainActivity,downloadCacheDirectory="+downloadCacheDirectory);
		
		//  externalStorageSize=1328881664  ,  cacheDirSize=1433739264 , filedirSize=1433739264 , dataDirectorySize=1433739264
		System.out.println("MainActivity,rootDirSize="+FileUtils.format(rootDirSize)+" , externalStorageSize="+FileUtils.format(externalStorageSize));
		System.out.println("MainActivity,cacheDirSize="+FileUtils.format(cacheDirSize)); //+" , externalCacheDirSize="+externalCacheDirSize);
		System.out.println("MainActivity,filedirSize="+FileUtils.format(filedirSize)+" , dataDirectorySize="+FileUtils.format(dataDirectorySize));
	
		double pow = Math.pow(3, 4);  // 81.0
		double pow2 = Math.pow(3, 2);  // 9.0
		
		System.out.println("MainActivity,pow="+pow+" , pow2="+pow2);
		
		// 
		
		long externalStorageAvailableSize = FileUtils.getExternalStorageAvailableSize();
		
		System.out.println("MainActivity,externalStorageAvailableSize="+FileUtils.format(externalStorageAvailableSize));
		
		String filename=getCacheDir()+"/test/11/22/33/test.txt";
		
		String name = FileUtils.getFileName(filename);
		
		System.out.println("MainActivity,name="+name);
		
		//  video.mp3
		File dstFile = new File(Environment.getExternalStorageDirectory(),"video_copy7.mp3");
		System.out.println("MainActivity,dstFile="+dstFile.getAbsolutePath());
		System.out.println("MainActivity,是否存在="+dstFile.exists());
		try {
			FileOutputStream fos=new FileOutputStream(dstFile);
			fos.close();
			fos=null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("MainActivity,error="+e.getMessage());
			e.printStackTrace();
		}
		
		try {
			FileUtils.copy(new File(Environment.getExternalStorageDirectory(),"video.mp3"), dstFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("MainActivity,是否存在="+dstFile.exists());
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
