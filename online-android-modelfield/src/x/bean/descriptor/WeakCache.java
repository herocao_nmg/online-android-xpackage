package x.bean.descriptor;


import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;

@SuppressWarnings({ "unchecked", "rawtypes" })
public final class WeakCache<K, V>
{

private final Map<K, Reference<V>> map = new WeakHashMap();

public V get(K paramK)
  {
    Reference localReference = (Reference)this.map.get(paramK);
    if (localReference == null) {
      return null;
    }
    Object localObject = localReference.get();
    if (localObject == null) {
      this.map.remove(paramK);
    }
    return (V) localObject;
  }

public void put(K paramK, V paramV)
  {
    if (paramV != null) {
      this.map.put(paramK, new WeakReference(paramV));
    }
    else
      this.map.remove(paramK);
  }

  public void clear()
  {
    this.map.clear();
  }
}
