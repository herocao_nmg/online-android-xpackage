/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package x.bean.util;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import x.bean.WeakFastHashMap;


/**
 * <p>Utility methods for populating JavaBeans properties via reflection.</p>
 *
 * <p>The implementations are provided by {@link BeanUtils}.
 * These static utility methods use the default instance.
 * More sophisticated behaviour can be provided by using a <code>BeanUtilsBean</code> instance.</p>
 *
 * @author Craig R. McClanahan
 * @author Ralph Schaer
 * @author Chris Audley
 * @author Rey Francois
 * @author Gregor Rayman
 * @version $Revision: 690380 $ $Date: 2008-08-29 21:04:38 +0100 (Fri, 29 Aug 2008) $
 * @see BeanUtils
 */

public class BeanManager {


    // ------------------------------------------------------ Private Variables


    /**
     * The debugging detail level for this component.
     * 
     * Note that this static variable will have unexpected side-effects if
     * this class is deployed in a shared classloader within a container.
     * However as it is actually completely ignored by this class due to its
     * deprecated status, it doesn't do any actual harm.
     * 
     * @deprecated BeanUtils now uses commons-logging for all log messages.
     *             Use your favorite logging tool to configure logging for
     *             this class.
     */
    private static int debug = 0;

    /**
     * The <code>debug</code> static property is no longer used
     * @return debug property
     * @deprecated BeanUtils now uses commons-logging for all log messages.
     *             Use your favorite logging tool to configure logging for
     *             this class.
     */
    public static int getDebug() {
        return (debug);
    }

    /**
     * The <code>debug</code> static property is no longer used
     * @param newDebug debug property
     * @deprecated BeanUtils now uses commons-logging for all log messages.
     *             Use your favorite logging tool to configure logging for
     *             this class.
     */
    public static void setDebug(int newDebug) {
        debug = newDebug;
    }

    // --------------------------------------------------------- Class Methods




    /**
     * <p>Return the value of the specified array property of the specified
     * bean, as a String array.</p>
     *
     * <p>For more details see <code>BeanUtilsBean</code>.</p>
     *
     * @param bean Bean whose property is to be extracted
     * @param name Name of the property to be extracted
     * @return The array property value
     *
     * @exception IllegalAccessException if the caller does not have
     *  access to the property accessor method
     * @exception InvocationTargetException if the property accessor method
     *  throws an exception
     * @exception NoSuchMethodException if an accessor method for this
     *  property cannot be found
     * @see BeanUtils#getArrayProperty 
     */
    public static String[] getArrayProperty(Object bean)
            throws IllegalAccessException, InvocationTargetException,
            NoSuchMethodException {

        return BeanUtils.getInstance().getArrayProperty(bean);
    }


    /** 
     * If we're running on JDK 1.4 or later, initialize the cause for the given throwable.
     * 
     * @param  throwable The throwable.
     * @param  cause     The cause of the throwable.
     * @return  true if the cause was initialized, otherwise false.
     * @since 1.8.0
     */
    public static boolean initCause(Throwable throwable, Throwable cause) {
        return BeanUtils.getInstance().initCause(throwable, cause);
    }

    /**
     * Create a cache.
     * @return a new cache
     * @since 1.8.0
     */
    @SuppressWarnings("rawtypes")
	public static Map createCache() {
        return new WeakFastHashMap();
    }

    /**
     * Return whether a Map is fast
     * @param map The map
     * @return Whether it is fast or not.
     * @since 1.8.0
     */
    @SuppressWarnings("rawtypes")
	public static boolean getCacheFast(Map map) {
        if (map instanceof WeakFastHashMap) {
            return ((WeakFastHashMap)map).getFast();
        } else {
            return false;
        }
    }

    /**
     * Set whether fast on a Map
     * @param map The map
     * @param fast Whether it should be fast or not.
     * @since 1.8.0
     */
    @SuppressWarnings("rawtypes")
	public static void setCacheFast(Map map, boolean fast) {
        if (map instanceof WeakFastHashMap) {
            ((WeakFastHashMap)map).setFast(fast);
        }
    }
}
