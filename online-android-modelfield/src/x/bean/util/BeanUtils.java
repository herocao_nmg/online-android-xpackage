/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package x.bean.util;


import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
//import org.apache.commons.logging.LogFactory;

import x.bean.ContextClassLoaderLocal;
import x.bean.ConversionException;
import x.bean.ConvertUtilsBean;
import x.bean.Converter;


/**
 * <p>JavaBean property population methods.</p>
 *
 * <p>This class provides implementations for the utility methods in
 * {@link BeanManager}.
 * Different instances can be used to isolate caches between classloaders
 * and to vary the value converters registered.</p>
 *
 * @author Craig R. McClanahan
 * @author Ralph Schaer
 * @author Chris Audley
 * @author Rey Francois
 * @author Gregor Rayman
 * @version $Revision: 834031 $ $Date: 2009-11-09 12:26:52 +0000 (Mon, 09 Nov 2009) $
 * @see BeanManager
 * @since 1.7
 */

public class BeanUtils {


    // ------------------------------------------------------ Private Class Variables

    /** 
     * Contains <code>BeanUtilsBean</code> instances indexed by context classloader.
     */
    private static final ContextClassLoaderLocal 
            BEANS_BY_CLASSLOADER = new ContextClassLoaderLocal() {
                        // Creates the default instance used when the context classloader is unavailable
                        protected Object initialValue() {
                            return new BeanUtils();
                        }
                    };
    
    /** 
     * Gets the instance which provides the functionality for {@link BeanManager}.
     * This is a pseudo-singleton - an single instance is provided per (thread) context classloader.
     * This mechanism provides isolation for web apps deployed in the same container.
     *
     * @return The (pseudo-singleton) BeanUtils bean instance
     */
    public static BeanUtils getInstance() {
        return (BeanUtils) BEANS_BY_CLASSLOADER.get();
    }

    /** 
     * Sets the instance which provides the functionality for {@link BeanManager}.
     * This is a pseudo-singleton - an single instance is provided per (thread) context classloader.
     * This mechanism provides isolation for web apps deployed in the same container.
     * 
     * @param newInstance The (pseudo-singleton) BeanUtils bean instance
     */
    public static void setInstance(BeanUtils newInstance) {
        BEANS_BY_CLASSLOADER.set(newInstance);
    }

    // --------------------------------------------------------- Attributes

    
    /** Used to perform conversions between object types when setting properties */
    private ConvertUtilsBean convertUtilsBean;
    

    /** A reference to Throwable's initCause method, or null if it's not there in this JVM */
    private static final Method INIT_CAUSE_METHOD = getInitCauseMethod();

    // --------------------------------------------------------- Constuctors
    
    public BeanUtils() {
    	convertUtilsBean=new ConvertUtilsBean();
  }
    // --------------------------------------------------------- Public Methods



    /**
     * Return the value of the specified array property of the specified
     * bean, as a String array.
     *
     * @param bean Bean whose property is to be extracted
     * @param name Name of the property to be extracted
     * @return The array property value
     *
     * @exception IllegalAccessException if the caller does not have
     *  access to the property accessor method
     * @exception InvocationTargetException if the property accessor method
     *  throws an exception
     * @exception NoSuchMethodException if an accessor method for this
     *  property cannot be found
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public String[] getArrayProperty(Object value)
            throws IllegalAccessException, InvocationTargetException,
            NoSuchMethodException {

        if (value == null) {
            return (null);
        } else if (value instanceof Collection) {
            ArrayList values = new ArrayList();
            Iterator items = ((Collection) value).iterator();
            while (items.hasNext()) {
                Object item = items.next();
                if (item == null) {
                    values.add((String) null);
                } else {
                    values.add(getConvertUtils().convert(item));
                }
            }
            return ((String[]) values.toArray(new String[values.size()]));
        } else if (value.getClass().isArray()) {
            int n = Array.getLength(value);
            String[] results = new String[n];
            for (int i = 0; i < n; i++) {
                Object item = Array.get(value, i);
                if (item == null) {
                    results[i] = null;
                } else {
                    results[i] = getConvertUtils().convert(item);
                }
            }
            return (results);
        } else {
            String[] results = new String[1];
            results[0] = getConvertUtils().convert(value);
            return (results);
        }

    }


    
    /** 
     * Gets the <code>ConvertUtilsBean</code> instance used to perform the conversions.
     *
     * @return The ConvertUtils bean instance
     */
    public ConvertUtilsBean getConvertUtils() {
        return convertUtilsBean;
    }
    
    /** 
     * If we're running on JDK 1.4 or later, initialize the cause for the given throwable.
     * 
     * @param  throwable The throwable.
     * @param  cause     The cause of the throwable.
     * @return  true if the cause was initialized, otherwise false.
     * @since 1.8.0
     */
    public boolean initCause(Throwable throwable, Throwable cause) {
        if (INIT_CAUSE_METHOD != null && cause != null) {
            try {
                INIT_CAUSE_METHOD.invoke(throwable, new Object[] { cause });
                return true;
            } catch (Throwable e) {
                return false; // can't initialize cause
            }
        }
        return false;
    }

    /**
     * <p>Convert the value to an object of the specified class (if
     * possible).</p>
     *
     * @param value Value to be converted (may be null)
     * @param type Class of the value to be converted to
     * @return The converted value
     *
     * @exception ConversionException if thrown by an underlying Converter
     * @since 1.8.0
     */
    @SuppressWarnings("rawtypes")
	protected Object convert(Object value, Class type) {
        Converter converter = getConvertUtils().lookup(type);
        if (converter != null) {
            return converter.convert(type, value);
        } else {
            return value;
        }
    }

    /**
     * Returns a <code>Method<code> allowing access to
     * {@link Throwable#initCause(Throwable)} method of {@link Throwable},
     * or <code>null</code> if the method
     * does not exist.
     * 
     * @return A <code>Method<code> for <code>Throwable.initCause</code>, or
     * <code>null</code> if unavailable.
     */ 
    @SuppressWarnings("rawtypes")
	private static Method getInitCauseMethod() {
        try {
            Class[] paramsClasses = new Class[] { Throwable.class };
            return Throwable.class.getMethod("initCause", paramsClasses);
        } catch (NoSuchMethodException e) {
        	e.printStackTrace();
            return null;
        } catch (Throwable e) {
        	e.printStackTrace();
            return null;
        }
    }
}
