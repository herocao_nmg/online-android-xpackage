package x.modelfield.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 類型工具類
 * @author Hero
 * @since 2013-12-12
 */
public class TypeUtils {
	
	public static Class<?> getActualType(Type type){
		if(null==type) return null;
		
		ParameterizedType pType=(ParameterizedType) type;
		return (Class<?>) pType.getActualTypeArguments()[0];
	}
	
}
