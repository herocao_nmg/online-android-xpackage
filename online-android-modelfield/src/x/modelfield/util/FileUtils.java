package x.modelfield.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class FileUtils {

	public static byte[] getByteFromInputStream(InputStream is) {
		try {
			byte[] buff = new byte[1024 * 16];
			int len = -1;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			while ((len = is.read(buff)) != -1) {
				baos.write(buff, 0, len);
			}
			baos.flush();
			byte[] data = baos.toByteArray();
			baos.close();
			baos = null;
			return data;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
