package x.modelfield;

import java.io.Serializable;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ModelField Annotation
 * @author Hero
 * @since 2013-12-12
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ModelField {

	public Class<? extends Serializable> model() default Void.class;
	public boolean isJson() default false;
	public boolean entityToJson() default false;
	public boolean jsonToEntity() default false;
	public String typeConvertSeparator() default ",";
	public boolean arrayOrCollectionToString() default false;
	public boolean stringToStringArrayOrCollection() default false;
	
}
