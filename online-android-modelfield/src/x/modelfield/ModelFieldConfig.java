package x.modelfield;

import java.io.Serializable;

/**
 * Model字段配置
 * @author Hero
 * @since 2013-12-12
 */
public class ModelFieldConfig {

	private String fieldName;
	private Class<? extends Serializable> model;
	private boolean isJson;
	private boolean entityToJson;
	private boolean jsonToEntity;
	private String typeConvertSeparator;
	private boolean arrayOrCollectionToString;
	private boolean stringToStringArrayOrCollection;
	
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Class<? extends Serializable> getModel() {
		return model;
	}

	public void setModel(Class<? extends Serializable> model) {
		this.model = model;
	}

	public boolean isJson() {
		return isJson;
	}

	public void setJson(boolean isJson) {
		this.isJson = isJson;
	}

	public boolean isEntityToJson() {
		return entityToJson;
	}

	public void setEntityToJson(boolean entityToJson) {
		this.entityToJson = entityToJson;
	}

	public boolean isJsonToEntity() {
		return jsonToEntity;
	}

	public void setJsonToEntity(boolean jsonToEntity) {
		this.jsonToEntity = jsonToEntity;
	}


	public String getTypeConvertSeparator() {
		return typeConvertSeparator;
	}

	public void setTypeConvertSeparator(String typeConvertSeparator) {
		this.typeConvertSeparator = typeConvertSeparator;
	}

	public boolean isArrayOrCollectionToString() {
		return arrayOrCollectionToString;
	}

	public void setArrayOrCollectionToString(boolean arrayOrCollectionToString) {
		this.arrayOrCollectionToString = arrayOrCollectionToString;
	}

	public boolean isStringToStringArrayOrCollection() {
		return stringToStringArrayOrCollection;
	}

	public void setStringToStringArrayOrCollection(
			boolean stringToStringArrayOrCollection) {
		this.stringToStringArrayOrCollection = stringToStringArrayOrCollection;
	}

	public ModelFieldConfig() {
		//
	}
	
	public ModelFieldConfig(String fieldName) {
		this.fieldName=fieldName;
	}

	public ModelFieldConfig(String fieldName,
			Class<? extends Serializable> model, boolean isJson,
			boolean entityToJson, boolean jsonToEntity,
			String typeConvertSeparator, boolean arrayOrCollectionToString,
			boolean stringToStringArrayOrCollection) {
		this.fieldName = fieldName;
		this.model = model;
		this.isJson = isJson;
		this.entityToJson = entityToJson;
		this.jsonToEntity = jsonToEntity;
		this.typeConvertSeparator = typeConvertSeparator;
		this.arrayOrCollectionToString = arrayOrCollectionToString;
		this.stringToStringArrayOrCollection = stringToStringArrayOrCollection;
	}

	
	
	
}
