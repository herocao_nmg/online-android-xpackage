package x.modelfield;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;

import x.bean.util.BeanUtils;
import x.json.JsonParser;
import android.annotation.SuppressLint;
import android.text.TextUtils;

/**
 * Model字段工具
 * @author Hero
 * @since 2013-12-12
 */
public class ModelFieldConfigUtils {

	@SuppressLint("UseSparseArrays")
	private static final Map<Integer,String> fieldNameMap=new HashMap<Integer, String>();
	
	private static final int FIELD_MODEL=1;
	private static final int FIELD_IS_JSON=2;
	private static final int FIELD_ENTITY_TO_JSON=3;
	private static final int FIELD_JSON_TO_ENTITY=4;
	private static final int FIELD_TYPE_CONVERT_SEPARATOR=5;
	private static final int FIELD_ARRAY_OR_COLLECTION_TO_STRING=6;
	private static final int FIELD_STRING_TO_STRING_ARRAY_OR_COLLECTION=7;
	
	static{
		fieldNameMap.put(FIELD_MODEL, "model");
		fieldNameMap.put(FIELD_IS_JSON, "isJson");
		fieldNameMap.put(FIELD_ENTITY_TO_JSON, "entityToJson");
		fieldNameMap.put(FIELD_JSON_TO_ENTITY, "jsonToEntity");
		fieldNameMap.put(FIELD_TYPE_CONVERT_SEPARATOR, "typeConvertSeparator");
		fieldNameMap.put(FIELD_ARRAY_OR_COLLECTION_TO_STRING, "arrayOrCollectionToString");
		fieldNameMap.put(FIELD_STRING_TO_STRING_ARRAY_OR_COLLECTION, "stringToStringArrayOrCollection");
	}
	
	private static Class<?> annotationFactoryClazz;
	private static Field elementsField;
	private static Class<?> annotationMemberClazz;
	private static Field nameField;
	private static Field valueField;
	private static final int[] configFieldNums = lookupClasses();
	
	private static int configFieldNameToNum(String configName) {
		if(TextUtils.isEmpty(configName)) throw new IllegalStateException("support for ModelField "+configName+" can't be null");
		for(int key : fieldNameMap.keySet()){
			if(fieldNameMap.get(key).equals(configName)){
				return key;
			}
		}
		throw new IllegalStateException("Could not find support for ModelField " + configName);
	}
	
	private static int[] lookupClasses() {
		Class<?> annotationMemberArrayClazz;
		try {
			annotationFactoryClazz = Class.forName("org.apache.harmony.lang.annotation.AnnotationFactory");
			annotationMemberClazz = Class.forName("org.apache.harmony.lang.annotation.AnnotationMember");
			annotationMemberArrayClazz = Class.forName("[Lorg.apache.harmony.lang.annotation.AnnotationMember;");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}

		Field fieldField;
		try {
			elementsField = annotationFactoryClazz.getDeclaredField("elements");
			elementsField.setAccessible(true);
			nameField = annotationMemberClazz.getDeclaredField("name");
			nameField.setAccessible(true);
			valueField = annotationMemberClazz.getDeclaredField("value");
			valueField.setAccessible(true);
			fieldField = ModelFieldSample.class.getDeclaredField("field");
		} catch (SecurityException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			return null;
		}

		ModelField modelField = fieldField.getAnnotation( ModelField.class);
		InvocationHandler proxy = Proxy.getInvocationHandler(modelField);
		if (proxy.getClass() != annotationFactoryClazz) {
			return null;
		}

		try {
			Object elements = elementsField.get(proxy);
			if (elements == null || elements.getClass() != annotationMemberArrayClazz) {
				return null;
			}

			Object[] elementArray = (Object[]) elements;
			int[] configNums = new int[elementArray.length];
			for (int i = 0; i < elementArray.length; i++) {
				String name = (String) nameField.get(elementArray[i]);
				configNums[i] = configFieldNameToNum(name);
			}
			return configNums;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 克隆一個Model的字段的值給目標對象 , 如果ModelField設置 jsonToEntity=true 或者  entityToJson()=true 必須實現jsonParser接口
	 * @param withModelFieldOfObject
	 * @param targetClass
	 * @return
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 */
	public static <T extends Serializable> T  fromModel(Object withModelFieldOfObject,Class<T> targetClass) throws IllegalAccessException, IllegalArgumentException, InstantiationException, InvocationTargetException, NoSuchMethodException{
		return fromModel(withModelFieldOfObject, targetClass, null);
	}
	
	/**
	 * 克隆一個Model的字段的值給目標對象
	 * @param withModelFieldOfObject
	 * @param targetClass
	 * @param jsonParser 如果ModelField設置 jsonToEntity=true 或者  entityToJson()=true 必須實現接口
	 * @return
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 */
	public static  <T extends Serializable> T  fromModel(Object withModelFieldOfObject,Class<T> targetClass,JsonParser jsonParser) throws IllegalAccessException, IllegalArgumentException, InstantiationException, InvocationTargetException, NoSuchMethodException{
		if(null==withModelFieldOfObject || null==targetClass ) throw new IllegalArgumentException("At {@link ModelFieldConfigUtils} of fromModel method has null value !");
		
		T t = targetClass.newInstance();
		for (Field field : withModelFieldOfObject.getClass()
				.getDeclaredFields()) {
			field.setAccessible(true);
			Object value = field.get(withModelFieldOfObject);
			if (null != value) {
				ModelFieldConfigUtils.fromField(withModelFieldOfObject, t, field, value, jsonParser);
			}
		}
				
		return t;
	}
	
	private static <T extends Serializable> void fromField(Object model,T t,Field field,Object value,JsonParser jsonParser) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException{
		if(null==fieldNameMap || fieldNameMap.size()<=0) return;
		
		ModelField modelField = field.getAnnotation(ModelField.class);
		if(null==modelField) return;
		
		InvocationHandler proxy = Proxy.getInvocationHandler(modelField);
		if (proxy.getClass() != annotationFactoryClazz) {
			return;
		}
		Object elementsObject = elementsField.get(proxy);
		if (elementsObject == null) {
			return;
		}
		
		ModelFieldConfig config = new ModelFieldConfig(field.getName());
		Object[] objs = (Object[]) elementsObject;
		for (int i = 0; i < configFieldNums.length; i++) {
			Object obj= valueField.get(objs[i]);
			if(obj!=null)
				assignConfigField(configFieldNums[i], config, field, obj);
		}
		toField(t, field, value, config,jsonParser);
	}
	
	private static <T> void toField(T t,Field field,Object value,ModelFieldConfig config,JsonParser jsonParser) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException{
		String name=field.getName();
		boolean srcIsArray=field.getType().isArray();
		Field[] fields = t.getClass().getDeclaredFields();
		boolean isNull=null==jsonParser;
		for(Field tempField : fields){
			tempField.setAccessible(true);
			if(tempField.getName().equals(name)){
				boolean targetIsArray = tempField.getType().isArray();
				boolean result=!TextUtils.isEmpty(config.getTypeConvertSeparator());
				if(config.isJson() && config.getModel() instanceof Serializable && config.isJsonToEntity() && !isNull){
					Serializable entity = jsonParser.fromJson((String)value, config.getModel());
					tempField.set(t, entity);
				}else if(config.isEntityToJson() && !isNull){
					String json=jsonParser.toJson(value);
					tempField.set(t, json);
				}else if(config.isArrayOrCollectionToString() && result  && srcIsArray){
					String[] data= BeanUtils.getInstance().getArrayProperty(value);
						StringBuilder sb=new StringBuilder();
						for(String str : data){
							sb.append(str).append(config.getTypeConvertSeparator());
						}
						String strData = sb.toString();
						tempField.set(t, strData.substring(0, strData.length()-1));
						data=null;
						strData=null;
						sb=null;
				}else if(config.isStringToStringArrayOrCollection() && result && targetIsArray){
					String[] data = value.toString().split(config.getTypeConvertSeparator());
					tempField.set(t, data);
					data=null;
				}else{
					tempField.set(t, value);
				}
				break;
			}
		}
		fields=null;  
	}
	
	
	@SuppressWarnings("unchecked")
	private static void assignConfigField(int configNum, ModelFieldConfig config, Field field, Object value) {
		switch (configNum) {
		case FIELD_MODEL:
			config.setModel((Class<? extends Serializable>) value);
			break;
		case FIELD_IS_JSON:
			config.setJson((Boolean) value);
			break;
		case FIELD_JSON_TO_ENTITY:
			config.setJsonToEntity((Boolean) value);
			break;
		case FIELD_ENTITY_TO_JSON:
			config.setEntityToJson((Boolean) value);
			break;
		case FIELD_TYPE_CONVERT_SEPARATOR:
			config.setTypeConvertSeparator(valueIfNotBlank((String)value));
			break;
		case FIELD_ARRAY_OR_COLLECTION_TO_STRING:
			config.setArrayOrCollectionToString((Boolean) value);
			break;
		case FIELD_STRING_TO_STRING_ARRAY_OR_COLLECTION:
			config.setStringToStringArrayOrCollection((Boolean) value);
			break;
		default :
			throw new IllegalStateException("Could not find support for ModelField number " + configNum);
		}
	}
	
	private static String valueIfNotBlank(String value) {
		if (value == null || value.length() == 0) {
			return null;
		} else {
			return value;
		}
	}
	
	private static class ModelFieldSample {
		@ModelField
		String field;
	}
}
