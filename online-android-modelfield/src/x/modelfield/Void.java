package x.modelfield;

import java.io.Serializable;

/**
 * 
 * @author Hero
 * @since 2013-12-12
 */
public final class Void implements Serializable {
	private static final long serialVersionUID = -3857153500671781419L;
}
