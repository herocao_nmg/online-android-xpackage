package x.json;

import java.io.Serializable;

import com.google.gson.Gson;

/**
 * Json的基本解析器
 * @author Hero
 * @since 2013-12-12
 */
public class BasicJsonParser implements JsonParser {

	private Gson mGson=new Gson();
	
	@Override
	public String toJson(Object object) {
		if(null==object) return null;
		return mGson.toJson(object);
	}

	@Override
	public <T extends Serializable> T fromJson(String json,Class<T> clazz) {
		if(null==json || null==clazz) return null;
		return mGson.fromJson(json, clazz);
	}

}
