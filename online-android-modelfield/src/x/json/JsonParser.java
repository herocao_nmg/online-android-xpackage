package x.json;

import java.io.Serializable;

/**
 * Json解析器接口
 * @author Hero
 * @since 2013-12-12
 */
public interface JsonParser {
	String toJson(Object object);
	<T extends Serializable> T fromJson(String json,Class<T> clazz);
}
