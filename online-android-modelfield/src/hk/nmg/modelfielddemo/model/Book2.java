package hk.nmg.modelfielddemo.model;

import java.io.Serializable;
import java.util.List;

public class Book2 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4388209376842658449L;
	
	
	public String name;
	public int age;
	public String address;
	public int page;
	@Override
	public String toString() {
		return "Book2 [name=" + name + ", age=" + age + ", address=" + address
				+", page=" + page + "]";
	}
	
	
}
