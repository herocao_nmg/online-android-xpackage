package hk.nmg.modelfielddemo.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import x.modelfield.ModelField;

public class Person implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -216245444591206716L;

	@ModelField(model=String.class,isJson=true,typeConvertSeparator="@")
	public String name;
	@ModelField(model=Integer.class,isJson=true,typeConvertSeparator="&")
	public int age;
	@ModelField(model=Long.class,isJson=true,typeConvertSeparator="#")
	public String address;
	@ModelField
	public List<String> content;
	@ModelField
	public List<Book2> books;
	
	@ModelField(arrayOrCollectionToString=true,typeConvertSeparator="%@%")
	public String[] data1;
	
	@ModelField(stringToStringArrayOrCollection=true,typeConvertSeparator="@@@")
	public String data2;
	
	
	@ModelField(isJson=true,model=BookList.class,jsonToEntity=true)
	public String json;
	
	@ModelField(entityToJson=true)
	public List<Book2> books2;
	
	@ModelField(arrayOrCollectionToString=true)
	public Long[] data3;
	@ModelField
	public Book2 books3;
	@ModelField
	public List<Book> bookList;
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", address=" + address
				+ ", content=" + content + ", books=" + books + ", data1="
				+ Arrays.toString(data1) + ", data2=" + data2 +" , data3="+data3+ " , json="
				+ json +" ,\r\n\r\n -->  bookList="+bookList+ "]";
	}
	
	
}
