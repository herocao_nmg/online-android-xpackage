package hk.nmg.modelfielddemo.model;

import java.io.Serializable;

import x.modelfield.ModelField;

public class Animal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6581682814333210953L;

	@ModelField
	public String name;
	@ModelField
	public int age;
	public String des;
	@Override
	public String toString() {
		return "Animal [name=" + name + ", age=" + age +" , des="+des+ "]";
	}
	
}
