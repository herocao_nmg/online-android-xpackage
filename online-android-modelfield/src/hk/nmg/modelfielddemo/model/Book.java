package hk.nmg.modelfielddemo.model;

import java.io.Serializable;
import java.util.Arrays;

public class Book implements Serializable{

	private static final long serialVersionUID = 1L;
	
	public long book_id;
	public String short_title;
	public int type;// "4",書本格式
	public String page_size;
	public String page_number_type;
	public String zoomable;// "0",
	public String orientation;// "3",
	public int number;// "1",1=Book A, 2=Book B, 可以不順序，可以重覆。
	public String thumbnail;// "1",
	public String toc;// "0",
	public int reader_style;// "0",
	public int cover_lastmodify;// "1378372813",
	public int book_lastmodify;// "1378372812",
	public String encryption;// "",
	public String issue;// "FS12",
	public int issue_id;// "3000012",
	public String title;// "Book 12 Toyota Landscape 2 pages",
	public String cover_url;// "http://mbook.nmg.hk/upload/2013/reader3/book_cover/13/09/131.png",
	public String[] download_url;// ["http://mbook.nmg.hk/upload/2013/reader3/book_zip/13/09/13.zip"],array, 每本書可多於⼀一個下載檔案
	public String[] pdownload_url;// ["http://mbook.nmg.hk/upload/2013/reader3/book_zip/13/09/13.zip"],array, 每本書可多於⼀一個下載檔案
	public String offline_readable;// 1,是否能離線閱讀
	public String flip;// 1,揭頁方向，1為右到左
	@Override
	public String toString() {
		return "Book [book_id=" + book_id + ", short_title=" + short_title
				+ ", type=" + type + ", page_size=" + page_size
				+ ", page_number_type=" + page_number_type + ", zoomable="
				+ zoomable + ", orientation=" + orientation + ", number="
				+ number + ", thumbnail=" + thumbnail + ", toc=" + toc
				+ ", reader_style=" + reader_style + ", cover_lastmodify="
				+ cover_lastmodify + ", book_lastmodify=" + book_lastmodify
				+ ", encryption=" + encryption + ", issue=" + issue
				+ ", issue_id=" + issue_id + ", title=" + title
				+ ", cover_url=" + cover_url + ", download_url="
				+ Arrays.toString(download_url) + ", pdownload_url="
				+ Arrays.toString(pdownload_url) + ", offline_readable="
				+ offline_readable + ", flip=" + flip + "]";
	}

	
}
