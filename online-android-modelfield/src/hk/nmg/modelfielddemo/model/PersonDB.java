package hk.nmg.modelfielddemo.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class PersonDB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4784490785722802320L;

	public String name;
	public int age;
	public String address;
	public List<String> content;
	public List<Book2> books;
	public String data1;
	public String[] data2;
	public BookList json;
	public String books2;
	public String data3;
	public Book2 books3;
	public List<Book> bookList;
	@Override
	public String toString() {
		return "PersonDB [name=" + name + ", age=" + age + ", address="
				+ address + ", content=" + content + ", books=" + books
				+ ", data1=" + data1 + ", data2=" + Arrays.toString(data2) +" , data3="+data3
				+ ", json=" + json +" , \r\n\r\n --> bookList="+bookList+ "]";
	}
	
	
}
