package hk.nmg.modelfielddemo;

import hk.nmg.modelfielddemo.model.Animal;
import hk.nmg.modelfielddemo.model.Book;
import hk.nmg.modelfielddemo.model.Book2;
import hk.nmg.modelfielddemo.model.Person;
import hk.nmg.modelfielddemo.model.PersonDB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import x.json.BasicJsonParser;
import x.json.JsonParser;
import x.modelfield.ModelFieldConfigUtils;
import x.modelfield.util.FileUtils;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;

public class MainActivity extends Activity {

	private static final String TAG = "MainActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		int count=0;
		
		long currentTime;
		System.out.println("current time="+(currentTime=System.currentTimeMillis()));
		
		
		// 11 * 1000  ,  44674 ,  44s  , 白色平板  ,   10 * 11  , 558
		
		//  11 * 1000 HTC ,   56474  ,     11 * 10 ,   1138 
		while(count<=10){
			count++;
			
		Person person=new Person();
		person.name="test_name";
		person.address="test_address";
		person.age=25;
		List<String> content=new ArrayList<String>();
		
		content.add("content01");
		content.add("content02");
		content.add("content03");
		content.add("content04");
		content.add("content05");
		
		person.content=content;
		
		

		List<Book2> books=new ArrayList<Book2>();
		
		for(int i=0;i<3;i++){
			Book2 book=new Book2();
			book.name="name_"+i;
			book.page=800+i;
			books.add(book);
		}
		
		person.books=books;
		
		person.data1=new String[]{ "data01","data02","data03"};
		person.data2="test01@@@test02@@@test03";
		
		person.books2=books;
		
		person.data3=new Long[]{11L,22L,33L};
	
		
		Book2 book3=new Book2();
		book3.name="array_name_"+12;
		book3.page=500+12;
		person.books3=book3;
		
		try {
			
			byte[] jsonData=FileUtils.getByteFromInputStream(getResources().getAssets().open("issue_json2.txt"));
			
			String json=new String(jsonData,0,jsonData.length,"utf-8");
			
			// Ormlite 
			
			person.json=json;
			
			Log.i(TAG, "json ="+person.json);
			
			Gson gson=new Gson();
			List<Book> bookList = gson.fromJson(json, List.class);
			
			System.out.println("bookList="+bookList);
			
			person.bookList=bookList;
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			Log.i(TAG, "error 1 ="+e1.getMessage());
		}
		
		try {
			
			JsonParser jsonParser=new BasicJsonParser();
			
			PersonDB personDB = ModelFieldConfigUtils.fromModel(person, PersonDB.class,jsonParser);
			
			// 
			
			
			
			// insert  update -->DB 
			
			// Ormlite 
			
			//
			
			Log.i(TAG, "personDB="+personDB);
			
			for(Book bk : personDB.json){
				System.out.println("bk="+bk);
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i(TAG, "error 2 ="+e.getMessage());
			Log.i(TAG, "error 2 ="+e.getStackTrace()[0]);
		}
		
		System.out.println("\r\n------- Clone ---- , count="+count);
		
		}
		
		System.out.println("\r\n\r\n\r\n ------- Clone ---- , count="+count+" , over !!!");
		
		System.out.println("\r\n\r\n over time="+(System.currentTimeMillis()-currentTime));
		
		///////////  簡單克隆
		
		Animal animal=new Animal();
		animal.name="動物名稱";
		animal.age=88;
		animal.des="描述內容";
		
		try {
			Animal animal2 = ModelFieldConfigUtils.fromModel(animal, Animal.class);
			
			System.out.println("\r\n Animal , "+animal);
			System.out.println("\r\n Animal , "+animal2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
}
