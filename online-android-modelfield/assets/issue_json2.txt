[
        {
          "book_id": "27",
          "title": "Book A",
          "short_title": "Issue0730A",
          "type": "4",
          "page_size": "auto",
          "page_number_type": "2",
          "zoomable": "0",
          "orientation": "1",
          "number": "1",
          "thumbnail": "1",
          "toc": "3",
          "reader_style": "1",
          "cover_lastmodify": "1385632394",
          "book_lastmodify": "1385956554",
          "encryption": "2",
          "issue": "",
          "issue_id": "100000006",
          "cover_url": "http:\/\/bsdl01.nmg.com.hk\/upload\/2013\/wwreader3\/book_cover\/13\/11\/272.jpg",
          "download_url": [
            
          ],
          "pdownload_url": [
            
          ],
          "offline_readable": 1,
          "flip": 1
        },
        {
          "book_id": "28",
          "title": "Book C",
          "short_title": "Issue0730C",
          "type": "4",
          "page_size": "auto",
          "page_number_type": "3",
          "zoomable": "0",
          "orientation": "1",
          "number": "3",
          "thumbnail": "1",
          "toc": "3",
          "reader_style": "1",
          "cover_lastmodify": "1385355321",
          "book_lastmodify": "1385955228",
          "encryption": "2",
          "issue": "",
          "issue_id": "100000006",
          "cover_url": "http:\/\/bsdl01.nmg.com.hk\/upload\/2013\/wwreader3\/book_cover\/13\/11\/282.jpg",
          "download_url": [
            
          ],
          "pdownload_url": [
            
          ],
          "offline_readable": 1,
          "flip": 1
        },
        {
          "book_id": "29",
          "title": "Book D",
          "short_title": "Issue0730D",
          "type": "4",
          "page_size": "auto",
          "page_number_type": "3",
          "zoomable": "0",
          "orientation": "1",
          "number": "4",
          "thumbnail": "1",
          "toc": "3",
          "reader_style": "1",
          "cover_lastmodify": "1385355336",
          "book_lastmodify": "1385954301",
          "encryption": "2",
          "issue": "",
          "issue_id": "100000006",
          "cover_url": "http:\/\/bsdl01.nmg.com.hk\/upload\/2013\/wwreader3\/book_cover\/13\/11\/292.jpg",
          "download_url": [
            
          ],
          "pdownload_url": [
            
          ],
          "offline_readable": 1,
          "flip": 1
        },
        {
          "book_id": "33",
          "title": "Book E",
          "short_title": "Issue0730E",
          "type": "4",
          "page_size": "auto",
          "page_number_type": "3",
          "zoomable": "0",
          "orientation": "1",
          "number": "5",
          "thumbnail": "1",
          "toc": "3",
          "reader_style": "1",
          "cover_lastmodify": "1385355353",
          "book_lastmodify": "1385957580",
          "encryption": "2",
          "issue": "",
          "issue_id": "100000006",
          "cover_url": "http:\/\/bsdl01.nmg.com.hk\/upload\/2013\/wwreader3\/book_cover\/13\/11\/332.jpg",
          "download_url": [
            "http:\/\/bsdl01.nmg.com.hk\/public\/wwreader3\/book_zip\/33_29.zip"
          ],
          "pdownload_url": [
            
          ],
          "offline_readable": 1,
          "flip": 1
        }
      ]
 