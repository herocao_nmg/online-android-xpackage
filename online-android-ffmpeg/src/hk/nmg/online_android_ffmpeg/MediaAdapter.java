package hk.nmg.online_android_ffmpeg;

import java.io.File;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MediaAdapter extends BaseAdapter {
	
	private Context mContext;
	private List<File> files;
	

	public MediaAdapter(Context context,List<File> files) {
		if(null==files || null==context) throw new IllegalArgumentException("At {@link MediaAdapter } of parameter is null !");
		this.mContext = context;
		this.files=files;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return files.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return files.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View converterView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		ViewHolder vh=null;
		if(null==converterView){
			converterView=View.inflate(mContext, R.layout.video_listview_item, null);
			vh=new ViewHolder();
			vh.icon=(ImageView) converterView.findViewById(R.id.iv_icon);
			vh.title=(TextView) converterView.findViewById(R.id.tv_title);
			converterView.setTag(vh);
		}else{
			vh=(ViewHolder) converterView.getTag();
		}
		
		vh.title.setText(files.get(position).getName());
		
		return converterView;
	}
	
	private final static class ViewHolder{
		private ImageView icon;
		private TextView title;
	}

}
