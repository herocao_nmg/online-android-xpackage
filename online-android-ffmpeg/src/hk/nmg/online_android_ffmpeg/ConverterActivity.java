package hk.nmg.online_android_ffmpeg;

import hk.nmg.online_android_ffmpeg.constant.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.netcompss.ffmpeg4android_client.BaseWizard;

public class ConverterActivity extends BaseWizard {
	
	/**
	 * <string-array name="formats">
        <item>MP4</item>
        <item>FLV</item>
        <item>3GP</item>
        <item>AVI</item>
        <item>MOV</item>
        <item>MPG</item>
        <item>WMV</item>
    </string-array>
    
    <string-array name="resolutions">
        <item>128x96(SQCIF)</item>
        <item>160x120(QQVGA)</item>
        <item>320x240(QVGA)</item>
        <item>640x480(VGA)</item>
        <item>800x600(SVGA)</item>
        <item>1024x768(XGA)</item>
        <item>1600x1200(UXGA)</item>
        <item>2048x1536(QXGA)</item>
    </string-array>
    
    <string-array  name="definitions">
        <item>HIGH</item>
        <item>MEDIUM</item>
        <item>LOW</item>
    </string-array>
	 */

	private Spinner spinnerResolution;
	private List<String> resolutions=new ArrayList<String>();
	private List<String> formats=new ArrayList<String>();
	private List<String> definitions=new ArrayList<String>();
	private ArrayAdapter<String> resolutionAdapter;
	private ArrayAdapter<String> definitionAdapter;
	private ArrayAdapter<String> formatAdapter;
//	private TextView tv_resolution;
	private String selectedPath;
private Spinner spinnerDefinition;
private Spinner spinnerFormat;

private int definitionPosition;
private int resolutionPosition;
private int formatPosition;

private TextView tv_selected_path;
private EditText out;
private EditText et_bitrate;

private Activity act=this;

private boolean isFirstDefinition=true;
private boolean isFirstResolution=true;
private boolean isFristFormat=true;

private final int LOOKUP_RESOLUTION=201;

private String resolution;

private Handler handler=new Handler(){
	
	public void handleMessage(android.os.Message msg) {
		
		switch (msg.what) {
		case LOOKUP_RESOLUTION:
			
			System.out.println("handleMessage, 搜索到分辨率 , resolution="+resolution+" ,  format="+format);
			
			if(!formats.contains(format)){
				formats.add(format);
				// 如果是這樣就不需要再遍歷了,最後一項,考慮性能
			}
			
			for(int i=0;i<formats.size();i++){
				if(formats.get(i).contains(format)){
					spinnerFormat.setSelection(i);
					break;
				}
			}
			
			if(!resolutions.contains(resolution)){
				resolutions.add(resolution);
			}
			
		for(int i=0;i<resolutions.size();i++){
			if(resolutions.get(i).contains(resolution)){
				spinnerResolution.setSelection(i);
				break;
			}
		}
		
		spinnerFormat.invalidate();
		spinnerResolution.invalidate();
			
			break;

		 
		}
		
	};
	
};
private String format;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
		setContentView(R.layout.activity_converter);
		
		selectedPath = getIntent().getStringExtra(Constants.SELECTED_PATH);
		
		isFirstDefinition=true;
		isFirstResolution=true;
		isFristFormat=true;
		
		getVideoInfo(selectedPath);
		
		
		System.out.println("onCreate,selectedPath="+selectedPath);
		
		// 
		
		tv_selected_path = (TextView) findViewById(R.id.tv_selected_path);
		
		format = FileUtils.getSuffix(selectedPath);
		
		tv_selected_path.setText(selectedPath);
		
		resolutions.add("128x96(SQCIF)");
		resolutions.add("160x120(QQVGA)");
		resolutions.add("320x240(QVGA)"); 
		resolutions.add("640x480(VGA)");
		resolutions.add("800x600(SVGA)");
		resolutions.add("1024x768(XGA)");
		resolutions.add("1600x1200(UXGA)");
//		resolutions.add("2048x1536(QXGA)");
		
		formats.add("MP4");
		formats.add("FLV");
//		formats.add("3GP");  //3GP需要處理才能支持 ,好像目前沒有封轉到支持3GP的 libs
		formats.add("MOV");
		formats.add("MPG");
		formats.add("WMV");
		formats.add("AVI");
		
        definitions.add("HIGH");
//        definitions.add("MEDIUM");
        definitions.add("LOW");
		
//		tv_resolution = (TextView) findViewById(R.id.tv_resolution);
        
      
		
		spinnerResolution = (Spinner) findViewById(R.id.spinnerResolution);
		spinnerFormat = (Spinner) findViewById(R.id.spinnerFormat);
		spinnerDefinition = (Spinner) findViewById(R.id.spinnerDefinition);
		
		resolutionAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item , R.id.tv_data, resolutions);
		definitionAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item , R.id.tv_data, definitions);
		formatAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item , R.id.tv_data, formats);
		
		out = (EditText) findViewById(R.id.et_out);
		
		et_bitrate = (EditText) findViewById(R.id.et_bitrate);
		
//		adapter=new ArrayAd
		
		
//		adapter.setDropDownViewResource(R.layout.spinner_item);
		
//		spinnerResolution.
		
		spinnerResolution.setAdapter(resolutionAdapter);
		spinnerDefinition.setAdapter(definitionAdapter);
		spinnerFormat.setAdapter(formatAdapter);
		
//		spinnerResolution.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position,
//					long arg3) {
//				// TODO Auto-generated method stub
//				
//				tv_resolution.setText("選擇分辨率:"+data.get(position));
//				
//			}
//		});
		
	 
		
		// 分辨率
		spinnerResolution.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long arg3) {
				// TODO Auto-generated method stub
				
//				tv_resolution.setText("選擇分辨率:"+data.get(position));
				System.out.println("onCreate,分辨率="+resolutions.get(position));
				
				resolutionPosition=position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		// 格式
		spinnerFormat.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				System.out.println("onCreate,格式="+formats.get(position));
				
				formatPosition=position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		// 清晰度
		spinnerDefinition.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				// TODO Auto-generated method stub
				
				System.out.println("onCreate,清晰度="+definitions.get(position));
				
				definitionPosition=position;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		  Button showLog =  (Button)findViewById(R.id.showLastRunLogButton);
	      showLog.setOnClickListener(new OnClickListener() {
				public void onClick(View v){
					startAct(com.netcompss.ffmpeg4android_client.ShowFileAct.class);				
				}
			});
	}
	
	private void getVideoInfo(final String filePath) {
		// TODO Auto-generated method stub
		
		ContentResolver contentResolver = this.getContentResolver();
		
//		contentResolver.query(uri, projection, selection, selectionArgs, sortOrder);
		
		// MediaStore.Audio.Media. EXTERNAL _CONTENT_URI
		
		// MediaStore.Video.Media.DISPLAY_NAME=
		contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,null, null, null,null);
		
//		MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
		
//		MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
		
	new AsyncTask<Void, Void, Void>() {

		

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
//			ExifInterface ei = new ExifInterface(filePath);
			
			 
			
			ContentResolver contentResolver = act.getContentResolver();
//			String name = FileUtils.getFileName(filePath);
			Cursor cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,new String[]{ "resolution","_display_name"}, "_display_name=?", new String[]{ FileUtils.getFileName(filePath) },null);
//			System.out.println("getVideoInfo,doInBackground,cursor="+cursor+" , name="+name);
			if(null!=cursor){
			
			while(cursor.moveToNext()){
				
		/*	int columnCount = cursor.getColumnCount();
			
			System.out.println("getVideoInfo,doInBackground,columnCount="+columnCount);
			
			System.out.println("getVideoInfo,doInBackground,-------------  Begin ... ");
			
			for(int i=0;i<columnCount;i++){
				
				String columnName = cursor.getColumnName(i);
				String value=cursor.getString(i);
				
				// resolution
				System.out.println("getVideoInfo,doInBackground,column="+ i+" , columnName="+columnName+" , value="+value);
			}*/
			
			resolution = cursor.getString(cursor.getColumnIndex("resolution"));
			
			System.out.println("getVideoInfo,doInBackground,resolution="+resolution);
		
//			File file=new File(filePath);
			
			//
			
			if(!TextUtils.isEmpty(resolution)){
				handler.sendEmptyMessage(LOOKUP_RESOLUTION);
			}
			
			//
		
			}
		
			cursor.close();
			cursor=null;
				
			}
			
			System.out.println("getVideoInfo,doInBackground,-------------  Over !!!");
			
			return null;
		}
	}.execute();
		
	}

	public void convert(View view){
		int bitrate=200;
		try{
			bitrate=Integer.valueOf(et_bitrate.getText().toString());
			System.out.println("onCreate,convert,bitrate="+bitrate);
			
			if(bitrate<200){
				bitrate=200;
			}else if(bitrate>3000){
				bitrate=3000;
			}
			
		}catch(Exception e){
			e.printStackTrace();
			Toast.makeText(this, "Sorry,比特率非法值,範圍200-3000", Toast.LENGTH_LONG).show();
			return;
		}
		
		if(TextUtils.isEmpty(out.getText().toString())){
			Toast.makeText(this, "Sorry,請輸入輸出名稱", Toast.LENGTH_LONG).show();
			return;
		}
		
		System.out.println("onCreate,convert,開始轉換 ...");
		
//		String resolution=resolutions.get(resolutionPosition).split("(")[0];
//		String resolution=resolutions.get(resolutionPosition).split("(")[0];
		
		String resolution=null;
		if(resolutions.get(resolutionPosition).contains("(")){
		System.out.println("onCreate,convert,resolutions.get()="+resolutions.get(resolutionPosition).substring(0, resolutions.get(resolutionPosition).lastIndexOf("(")));
		
		resolution=resolutions.get(resolutionPosition).substring(0, resolutions.get(resolutionPosition).lastIndexOf("("));
		}else{
			 resolution=resolutions.get(resolutionPosition);
		}
		
		String definition=definitions.get(definitionPosition);
		String format=formats.get(formatPosition);
		System.out.println("onCreate,convert,resolution="+resolution+" , definition="+definition+" , format="+format);
		String definitionValue=null;
		if("HIGH".equals(definition)){
			definitionValue="-qscale 4";
		}else{
			definitionValue="-qscale 6";
		}
	
		/**
		 * 壓縮率主要最決於 bitrate (bits/s)

作者特別強調畫面大小不會影響檔案大小, 我也就沒試了。基本上就是調整 -b:v, 從參數和影片長度即可推論出最後大概的大小。2000k 畫質就很不錯了。
		 */
		
		/*
		 * 旋轉畫面90度

用 iPhone 直拍的話, 在桌機上播畫面會歪一邊, 可加上 -vf "transpose=1" 將它轉回來。視當初拍的角度, 參數要換不同值, 細節見 man ffmpeg 的 transpose 那節。
		 */
		
		/**
		 * ffmpeg -y -i /sdcard/videokit/in.mp4 -strict experimental -vf transpose=1 -s 160x120 -r 30 -aspect 4:3 -ab 48000 -ac 2 -ar 22050 -b 2097k /sdcard/videokit/out.mp4
		 */
		
		/**
		 * ffmpeg -y -i /storage/sdcard0/Videos/camera.mp4 -strict experimental -vf transpose=1 -s 160x120 -r 29.97 -aspect 4:3 -ab 48000 -ac 2 -ar 22050 -b 2097k  -qscale 6  /sdcard/videokit/outqq.MP4
		 */
		
		/**
		 * 注：如果要转换为3GP格式，则ffmpeg在编译时必须加上–enable-amr_nb –enable-amr_wb，详细内容可参考：转换视频为3GPP格式
		 */
		
		//   -r 29.97 報錯 
//		commandStr="ffmpeg -y -i /sdcard/videokit/in.mp4 -strict experimental -vf transpose=1 -s 160x120 -r 29.97 -aspect 4:3 -ab 48000 -ac 2 -ar 22050 -b 1897k  /sdcard/videokit/out_20140126_1100.mp4";
		
		// 視頻 transpose=1 旋轉 90 
//		commandStr="ffmpeg -y -i /sdcard/videokit/in.mp4 -strict experimental -vf transpose=1 -s 160x120 -r 30 -aspect 4:3 -ab 48000 -ac 2 -ar 22050 -b 1897k  /sdcard/videokit/out_20140126_1109.mp4";
		
		//不可以有多餘的空 , 只能有一個  ,否則失敗 
		
		//  transpose 顛倒  
//		commandStr="ffmpeg -y -i /sdcard/ffmpeg/in.mp4 -strict experimental -vf transpose=1 -s 160x120 -r 30 -aspect 4:3 -ab 48000 -ac 2 -ar 22050 -b 2097k /sdcard/ffmpeg/out.mp4";
//		commandStr="ffmpeg -y -i /sdcard/ffmpeg/in.mp4 -strict experimental -vf transpose=1 -s "+resolution+" -r 29.97 -aspect 4:3 -ab 48000 -ac 2 -ar 22050 -b 2097k "+ formatValue +" /sdcard/ffmpeg/"+out+"."+format;
//		commandStr="ffmpeg -y -i "+selectedPath.trim()+" -strict experimental -s "+resolution.trim()+" -r 29.97 -aspect 4:3 -ab 48000 -ac 2 -ar 22050 -b 2097k "+ definitionValue.trim() +" /sdcard/videokit/"+out.getText().toString()+"."+format.toLowerCase();
		
		if("mp4".equalsIgnoreCase(format)){
		commandStr="ffmpeg -y -i "+selectedPath.trim()+" -strict experimental -s "+resolution.trim()+" -r 29.97 -aspect 4:3 -ab 48000 -ac 2 -ar 22050 -b "+bitrate+"k "+ definitionValue.trim() +" /sdcard/videokit/"+out.getText().toString()+"."+format.toLowerCase();
		}else{
		commandStr="ffmpeg -y -i "+selectedPath.trim()+" -strict experimental -vf transpose=1 -s "+resolution.trim()+" -r 29.97 -aspect 4:3 -ab 48000 -ac 2 -ar 22050 -b "+bitrate+"k "+ definitionValue.trim() +" /sdcard/videokit/"+out.getText().toString()+"."+format.toLowerCase();
		}
		
		System.out.println("onCreate,convert,commandStr="+commandStr);
		
		/**
		 * 高品质：ffmpeg -i infile -ab 128 -acodec libmp3lame -ac 1 -ar 22050 -r 29.97 -qscale 6 -y outfile
			低品质：ffmpeg -i infile -ab 128 -acodec libmp3lame -ac 1 -ar 22050 -r 29.97 -b 512 -y outfile
		 */
		
		setCommand(commandStr);
		//setCommandComplex(complexCommand);

		///optional////
//		setOutputFilePath("/sdcard/videokit/out_compress2.mp4");
		setProgressDialogTitle("Exporting As Video");
		setProgressDialogMessage("Depends on your video size, it can take a few minutes");
		setNotificationIcon(R.drawable.icon2);
		setNotificationMessage("online-android-ffmpeg is running...");
		setNotificationTitle("online-android-ffmpeg");
		setNotificationfinishedMessageTitle("online-android-ffmpeg Transcoding finished");
		setNotificationfinishedMessageDesc("Click to play demo");
		setNotificationStoppedMessage("online-android-ffmpeg Transcoding stopped");
		///////////////

		runTranscoing();
	}
	
	
	
	
}
