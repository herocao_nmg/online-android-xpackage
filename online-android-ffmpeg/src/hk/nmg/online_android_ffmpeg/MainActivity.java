package hk.nmg.online_android_ffmpeg;

import hk.nmg.online_android_ffmpeg.constant.Constants;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends Activity {
	
	private final int FINISHED=200;
	private final int SHOW_DIALOG=100;

	private ListView mListView;
	private List<File> files=new ArrayList<File>();
	private Activity act=this;
	 
	private Handler handler=new Handler(){
		
		private AlertDialog mAlertDialog;

		public void handleMessage(android.os.Message msg) {
			
			switch (msg.what) {
			case FINISHED:
				
				System.out.println("handleMessage,接受到消息, files="+files);
				
				//關閉對話框
				mAlertDialog.dismiss();
				
				mListView.setAdapter(new MediaAdapter(act,files));
				break;
			case SHOW_DIALOG:
				mAlertDialog = builder.show();
				break;
			}
			
		};
		
	};

	private Builder builder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		mListView = (ListView) findViewById(R.id.listView);
		
		builder = new AlertDialog.Builder(this).setTitle("溫馨提示").setMessage("正在加載中,請稍候...");
		
	 
		new AsyncTask<Void, Void, Void>(){

			protected void onPreExecute() {
				
				handler.sendEmptyMessage(SHOW_DIALOG);
				
			};
			
			@Override
			protected Void doInBackground(Void... params) {
				// TODO Auto-generated method stub
				
				FileUtils.lookupFiles(Environment.getExternalStorageDirectory(), files, "mp4" ,"avi","flv");
				
				return null;
			}
		
			protected void onPostExecute(Void result) {
				
				System.out.println("onPostExecute, files="+files);
				
				handler.sendEmptyMessage(FINISHED);
				
			};
		
		}.execute();
			
			mListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long arg3) {
					// TODO Auto-generated method stub
					
					Intent intent=new Intent(act, ConverterActivity.class);
					intent.putExtra(Constants.SELECTED_PATH, files.get(position).getPath());
					startActivity(intent);
//					act.finish();
				}
			});
		
//		mListView.setAdapter(new MediaAdapter(this, files));
	}
	
	
	

	public void test(View view) {
		
		System.out.println("test...");

		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {

				List<File> files =new ArrayList<File>();
				FileUtils.lookupFiles(Environment.getExternalStorageDirectory(),files, "mp4","mp3");
				
				Log.w("TestFileUtils", "TestFileUtils,doInBackground, size="+files.size()+" , files="+files);
				
				
				
				return null;
			}
		}.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	/*private Bitmap createVideoThumbnail(String filePath) {
        Bitmap bitmap = null;
        MediaMetadataRetriever retriever = new MediaMetadataRetriever();
        try {
            retriever.setMode(MediaMetadataRetriever.MODE_CAPTURE_FRAME_ONLY);
            
//            MediaMetadataRetriever.METADATA_KEY_HAS_VIDEO
            retriever.setDataSource(filePath);
            bitmap = retriever.captureFrame();
            
//            retriever.
            
        } catch(IllegalArgumentException ex) {
            // Assume this is a corrupt video file
        } catch (RuntimeException ex) {
            // Assume this is a corrupt video file.
        } finally {
            try {
                retriever.release();
            } catch (RuntimeException ex) {
                // Ignore failures while cleaning up.
            }
        }
        return bitmap;
    }
    
private void getVideoFile(final List<videoItem> list)
{
  Bitmap bitmap = null;
  
  ContentResolver mContentResolver = this.getContentResolver();
        Cursor cursor = mContentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null,  
                null, null, MediaStore.Video.DEFAULT_SORT_ORDER);
        
        
        
        if (cursor.moveToFirst())
        {
            do {
                //ID：MediaStore.Audio.Media._ID 
                int id = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID));  
                  
                //名称 ：MediaStore.Audio.Media.TITLE
                String tilte = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.TITLE));  
                  
                //专辑名：MediaStore.Audio.Media.ALBUM 
                String album = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.ALBUM));  
                  
                  
                //歌手名： MediaStore.Audio.Media.ARTIST 
                String artist = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.ARTIST));  
                  
                //路径 ：MediaStore.Audio.Media.DATA 
                String url = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));  
                  
                //总播放时长 ：MediaStore.Audio.Media.DURATION
                int duration = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION));  
                  
                //大小 ：MediaStore.Audio.Media.SIZE 
                int size = (int)cursor.getLong(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE));
                
                //拍摄时间
                int dateTaken = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATE_TAKEN));
                String datetime = DateFormat.format("yyyy-MM-dd kk:mm:ss", dateTaken).toString();
                              
                bitmap = createVideoThumbnail(url);
   }*/

}
