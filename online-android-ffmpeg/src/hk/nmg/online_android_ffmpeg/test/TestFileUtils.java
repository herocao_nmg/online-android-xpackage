package hk.nmg.online_android_ffmpeg.test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import hk.nmg.online_android_ffmpeg.FileUtils;
import android.os.AsyncTask;
import android.os.Environment;
import android.test.AndroidTestCase;
import android.util.Log;

public class TestFileUtils extends AndroidTestCase {

	public void testLookupFiles(){
		
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				
				Log.w("TestFileUtils", "TestFileUtils,doInBackground, begin ...");

				List<File> files =new ArrayList<File>();
				FileUtils.lookupFiles(Environment.getExternalStorageDirectory(),files, "mp4");
				
				Log.w("TestFileUtils", "TestFileUtils,doInBackground, size="+files.size()+" , files="+files);
				
//				System.out.println("TestFileUtils,doInBackground,files="+files+" , size="+files.size());
				
				return null;
			}
		}.execute();
		
	}
	
}
